#ifndef __TOOLS__
#define __TOOLS__

#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TH2D.h"
#include "TMath.h"
#include "TPaveLabel.h"
#include "TLegend.h"
#include "Parser.h"

#include "ComponentAnsys123.hh"
#include "ComponentComsol.hh"
#include "ViewFEMesh.hh"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <dirent.h>
#include <regex>


using namespace std;
using namespace Garfield;



class AvalancheGif                      // begin declaration of the class
{

 public:
    static const int NGraph=140;
    ////////////////
    ////  c++ constuctor overloading
    ///////////////
    
    /// NO MUON (ANSYS , COMSOL)
    AvalancheGif(string OutDir, ComponentAnsys123* fm, vector<vector <double>> electron,vector <double> Signal,GEM3L* tripleGEM);       // constructor
    AvalancheGif(string OutDir, ComponentComsol* fm, vector<vector <double>> electron,vector <double> Signal,GEM3L* tripleGEM );       // constructor
    /// MUON (ANSYS , COMSOL)
    AvalancheGif(string OutDir, ComponentAnsys123* fm, vector<vector <double>> electron,vector <double> Signal,double x0Track,double dx0Track,double z0Track,double dz0Track,GEM3L* tripleGEM);       // constructor
    AvalancheGif(string OutDir, ComponentComsol* fm, vector<vector <double>> electron,vector <double> Signal,double x0Track,double dx0Track,double z0Track,double dz0Track,GEM3L* tripleGEM );       // constructor

    AvalancheGif(const AvalancheGif& copy_from); //copy constructor
    AvalancheGif& operator=(const AvalancheGif& copy_from); //copy assignment
    ~AvalancheGif();                    // destructor


    
    void MakeGif();
    void PlotFields();
    
 private:                      // begin private section
    string gifDir;
    string outFileName = "Temp.root";
    int muonFrames=3;
    int frames = 100;
    bool SingleElectron = false, MuonTrack = false;
    double pitch = 0.014;
    double zDrift;
    double zAnode;
    bool Ansys,Comsol;
    double zGem1;
    double zGem2;
    double zGem3;
    double x0,z0,dx0,dz0;

    vector<vector <double>> electronDrift;
    vector<double> electrodeSignal;
    
    ViewFEMesh* meshView = new ViewFEMesh();
    Medium * helpMedium = new Medium();
    ComponentAnsys123 * fansys;
    ComponentComsol * fcomsol;
    
    // TGraph of which I want to keep the previous frame
    TGraph * muonframe = new TGraph();
    TGraph * signalFrame = new TGraph();
    // TGraph of which I DON'T want to keep the previous frame
    TGraph * electronframe;
    //FieldLines
    TGraph * graph[NGraph];

    TCanvas * meshCanvas = new TCanvas();
    TCanvas * Animated = new TCanvas("Animated Frames","Animated Frames",860,1080);
    TCanvas * FieldCanvas = new TCanvas("EField","EField");
    TCanvas * ComparisonCanvas = new TCanvas("Comparison");

    //TFile * fieldRoot = new TFile(Form("%s",outFileName.c_str()),"UPDATE");
    
    
    int number_of_layer=0;
    double ArFraction,CO2Fraction,N2Fraction,O2Fraction,H2OFraction;
    double pressureTorr, tempKelvin;
    long long int muonEnergy;
    int Vdrift;
    HOLE hole;
    GEM3L * GEMGaps;

    void drawMesh();        
    void drawMuon();
    void PlotFieldsGEM(double z);
};


#endif
