
#include "PlotTools.h"

// constructor of Avalanchegif, no muon, ansys
AvalancheGif::AvalancheGif(string OutDir, ComponentAnsys123* fm, vector<std::vector <double>> electron,vector <double> Signal,GEM3L* tripleGEM)
{
    gifDir = OutDir;
    GEMGaps = tripleGEM;
    zDrift = GEMGaps->GetzDrift();
    zAnode = GEMGaps->GetzAnode();
    zGem1 = GEMGaps->GetzGem1();
    zGem2 = GEMGaps->GetzGem2();
    zGem3 = GEMGaps->GetzGem3();

    electronDrift = electron;
    electrodeSignal = Signal;

    MuonTrack = false;
    SingleElectron = true;
    Ansys = true;
    Comsol = false;
    
    fansys = fm;
    meshView->SetArea(-5 * pitch, -5 * pitch, zAnode , 
                      5 * pitch,  5 * pitch, zDrift);
    meshView->SetComponent(fm);
    drawMesh();
    
}

// constructor of Avalanchegif, no muon, comsol
AvalancheGif::AvalancheGif(string OutDir, ComponentComsol* fm, vector<vector <double>> electron,vector <double> Signal,GEM3L* tripleGEM )
{
    gifDir = OutDir;
    GEMGaps = tripleGEM;
    zDrift = GEMGaps->GetzDrift();
    zAnode = GEMGaps->GetzAnode();
    zGem1 = GEMGaps->GetzGem1();
    zGem2 = GEMGaps->GetzGem2();
    zGem3 = GEMGaps->GetzGem3();

    electronDrift = electron;
    electrodeSignal = Signal;

    MuonTrack = false;
    SingleElectron = true;
    Ansys = false;
    Comsol = true;
    
    fcomsol = fm;

    meshView->SetArea(-5 * pitch, -5 * pitch, zAnode , 
                      5 * pitch,  5 * pitch, zDrift);
    meshView->SetComponent(fm);
    drawMesh();
    
}

// constructor of Avalanchegif, muon, ansys
AvalancheGif::AvalancheGif(string OutDir, ComponentAnsys123* fm, vector<vector <double>> electron,vector <double> Signal,double x0Track,double dx0Track,double z0Track,double dz0Track,GEM3L* tripleGEM)
{
    gifDir = OutDir;
    GEMGaps = tripleGEM;
    zDrift = GEMGaps->GetzDrift();
    zAnode = GEMGaps->GetzAnode();
    zGem1 = GEMGaps->GetzGem1();
    zGem2 = GEMGaps->GetzGem2();
    zGem3 = GEMGaps->GetzGem3();


    electronDrift = electron;
    electrodeSignal = Signal;

    MuonTrack = true;
    SingleElectron = false;
    Ansys = true;
    Comsol = false;
    
    fansys = fm;

    x0 = x0Track;
    z0 = z0Track;
    dx0 = dx0Track;
    dz0 = dz0Track;


    meshView->SetArea(-5 * pitch, -5 * pitch, zAnode , 
                      5 * pitch,  5 * pitch, zDrift);
    meshView->SetComponent(fm);
    drawMesh();
    
}

// constructor of Avalanchegif, muon, comsol
AvalancheGif::AvalancheGif(string OutDir, ComponentComsol* fm, vector<vector <double>> electron,vector <double> Signal,double x0Track,double dx0Track,double z0Track,double dz0Track,GEM3L* tripleGEM)
{
    gifDir = OutDir;
    GEMGaps = tripleGEM;
    zDrift = GEMGaps->GetzDrift();
    zAnode = GEMGaps->GetzAnode();
    zGem1 = GEMGaps->GetzGem1();
    zGem2 = GEMGaps->GetzGem2();
    zGem3 = GEMGaps->GetzGem3();

    electronDrift = electron;
    electrodeSignal = Signal;

    MuonTrack = true;
    SingleElectron = false;
    Ansys = false;
    Comsol = true;
    
    fcomsol = fm;
    x0 = x0Track;
    z0 = z0Track;
    dx0 = dx0Track;
    dz0 = dz0Track;

    meshView->SetComponent(fm);
    meshView->SetArea(-5 * pitch, -5 * pitch, zAnode , 
                      5 * pitch,  5 * pitch, zDrift);
    drawMesh();
    
}

void AvalancheGif::drawMesh()
{

    meshView->SetCanvas(meshCanvas);
    meshView->SetPlane(0, -1,0, 0, 0, 0);
    meshView->SetFillMesh(true);
    meshView->SetColor(0, kYellow);
    meshView->EnableAxes();
    meshView->SetXaxisTitle("x [cm]");
    meshView->SetYaxisTitle("z [cm]");
    meshView->Plot();

    //Draw a copy of the mesh on the animated canvas
    Animated->Divide(1,2);
    Animated->cd(1);
    meshCanvas->DrawClonePad();
}

void AvalancheGif::MakeGif()
{
    double maxTime=0;

    // loop to find max time
    for(unsigned int k = 0; k < electronDrift.size(); k++)
        {
            if(maxTime < electronDrift[k][3])
                maxTime = electronDrift[k][3];
        }
    maxTime = maxTime*1.1; // increasing time window by 10%


    if(MuonTrack)
        {
            drawMuon();
        }


    double stepTime = maxTime/double(frames);

    //avlannche frames: each cycle is a frame
    for (int i=1; i<=frames; i++)
        {
            electronframe = new TGraph();
            electronframe->SetMarkerStyle(kFullDotMedium);
            electronframe->SetMarkerColor(2);
            electronframe->SetMarkerSize(2);

            signalFrame->SetMarkerStyle(kDot);
            signalFrame->SetMarkerColor(kBlack);
            signalFrame->GetXaxis()->SetTitle("Time [ns]");
            signalFrame->GetYaxis()->SetTitle("Induced signal [#muA]");
            signalFrame->SetTitle("Induced Current on the strip");

            //will store the number of point on the tgraph electronfame for each frame
            int count = 0;

            //Drawing signal on the frame
            // electrodeSignal sample the electrode every ns. The vector index is the time, the content is the current
            signalFrame->SetPoint(i,stepTime*(i),electrodeSignal[TMath::Floor(stepTime*(i))]);

            //Drawing electrons
            for(unsigned int k = 0; k < electronDrift.size(); k++)
                {
                    // electronDrif has 4 items for each index. (x,y,z,t) of one electron in the avalanche
                    // let's draw only the electron relavant for this frame
                    if(electronDrift[k][3] > stepTime*(i-1) && electronDrift[k][3] < stepTime*i )
                        {
                            electronframe->SetPoint(count,electronDrift[k][0],electronDrift[k][2]);
                            count++;
                        }
                                    
                }

            // Filling canvas
            Animated->cd(1);
            if(MuonTrack)
                muonframe->Draw("l");
            electronframe->Draw("P");
            Animated->cd(2);
            signalFrame->Draw("SAME APC");

            // Draw time label
            Animated->cd(1);
            TPad *newpad=new TPad("newpad","a transparent pad",0,0,1,1);
            newpad->SetFillStyle(4050);
            newpad->Draw();
            newpad->cd();
                                                       
            TPaveLabel *title = new TPaveLabel(0.8,0.8,0.94,0.94,Form("t = %.2f ns",stepTime*i));
            title->SetFillColor(10);
            title->SetTextSize(.22);
            title->SetTextFont(52);
            title->Draw();

            TLegend * legend = new TLegend(0.2,0.7,0.4,0.94);
            if(MuonTrack)
                legend->AddEntry(muonframe,"Muon Track","l");
            legend->AddEntry(electronframe,"Avalanche Electrons","p");
            legend->SetFillStyle(0);
            legend->SetTextSize(.04);
            legend->Draw("SAME");

            
   
            Animated->Print("afile.gif+7");
            cout<<"Stored Frame " << i+muonFrames << "/"<<frames+muonFrames<<endl;
            //Erase the electronframe tgraph
            electronframe->Delete();
        }
}

void AvalancheGif::drawMuon()
{
    muonframe->SetLineColor(4);
    muonframe->SetPoint(0,x0, z0);
    double xTrackStep = (5*0.0140*TMath::Sign(1,dx0)-x0)/dx0;
    for(int h = 1; h <= muonFrames; h++)
        {
            muonframe->SetPoint(1,x0+dx0*xTrackStep*h/muonFrames, z0+dz0*xTrackStep*h/muonFrames);

            Animated->cd(1);
            muonframe->Draw("l");
            Animated->Print("afile.gif+7");
            cout<<"Stored Frame " << h << "/"<<frames+muonFrames<<endl;
        } 
    

}


// void AvalancheGif::PlotFields()
// {
//     PlotFieldsGEM(zGem1);
// }

// void AvalancheGif::PlotFieldsGEM(double z)
// {
//     double zCenterView = z;
//     double startZ=zCenterView-0.02;
//     double endZ=zCenterView+0.02;

//     double startX=-pitch/2;
//     double endX=pitch/2;

//     ComponentAnsys123 *  fmap;
//     if(Ansys)
//         fmap = fansys;
//     if(Comsol)
//         {
//             delete fmap;
//             ComponentComsol * fmap = fcomsol;
//         }

//     //View coordinates
//     int status;
//     int f=0;

//     double xcoordinate;
//     double ycoordinate;
//     double zcoordinate;
//     double Ex, Ey, Ez;
//     double mag;


//     TH2D * magnitude  = new TH2D("magnitude",
//                                   "EField Magnitude",
//                                  280, startX,endX,800, startZ, endZ);
//     magnitude->SetStats(false);
//     magnitude->SetTitle("EField Magnitude;x[cm];z[cm];EField [kV/cm]");
//     magnitude->GetZaxis()->SetTitleOffset(0.4);

//     TH2D * comparison  = new TH2D("comparison",
//                                   "EField Magnitude Comparison",
//                                  280, startX,endX,800, startZ, endZ);
//     comparison->SetStats(false);
//     comparison->SetTitle("EField Magnitude Comparison;x[cm];z[cm];EField [kV/cm]");
//     comparison->GetZaxis()->SetTitleOffset(0.7);

//     // temp will host (x,y,z, Ex,Ey,Ez)
//     vector <double> temp;
//     // r will store a list of temp
//     vector<vector <double>> r;


//     // // Field magnitude CUSTOM
//     for (int ix =0; ix<int((endX-startX)*20000); ix++)
//         {
//             for(int iz=0; iz<800; iz++)
//                 {
//                     xcoordinate = startX+(ix)/20000.;
//                     ycoordinate=0.;
//                     zcoordinate=startZ+iz/20000.;
//                     fmap->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
//                     mag= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2))/1000; // EField magn. in Kv/cm
//                     magnitude->SetBinContent(ix,iz,mag);

//                     // // TODO plot differences
//                     // fm2->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
//                     // diff= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2))/1000; // EField magn. in Kv/cm
//                     // if (diff > 0.5 && mag >0.5)
//                     //     {

//                     //         diff = (mag -diff)/mag;
//                     //         comparison->SetBinContent(ix,iz,diff);
                            
//                     //     }
//                 }
//         }

//     FieldCanvas->cd();
//     magnitude->Draw("COLZ");    
//     ComparisonCanvas->cd();
//     //comparison->Draw("COLZ");




//     /////////////////////
//     // Field Lines section
//     /////////////////////

    
//     // https://compphys.go.ro/electric-field-lines/
//     int point = 600000; // cut on the number of points used in field discretization
//     double stepsize = 1; // discretization: evaluation of E-Field every stepsize um
    
//     xcoordinate = startX;
//     ycoordinate=0.;
//     zcoordinate=endZ;


//     while(f<NGraph && xcoordinate <= endX){
//         graph[f] = new TGraph();
//         r.clear();
//         fmap->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
//         temp = {xcoordinate, ycoordinate,zcoordinate,Ex,Ey,Ez};
//         r.push_back(temp);
//         graph[f]->SetPoint(0,xcoordinate,zcoordinate);
//         for(int k = 1; k < point; k++)
//             {
//                 mag= sqrt(pow(temp[3],2)+pow(temp[4],2)+pow(temp[5],2))*10000; // EField magn. in Kv/cm
//                 if(temp[2]< startZ || mag == 0) break;
//                 xcoordinate = r[k-1][0]-r[k-1][3]/(mag) * stepsize;
//                 zcoordinate = r[k-1][2]-r[k-1][5]/(mag)* stepsize;
//                 fmap->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
//                 mag= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2));
//                 if(mag==0)
//                     {
//                         cout << " z err = " << zcoordinate<<endl;
//                         zcoordinate = zcoordinate - 2/10000;
//                         fmap->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
//                         mag= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2));
//                     }

//                 temp = {xcoordinate, ycoordinate,zcoordinate,Ex,Ey,Ez};
//                 r.push_back(temp);                    
//                 graph[f]->SetPoint(k, r[k][0], r[k][2]);
//                 if(mag==0)
//                     break;
//             }


        
//         FieldCanvas->cd();
//         graph[f]->Draw("SAME cl");

//         f++;        
//         xcoordinate = startX +f*0.0002;
//         ycoordinate=0.;
//         zcoordinate=endZ;
//     }



//     FieldCanvas->cd();
//     //fieldRoot->cd();
//     &&TDirectory *dir = fieldRoot->mkdir("FieldSummary");
//     dir->cd();
//     FieldCanvas->Write();
//     // TODO 
//     //ComparisonCanvas->Write();
//     magnitude->Write();
//     //fieldRoot->Write();
//     std::cout<< "EField visualtization have been written in:\n\t" << gifDir+"/"+outFileName << std::endl;
        


// }
