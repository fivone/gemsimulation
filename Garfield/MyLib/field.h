#ifndef __GEM__
#define __GEM__

#include <TApplication.h>
#include <TCanvas.h>
#include <TH1F.h>
#include "TFile.h"
#include "TView3D.h"
#include "TTree.h"

#include "ComponentAnsys123.hh"
#include "ComponentComsol.hh"
#include "ViewField.hh"
#include "ViewFEMesh.hh"
#include "MediumMagboltz.hh"
#include "Sensor.hh"
#include "AvalancheMicroscopic.hh"
#include "AvalancheMC.hh"
#include "Random.hh"
#include "Plotting.hh"
#include "Track.hh"
#include "TrackHeed.hh"
#include "ViewSignal.hh"
#include "Medium.hh"
#include "TCanvas.h"
#include "TBranch.h"
#include "TRandom3.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include<ctime>
 
using namespace std;

bool muonTrack = false;
bool getElectricFieldHole = true;
bool debug = true;
bool strips = false;
bool plotDrift = false;
bool plotField = true;
bool plotDriftTrack = true;
bool plotInducedSignal = false;
bool storeElectronsPositions = false;

const int MAXLEN = 12;
char s [MAXLEN];
time_t t;


std::string hostname;
std::string ansysFileEnding;
std::string ansysFilePath;
std::string repo_path;
std::string repoDataPath;
std::string GEM_type;
std::string GarfieldPath;
std::string outputFilePath;
std::string randomEnergyHistogram;
std::string outFileName;
std::string fullOutPath;
std::string field_map_origin;

//Set up structs for different kinds of info
struct ELECTRON{ 
	double x1,y1,z1;
	double x2,y2,z2;
	double dx2, dy2, dz2;
	double time1, energy1;
	double time2, energy2;
	double radius;
	int status;
    int PrimaryParent;
};

struct ION{ 
	double x1,y1,z1;
	double x2,y2,z2;
	double time1;
	double time2;
	int status;
};

struct INFO{
	int cnt;
	int numberElec, numberIon, numberElecAll;
	double x0,y0,z0,dx0,dy0,dz0,t0,e0;
};

struct MUON{
	double beta;
	double x0Track, y0Track, z0Track;
	double t0Track;
	double dx0Track, dy0Track, dz0Track;
	int numberClusters, numberElecTrack;
	//~ int numberElecPerCluster;
};

struct CLUSTER{
	double xcls, ycls, zcls, tcls;
	double e, extra; //Extra not always implemented, whatever that means.
	int n;
};

struct TRACKELECTRON{ 
	double x,y,z;
	double t,e;
	double dx,dy,dz;
    int clusternumber;
    int aval_ne = 0, aval_ni = 0, aval_np = 0;
	//~ int n;
};

struct ELECTRONDRIFTLINES{
    double xe,ye,ze,te;
    int elecID;
    int PrimaryParent;
    int status;

};

ELECTRON electron;
ION ion;
INFO info;
MUON muon;
ELECTRONDRIFTLINES elecDriftLines;

std::vector<CLUSTER> clusterCollection;
std::vector<TRACKELECTRON> trackElectronCollection;
//~ std::vector<ELECTRON> electronCollection;
//~ std::vector<ION> ionCollecton;
//~ std::vector<INFO> infoCollection;

void TablePrint();
bool makeDir(string path);
void addFileToIndex();
double average(vector<vector <double>> s,int index, int coordinate);
//~ std::string filename;

// Set Dimensions of Area of Detector, Sensor etc.

double ArgonFraction, CO2Fraction, N2Fraction, O2Fraction, H2OFraction;
double temperatureKelvin, pressureTorr;
double zDrift,zGem1,zGem2,zGem3,zAnode;
long long int muonEnergy;
double UpRadius,MidRadius,BotRadius;
double xe1, ye1, ze1, te1, e1; //elektronen
double xe2, ye2, ze2, te2, e2;
double dxe2, dye2, dze2;


double xe1n, ye1n, ze1n, te1n, e1n; //elektronen
double xe2n, ye2n, ze2n, te2n, e2n;
double dx2n, dy2n, dz2n;
int estatusn;

double radius;

double xi1, yi1, zi1, ti1; //ionen
double xi2, yi2, zi2, ti2;

int np;
int elecID=0; // is the ID of each simulated electron
int numberOfPoints;
int numberOfGEMfoils;

int estatus, istatus;
int aval_ne = 0, aval_ni = 0, aval_nh = 0;

double beta, x0Track, y0Track, z0Track, t0Track, dx0Track, dy0Track, dz0Track,dTrackMagnitude; //Muon

//custom study
double xzero;

TTree* t1;
TTree* driftLinesTree;
TBranch* electronBranch;
TBranch* ionBranch;
TBranch* infoBranch;
TBranch* muonBranch;
TBranch* DriftLineBranch;

TFile* OutputFile;

TCanvas * FieldCanvas;
TCanvas* DriftCanvas;
TCanvas* DriftCanvasTrack;
TCanvas* SignalCanvasPrevious;
TCanvas* SignalCanvasAfter;

#endif

