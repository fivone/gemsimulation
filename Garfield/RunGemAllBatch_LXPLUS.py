#!/usr/bin/env python
import os, re
import commands
import math, time
import sys
import random
import subprocess
import numpy as np

print 
print 'START'
print 
########   YOU ONLY NEED TO FILL THE AREA BELOW   #########
########   customization  area #########

##########
# Example
#########
# NumberOfJobs = 10
# interval = 50
# 2 values of pressure
# 2 values of T
# 5 values of ANSYSFILE
## TOTAL = 20 different simulation conditions

## 10 cluster node involved for each condition
## Each produces 50 simulation .root files for each condition
## 500 Simulations for each conditions
## It means you will run 10x20 = 200 jobs that will produce 10000 simulation .root files


### CAREFULLY CHOSE WHAT YOU WANT TO SIMULATE, DON'T CLOG THE GRID

NumberOfJobs = 200  # number of cluster node to use (machines that will run the simulation executable)
interval = 25  # number of times that each machine will execute the executable
job_flavour = "tomorrow" #longlunch=2h, workday=8h

#~ espresso	20min 	8nm
#~ microcentury 	1h 	1nh
#~ longlunch 	2h 	8nh
#~ workday 	8h 	1nd
#~ tomorrow 	1d 	2nd
#~ testmatch 	3d 	1nw
#~ nextweek 	1w 	2nw 

if os.getenv("REPODATAPATH"):
    eos_dir = str(os.getenv("REPODATAPATH")) #where your job should save the data and create the subfolders.
else:
    print("No env variable found for storage directory")
    print("Exit...")
    sys.exit()
if os.getenv("REPO_HOME"):
    afs_dir = str(os.getenv("REPO_HOME") + "/Garfield/") #where gemAllBatch.cpp and this file are located.
else:
    print("No env variable found for afs_dir (working directory)")
    print("Exit...")
    sys.exit()
if os.getenv("GARFIELD_HOME"):
    GARFIELD_HOME_Location = str(os.getenv("GARFIELD_HOME")) #To export GARFIELD_HOME and HEED_DATABASE
else:
    print("No env variable found for GARFIELD_HOME")
    print("Exit...")
    sys.exit()
if os.getenv("HOME"):
    home_dir = str(os.getenv("HOME")) #your home folder
else:
    print("No env variable found for home directory")
    print("Exit...")
    sys.exit()
########   customization end   #########

path = os.getcwd()

print
print

### Needs to be done only the first time running the script  ####
os.system("mkdir -p " + afs_dir + "/Executables/")
os.system("mkdir -p " + afs_dir + "/BatchScripts/")

jobCnt=0

# define everything in lists. Only vary one (otherwise computing time gets too long)

# date tag
dateTag = 'GemSimulation_{0}'.format(time.strftime("%Y-%m-%d_%H-%M-%S"))

# Automatically normalizes to 1.
### changed compared to previous versions
### MAKE SURE all lists with same length
ARGON = [70.]
CO2 =   [30.]
N2 =    [0.]
O2 =    [0.]
H2O =   [0.]

# Standard conditions
PRESSURE = [723] # in torr
#[288.15,292.15,296.15,300.15,304.15,308.15] # in Kelvin
TEMPERATURE = [296.15] # in Kelvin

# CMS P5 conditions
#PRESSURE = [723.] # in torr
#TEMPERATURE = [297.1] # in Kelvin

# CMS GE1/1 magnetic field 3T and 8-9 deg polar angle alpha(E-B)
# CMS GE2/1 magnetic field 1.5 T and 2-15 deg polar angle alpha(E-B)
BMag = 3.0
PolarAngle = 9. 
#~ MAGNETICFIELD = [BMag*np.sin(PolarAngle * np.pi / 180.),0.,BMag*np.cos(PolarAngle * np.pi / 180.)]
MAGNETICFIELD = [0,0,0]


#~ ANSYSFILES=["_1L_70_70_53_85_85_DeltaV_394V"]
#~ ANSYSFILES=["_Hole_70_50_70_VD_664_VGEM_394_VIND_437_1L"]
ANSYSFILES=[

    ## double mask
    #"Hole_70_50_70_VDRIFT_2584_3L",
    #"Hole_70_50_70_VDRIFT_2631_3L",
    #"Hole_70_50_70_VDRIFT_2678_3L",
    #"Hole_70_50_70_VDRIFT_2725_3L",
    #"Hole_70_50_70_VDRIFT_2772_3L",
    #"Hole_70_50_70_VDRIFT_2819_3L",
    #"Hole_70_50_70_VDRIFT_2866_3L",
    #"Hole_70_50_70_VDRIFT_2913_3L",
    #"Hole_70_50_70_VDRIFT_2960_3L",
    #"Hole_70_50_70_VDRIFT_3007_3L",
    #"Hole_70_50_70_VDRIFT_3054_3L",
    "Hole_70_50_70_VDRIFT_3101_3L",
    #"Hole_70_50_70_VDRIFT_3148_3L",
    #"Hole_70_50_70_VDRIFT_3195_3L",
    #"Hole_70_50_70_VDRIFT_3242_3L",
    #"Hole_70_50_70_VDRIFT_3289_3L",
    #"Hole_70_50_70_VDRIFT_3336_3L",
    #"Hole_70_50_70_VDRIFT_3383_3L",
    #"Hole_70_50_70_VDRIFT_3430_3L",
    #"Hole_70_50_70_VDRIFT_3477_3L"


    ## Hole Uncertainity
    #"Hole_67_47_82_VDRIFT_2500_3L",
    #"Hole_67_47_82_VDRIFT_2700_3L",
    #"Hole_67_47_82_VDRIFT_2900_3L",
    #"Hole_67_47_82_VDRIFT_3100_3L",
    #"Hole_67_47_82_VDRIFT_3290_3L",
    #"Hole_67_47_82_VDRIFT_3400_3L",
]


varied = ANSYSFILES # also part of naming. Default: ANSYSFILES
    

for gasCnt in range(0, len(ARGON)):
    for pressureCnt in range(0, len(PRESSURE)):
        for temperatureCnt in range(0, len(TEMPERATURE)):
            for ansysCnt in range(0, len(ANSYSFILES)):
            
            
                variedCnt = ansysCnt # For naming purposes.
                NAME="%s"%(str(ANSYSFILES[ansysCnt]))
                
                hourTAG='{0}'.format(time.strftime("%H.%M.%S"))
                MUON="n" ### options: "muon", "gamma" for the HEED particles and "n" if you want to simulate only avalanche
                MuonEnergy=300000000 #300 MeV
                PenningTransfer = 0.57

                ######### Customization end

                #Make executable
                os.chdir(afs_dir)
                os.system("make gemAllBatch")

                #Move executable
                os.system("cp "+afs_dir+"/gemAllBatch "+afs_dir+"/Executables/gemAllBatch%s"%(str(NAME)))

                StartJobNumber = 0

                for x in range(StartJobNumber, int(NumberOfJobs)):
                    
                    RUN = x
                    ContinueFromJobNumber = 0

                    ##### creates jobs ####### makes bash file
                    with open(afs_dir+'/BatchScripts/job_%s_Run_'%(str(NAME)) + str(x) + ".sh", 'w') as fout:
                        
                        
                       ####### Write the instruction for each job
                      fout.write("#!/bin/sh\n")
                      fout.write("echo\n")
                      fout.write("echo %s_Run%s\n"%(NAME, str(x)))
                      fout.write("echo\n")
                      fout.write("echo 'START---------------'\n")				
                      fout.write("ITERATOR=\"%s\""%(str(ContinueFromJobNumber))+"\n")			  
                      fout.write("cd "+afs_dir+"/Executables"+"\n")

                      ## sourceing the right gcc version to compile the source code
                      fout.write("source /cvmfs/sft.cern.ch/lcg/contrib/gcc/9.1.0/x86_64-centos7/setup.sh"+"\n")
                      fout.write("source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.04/x86_64-centos7-gcc48-opt/bin/thisroot.sh"+"\n")
                      fout.write("export GARFIELD_HOME="+GARFIELD_HOME_Location+"\n")
                      fout.write("export HEED_DATABASE=$GARFIELD_HOME/Heed/heed++/database/"+"\n")

                      #generate kerberos ticket to access EOS folder and link folder
                      #fout.write("sleep 15\n")
                      #fout.write("kinit -kt " +afs_dir+"/fivone.keytab  fivone\n")
                      #fout.write("eosfusebind -g\n")

                    ## Run the same job interval number of time
                      fout.write("while [ ${ITERATOR} -lt %i ]"%(interval)+"\n")
                      fout.write("do"+"\n")
                      fout.write("echo \"$ITERATOR\"\n");
                      fout.write("ClusterId=$1\n")
                      fout.write("ProcId=$2\n")
                      fout.write("hourTAG=$(date +%H.%M.%S)\n");
                      fout.write("\t"+"echo %s_Run%s_\"$ITERATOR\"\n"%(NAME, str(x)))
                      fout.write("\t"+"./gemAllBatch%s "%(str(NAME))+"\"$ClusterId\".\"$ITERATOR\"_\"$hourTAG\".root %s %s %d %d %d %d %d %d %d %d" %(str(MUON), str(ANSYSFILES[ansysCnt]), ARGON[gasCnt], CO2[gasCnt], N2[gasCnt], O2[gasCnt], PRESSURE[pressureCnt], TEMPERATURE[temperatureCnt], H2O[gasCnt], MuonEnergy)+"\n")
                      fout.write("\t"+"(( ITERATOR++ ))"+"\n")
                      fout.write("done"+"\n")

                      fout.write("cd "+afs_dir+"\n")
                      #Destroy kerberos ticket that allows to eccess EOS folder
                      #fout.write("kdestroy\n") 
                      #fout.write("echo 'STOP---------------'\n")
                      #fout.write("echo\n")
                      #fout.write("echo\n")
                      #os.system("chmod 755 "+afs_dir+"/BatchScripts/job_%s_Run_"%(str(NAME)) + str(x) + ".sh")

                    ##### creates .sub file for condor submit ######
                    with open(afs_dir+'/BatchScripts/subfile_job_%s_Run_'%(str(NAME)) + str(x) + ".sub", 'w') as sout:
                        sout.write("executable              = job_%s_Run_"%(str(NAME)) + str(x)+".sh"+"\n")
                        sout.write("getenv                  = true"+"\n")
                        sout.write("arguments               = $(ClusterId) $(ProcId)"+"\n")
                        sout.write("output                  = out.$(ClusterId).dat"+"\n")
                        sout.write("error                   = error.$(ClusterId).err"+"\n")
                        sout.write("log                     = log.$(ClusterId).log"+"\n")
                        sout.write("+JobFlavour             = \""+job_flavour+"\" "+"\n")
                        sout.write("queue"+"\n")
                          

                    ###### sends jobs ######
                    os.chdir(afs_dir+"/BatchScripts")
                    os.system("condor_submit "+afs_dir+"/BatchScripts/subfile_job_%s_Run_"%(str(NAME)) + str(x) + ".sub") 
                    print "job nr " + str(jobCnt) + " submitted"

                    os.chdir(path)
                    
                    jobCnt = jobCnt+1
   
print
print "your jobs:"
os.system("condor_q")
print
