#include <iostream>
#include <fstream>

#include "field.h"
#include "Parser.h"
#include <TGraph.h>
#include <TH2D.h>
using namespace Garfield;
using namespace std;
using namespace TMath;

/******
Including table_printer for
printing in a nice table shape
******/
#include "table_printer.h"
#define _USE_MATH_DEFINES
#include <math.h>
#if defined(USE_BOOST_KARMA)
#include <boost/spirit/include/karma.hpp>
namespace karma = boost::spirit::karma;
#endif
using bprinter::TablePrinter;
/******
******/


int main(int argc, char * argv[]) {
    //TODO: Store the Gas Mixture + Temperature + Pressure in the filename or log
    SimuParser* parser = new SimuParser(argc, argv);

    ansysFileEnding = parser->GetFieldMapID();
    numberOfGEMfoils = parser->GetNumberOfLayer();
    GEM_type = to_string(numberOfGEMfoils) + "L";
    field_map_origin = parser->GetFieldMapOrigin();

    zDrift = parser->GetTripleGem()->GetzDrift();
    zAnode = parser->GetTripleGem()->GetzAnode();
    zGem1 = parser->GetTripleGem()->GetzGem1();
    zGem2 = parser->GetTripleGem()->GetzGem2();
    zGem3 = parser->GetTripleGem()->GetzGem3();

    UpRadius = double(parser->GetHole()->GetUpRadius())/1000;
    MidRadius =double(parser->GetHole()->GetMidRadius())/10000;
    BotRadius =double(parser->GetHole()->GetBotRadius())/1000;
    
    if(debug) parser->PrintSummary();
    
    /***********Start Config**************/

    // repoDatapath stores the data
    // repo_home stores the source code fore simulation
    
    repoDataPath = getenv("REPODATAPATH");
    ansysFilePath = repoDataPath+"/Ansys/FinalAnsysFiles/"; //Put your path to the Ansys files here.

    // /***********End Config**************/

  

    FieldCanvas = new TCanvas("EField");
    TCanvas * ComparisonCanvas = new TCanvas("Comparison");
    FieldCanvas->Divide(1,1);
    //View coordinates
    int status;
    int f=0;

    const double pitch = 0.014;
    double startX=-pitch/2;
    double endX=pitch/2;

    double zCenterView = zGem1;
    double startZ=zCenterView-0.02;
    double endZ=zCenterView+0.02;

    double xcoordinate;
    double ycoordinate;
    double zcoordinate;
    double Ex, Ey, Ez;
    double mag;
    double diff;


    Medium * helpMedium = new Medium();
    const int NGraph=140;
    TGraph * graph[NGraph];
    TH2D * magnitude  = new TH2D("magnitude",
                                  "EField Magnitude",
                                 280, startX,endX,800, startZ, endZ);
    magnitude->SetStats(false);
    magnitude->SetTitle("EField Magnitude;x[cm];z[cm];EField [kV/cm]");
    magnitude->GetZaxis()->SetTitleOffset(0.4);

    TH2D * comparison  = new TH2D("comparison",
                                  "EField Magnitude Comparison",
                                 280, startX,endX,800, startZ, endZ);
    comparison->SetStats(false);
    comparison->SetTitle("EField Magnitude Comparison;x[cm];z[cm];EField [kV/cm]");
    comparison->GetZaxis()->SetTitleOffset(0.7);
    
    
    
    // temp will host (x,y,z, Ex,Ey,Ez)
    vector <double> temp;
    // r will store a list of temp
    vector<vector <double>> r;   
    
    

    
    // Silly way to do. The solution would be to delegate the fm creation to a function
    ComponentAnsys123* fm = new ComponentAnsys123();
    
    if(field_map_origin != "COMSOL")
        {
            fm->Initialise(ansysFilePath +"ELIST_" +ansysFileEnding +".lis",
                           ansysFilePath+"NLIST_" +ansysFileEnding +".lis",
                           ansysFilePath +"MPLIST_" +ansysFileEnding +".lis",
                           ansysFilePath+"PRNSOL_" +ansysFileEnding +".lis",
                           "mm");
        }
    else
        {
            delete fm;
            ComponentComsol* fm = new ComponentComsol();
            fm->Initialise(repoDataPath+"COMSOL/mesh_"+ansysFileEnding+".mphtxt",
                           repoDataPath+"COMSOL/Triple_GEM_dielectrics.dat",
                           repoDataPath+"COMSOL/volt_"+ansysFileEnding+".mphtxt"
                           );            
        }


    ComponentAnsys123* fm2 = new ComponentAnsys123();
    string ansysFileEnding2= "Hole_70_50_70_VDRIFT_2500_3L";
    fm2->Initialise(ansysFilePath +"ELIST_" +ansysFileEnding2 +".lis",
                    ansysFilePath+"NLIST_" +ansysFileEnding2 +".lis",
                    ansysFilePath +"MPLIST_" +ansysFileEnding2 +".lis",
                    ansysFilePath+"PRNSOL_" +ansysFileEnding2+".lis",
                    "mm");
    
    fm->EnableMirrorPeriodicityX();
    fm->EnableMirrorPeriodicityY();

    fm2->EnableMirrorPeriodicityX();
    fm2->EnableMirrorPeriodicityY();


    TCanvas* meshCanvas = new TCanvas();
    ViewFEMesh* meshView = new ViewFEMesh();
    meshView->SetArea(startX, -2 * pitch,startZ, 
                      endX,  2 * pitch, endZ);
    meshView->SetPlane(0, -1, 0, 0, 0, 0);
    meshView->SetCanvas(meshCanvas);
    meshView->SetComponent(fm2);
    // x-z projection.

    meshView->SetFillMesh(true);
    // Set the color of the kapton.
    meshView->SetColor(4, kYellow + 3);
    meshView->SetColor(1, kBlack);
    meshView->SetFillColor(1,kBlack);
    meshView->SetFillColor(4,kYellow+3);
    meshView->Plot();

    FieldCanvas->cd(1);
    meshCanvas->DrawClonePad();
    ComparisonCanvas->cd();
    meshCanvas->DrawClonePad();
    
    if(debug)
        {
            fm->PrintRange();
        }


    // // Field magnitude CUSTOM
    for (int ix =0; ix<280; ix++)
        {
            for(int iz=0; iz<800; iz++)
                {
                    xcoordinate = startX+(ix)/20000.;
                    ycoordinate=0.;
                    zcoordinate=startZ+iz/20000.;
                    fm->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
                    mag= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2))/1000; // EField magn. in Kv/cm
                    magnitude->SetBinContent(ix,iz,mag);

                    fm2->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
                    if (mag > 0.5)
                        {
                            diff= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2))/1000; // EField magn. in Kv/cm
                            diff = (mag -diff)/mag;
                        }
                    comparison->SetBinContent(ix,iz,diff);
                }
        }



    
    FieldCanvas->cd(1);
    magnitude->Draw("COLZ");
    ComparisonCanvas->cd();
    comparison->Draw("COLZ");
    
        
    // // Field magnitude GARFIELD
    // {
    // ViewField fieldView;
    // fieldView.SetComponent(fm);
    // fieldView.SetPlane(0, -1, 0, 0, 0, 0);
    // fieldView.SetArea(startX, startZ, endX, endZ);
    // fieldView.SetElectricFieldRange(-20., 60000.);
    // fieldView.SetNumberOfContours(40);
    // fieldView.SetCanvas(FieldCanvas);
    
    // fieldView.Plot("e","COLZ");


    // // }

    // Field Lines
    // https://compphys.go.ro/electric-field-lines/
    int point = 600000; // max number of points used in field discretization
    double stepsize = 1; // discretization: evaluation of E-Field every stepsize um
    
    xcoordinate = -startX;
    ycoordinate=0.;
    zcoordinate=endZ;


    while(f<NGraph && xcoordinate <= endX){
        graph[f] = new TGraph();
        r.clear();
        fm->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
        temp = {xcoordinate, ycoordinate,zcoordinate,Ex,Ey,Ez};
        r.push_back(temp);
        graph[f]->SetPoint(0,xcoordinate,zcoordinate);
        for(int k = 1; k < point; k++)
            {
                mag= sqrt(pow(temp[3],2)+pow(temp[4],2)+pow(temp[5],2))*10000; // EField magn. in Kv/cm
                if(temp[2]< startZ || mag == 0) break;
                xcoordinate = r[k-1][0]-r[k-1][3]/(mag) * stepsize;
                zcoordinate = r[k-1][2]-r[k-1][5]/(mag)* stepsize;
                fm->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
                mag= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2));
                if(mag==0)
                    {
                        cout << " z err = " << zcoordinate<<endl;
                        zcoordinate = zcoordinate - 2/10000;
                        fm->ElectricField(xcoordinate,ycoordinate,zcoordinate, Ex, Ey, Ez, helpMedium, status);
                        mag= sqrt(pow(Ex,2)+pow(Ey,2)+pow(Ez,2));
                    }

                temp = {xcoordinate, ycoordinate,zcoordinate,Ex,Ey,Ez};
                r.push_back(temp);                    
                graph[f]->SetPoint(k, r[k][0], r[k][2]);
                if(mag==0)
                    break;
            }


        
        FieldCanvas->cd(1);
        graph[f]->Draw("SAME cl");

        f++;        
        xcoordinate = startX +f*0.0002;
        ycoordinate=0.;
        zcoordinate=endZ;
    }

    /////////////////////////////
    ////   Write results to file
    /////////////////////////////


    outputFilePath=repoDataPath+"DataAna/Fields/"+ansysFileEnding;
    outFileName = "ElectricFieldChecker.root";
    
    if(makeDir(outputFilePath))
        {
            OutputFile = new TFile(Form("%s/%s",outputFilePath.c_str(),outFileName.c_str()),"RECREATE");
            TDirectory *dir = OutputFile->mkdir("Summary");
            dir->cd();
            FieldCanvas->Write();
            ComparisonCanvas->Write();
            magnitude->Write();
            comparison->Write();
            OutputFile->Write();
            std::cout<< "EField visualtization have been written in:\n\t" << outputFilePath+"/"+outFileName << std::endl;
        }
    else {}
    
}

bool makeDir(string path){
    DIR *pDIR;
    if( (pDIR=opendir(path.c_str())) )
        {
            if(debug)std::cout << "Directory: \n\t" << path << "\nAlready exsists" << std::endl;
        }
    else
        {
            //Directory created with success
            if(system(("mkdir -p " + path).c_str())==0)
                {
                    if (debug) std::cout << "Directory created: \n\t" << path << std::endl;
                }
            //Directory creation failed
            else
                {
                    std::cout << "There was an error creating the directory:\n\t" << path << "\nEXITING"<<std::endl;
                    return false;
                }
        }
    return true;
}


void PrintPrimariesSummary(int numberOfLines, bool I_want_to_copy_paste=false)
{        // Print Summary of the avalanches

    std::cout << "\n#######################    Primary Electrons Summary    #######################" <<std::endl;
    TablePrinter tp(&std::cout);
    tp.AddColumn("Prim. Elec.", 12);
    tp.AddColumn("Cluster", 7);
    tp.AddColumn("x0(um)", 8);
    tp.AddColumn("y0(um)", 8);
    tp.AddColumn("z0(mm)", 8);
    tp.AddColumn("Elec Prod", 8);
    tp.AddColumn("TotElec", 8);
    tp.AddColumn("Ions Prod", 8);
    tp.PrintHeader();
    int temp_cluster=0;
    for(int h = 0; h < numberOfLines; h++)
        {
            if (!I_want_to_copy_paste){
                if(temp_cluster!=trackElectronCollection[h].clusternumber)
                    {tp.PrintFooter(); temp_cluster =trackElectronCollection[h].clusternumber;}
                else{temp_cluster =trackElectronCollection[h].clusternumber;}

                tp << h+1 << trackElectronCollection[h].clusternumber+1
                   << trackElectronCollection[h].x*10000<< trackElectronCollection[h].y*10000
                   << trackElectronCollection[h].z*10<< trackElectronCollection[h].aval_ne
                   << trackElectronCollection[h].aval_np<< trackElectronCollection[h].aval_ni;
            }
            else{
                std::cout << h+1 << "\t"<<trackElectronCollection[h].clusternumber+1
                   << "\t"<< trackElectronCollection[h].x*10000<< "\t"<< trackElectronCollection[h].y*10000
                   << "\t"<< trackElectronCollection[h].z*10<< "\t"<< trackElectronCollection[h].aval_ne
                   << "\t"<< trackElectronCollection[h].aval_np<< "\t"<< trackElectronCollection[h].aval_ni
                          <<std::endl;

            }
        }
    tp.PrintFooter();
    tp << "" << ""<< ""<< ""<< ""<< ""<< elecID<< "";
    tp.PrintFooter();
}
