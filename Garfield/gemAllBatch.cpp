/***** MY Triple-GEM GEOMETRY


Drift        ____________________________      Z = 4mm      V = vanode (100% vanode)
            |                            |
            |                            |
            |                            |
            |                            |
3 mm        |                            |
            |                            |
            |                            |
            |                            |
            |                            |      GEMTOP      V =   (76% vanode)
GEM1        |++++++++++++++++++++++++++++|      Z = 1mm
            |                            |      GEMBOT      V =   (64% vanode)
1 mm        |                            |
            |                            |      GEMTOP      V =   (55% vanode)
GEM2        |++++++++++++++++++++++++++++|      Z = 0mm
            |                            |      GEMBOT      V =   (43% vanode)
            |                            |
2 mm        |                            |
            |                            |
            |                            |
            |                            |      GEMTOP      V =   (24% vanode)
GEM3        |++++++++++++++++++++++++++++|      Z = -2mm
            |                            |      GEMBOT      V =   (13% vanode)
1mm         |                            |
anod        |____________________________|      Z = -3mm -  V =    0

****/
#include <iostream>
#include <fstream>

#include "gemAllBatch.h"
#include "Parser.h"
#include "PlotTools.h"

using namespace Garfield;
using namespace std;
using namespace TMath;

/******
Including table_printer for
printing in a nice table shape
******/
#include "table_printer.h"
#define _USE_MATH_DEFINES
#include <math.h>
#if defined(USE_BOOST_KARMA)
#include <boost/spirit/include/karma.hpp>
namespace karma = boost::spirit::karma;
#endif
using bprinter::TablePrinter;
/******
******/


int main(int argc, char * argv[]) {
    //TODO: Store the Gas Mixture + Temperature + Pressure in the filename or log
    SimuParser* parser = new SimuParser(argc, argv);

    //gROOT->SetBatch(kTRUE);
    
    hostname = parser->GetHostName();
    outFileName = parser->GetOutFileName();
    muonTrack = parser->muonTrack();
    ansysFileEnding = parser->GetFieldMapID();
    numberOfGEMfoils = parser->GetNumberOfLayer();
    GEM_type = to_string(numberOfGEMfoils) + "L";
    ArgonFraction = parser->GetArFraction();
    CO2Fraction = parser->GetCO2Fraction();
    N2Fraction = parser->GetN2Fraction();
    O2Fraction = parser->GetO2Fraction();
    pressureTorr = parser->GetPressureTorr();
    temperatureKelvin = parser->GetTempKelvin();
    H2OFraction = parser->GetH2OFraction();
    muonEnergy = parser->GetMuonEnergy();
    field_map_origin = parser->GetFieldMapOrigin();

    TripleGEM = parser->GetTripleGem();
    zDrift = parser->GetTripleGem()->GetzDrift();
    zAnode =  parser->GetTripleGem()->GetzAnode();
    zGem1 = parser->GetTripleGem()->GetzGem1();
    zGem2 = parser->GetTripleGem()->GetzGem2();
    zGem3 = parser->GetTripleGem()->GetzGem3();

    UpRadius = double(parser->GetHole()->GetUpRadius())/1000;
    MidRadius =double(parser->GetHole()->GetMidRadius())/10000;
    BotRadius =double(parser->GetHole()->GetBotRadius())/1000;

    
    if(debug) parser->PrintSummary();
    
    /***********Start Config**************/

    // repoDatapath stores the data
    // repo_home stores the source code fore simulation
    
    repoDataPath = getenv("REPODATAPATH");
    ansysFilePath = repoDataPath+"/Ansys/FinalAnsysFiles/"; //Put your path to the Ansys files here.
    GarfieldPath = getenv("GARFIELD_HOME");

       
    // /***********End Config**************/

    //Make the Trees
    t1 = new TTree("t1","MyTree");
    t2 = new TTree("t2","MyTree2");
    electronBranch = t1->Branch("electronBranch", &electron, "x1/D:y1/D:z1/D:x2/D:y2/D:z2/D:dx2/D:dy2/D:dz2/D:time1/D:energy1/D:time2/D:energy2/D:radius/D:status/I:primaryParent/I");
    ionBranch = t1->Branch("ionBranch", &ion, "x1/D:y1/D:z1/D:x2/D:y2/D:z2/D:time1/D:time2/D:status/I");
    infoBranch = t1->Branch("infoBranch", &info, "cnt/I:numberElec/I:numberIon/I:numberElecAll/I:x0/D:y0/D:z0/D:dx0/D:dy0/D:dz0/D:time0/D:e0/D"); //only naming! sorting matters!
    if(muonTrack)
        {
            muonBranch = t1->Branch("muonBranch", &muon, "beta/D:x0Track/D:y0Track/D:z0Track/D:t0Track/D:dx0Track/D:dy0Track/D:dz0Track/D:numberClusters/I:numberElecTrack/I");
        }
    if(plotInducedSignal)
        {
            IntegratedSignal = t2->Branch("integratedsignal", &signalIntegral, "integratedSignal/D");
            WidthSignal = t2->Branch("SignalWidth", &signalWidth, "signalWidth/D");
        }
    
    driftLinesTree= new TTree("driftLinesTree","DrfitTree");
    DriftLineBranch = driftLinesTree->Branch("DriftLineBranch", &elecDriftLines, "xe/D:ye/D:ze/D:te/D:elecID/I:PrimaryParent/I:status/I");


    plottingEngine.SetDefaultStyle();

    //Run the simulation with Garfield
    int exitCode = runSim();
    if(exitCode == 5){return 5;}

    /*********************
    Write results to file
    **********************/

    t = time(0);
    strftime(s,MAXLEN,"%Y.%m.%d", localtime(&t));


    if(muonTrack)
        outputFilePath=repoDataPath+"DataRaw/MuonTrack/"+GEM_type+"/"+ansysFileEnding +"/" + s+"/";
    else
        outputFilePath=repoDataPath+"DataRaw/SingleElectron/"+GEM_type+"/"+ansysFileEnding+ "/" +s+"/";
    
    if(makeDir(outputFilePath))
        {
            
            fullOutPath = outputFilePath + to_string(int(pressureTorr))+"Torr_"+to_string(int(temperatureKelvin))+"K_"+outFileName;
            OutputFile = new TFile(Form("%s",fullOutPath.c_str()),"RECREATE");
            //Sleep function to ensure correct TFile creation
            sleep(1);
            OutputFile->cd();
            t1->Write();
            t2->Write();
            OutputFile->cd();
            
            if(storeElectronsPositions)
                {
                    driftLinesTree->Write();
                }
            

            if(plotMesh){
                 MeshCanvas->Write();

            }          
            // if(plotDrift){
            //      DriftCanvas->Write();
            // }
            if(plotInducedSignal){
                 inducedSignal->Write();
            }
            
            OutputFile->Write();
            std::cout<< "Simulation results have been written in:\n\t" << fullOutPath << std::endl;
        }
    else 
        {
            //TODO: Manage the error
        }

    // making x-z drift frames
    
    // if(storeAvalancheFrames)
    //     {
    //         string gifDir = repoDataPath+"DataRaw/Gif/";
    //         Test = new TCanvas("Animated Frames","Animated Frames",860,1080);
    //         Test->Divide(1,2);
    //         Test->cd(1);
    //         MeshCanvas->DrawClonePad();


            
    //         if(makeDir(gifDir))
    //             {
    //                 double maxTime=0;
    //                 // loop to find max time
    //                 for(int k = 0; k < electronDrift.size(); k++)
    //                     {
    //                         if(maxTime < electronDrift[k][3])
    //                             maxTime = electronDrift[k][3];
    //                     }

    //                 maxTime = maxTime*1.1; // increasing time window by 10%
    //                 // adding the muon track
    //                 TGraph * muonframe = new TGraph();
    //                 TGraph * signalFrame = new TGraph();
    //                 if(muonTrack)
    //                     {
    //                         int muonFrames=3;
    //                         muonframe->SetLineColor(4);
    //                         muonframe->SetPoint(0,x0Track, z0Track);
    //                         double xTrackStep = (5*0.0140*TMath::Sign(1,dx0Track)-x0Track)/dx0Track;
    //                         for(int h = 1; h <= muonFrames; h++)
    //                             {
    //                                 muonframe->SetPoint(1,x0Track+dx0Track*xTrackStep*h/muonFrames, z0Track+dz0Track*xTrackStep*h/muonFrames);
    //                                 // TCanvas * temp = MeshCanvas;

    //                                 Test->cd(1);
    //                                 // MeshCanvas->DrawClonePad();
    //                                 muonframe->Draw("l");
                                    
    //                                 // temp->cd();
    //                                 // muonframe->Draw("l");
                                    
    //                                 // temp->Print("file.gif+1");
    //                                 Test->Print("afile.gif+7");}
                                    
    //                     }

                    
    //                 int frames = 100;
    //                 double stepTime = maxTime/double(frames);
    //                 //avlannche frames loop. 
    //                 for (int i=1; i<frames; i++)
    //                     {
    //                         TGraph * electronframe = new TGraph();


    //                         electronframe->SetMarkerStyle(kFullDotMedium);
    //                         electronframe->SetMarkerColor(2);
    //                         electronframe->SetMarkerSize(2);

    //                         signalFrame->SetMarkerStyle(kDot);
    //                         signalFrame->SetMarkerColor(kBlue);
    //                         signalFrame->GetXaxis()->SetTitle("Time [ns]");
    //                         signalFrame->GetYaxis()->SetTitle("Induced signal [#muA]");
    //                         signalFrame->SetTitle("Induced Current on the strip");

    //                         int count = 0;

    //                         signalFrame->SetPoint(i,stepTime*(i),electrodeSignal[TMath::Floor(stepTime*(i))]);
    //                         cout << "X = " << stepTime*(i) << "\tY = "<<electrodeSignal[TMath::Floor(stepTime*(i))]<< endl;
    //                         //Filling Tgraph
    //                         for(int k = 0; k < electronDrift.size(); k++)
    //                             {
    //                                 //
    //                                 if(electronDrift[k][3] > stepTime*(i-1) && electronDrift[k][3] < stepTime*i )
    //                                     {
    //                                         electronframe->SetPoint(count,electronDrift[k][0],electronDrift[k][2]);
    //                                         count++;
    //                                     }
                                    
    //                             }

    //                         // Filling canvas
    //                         Test->cd(1);
    //                         if(muonTrack)
    //                             muonframe->Draw("l");
    //                         electronframe->Draw("P");
    //                         Test->cd(2);
    //                         signalFrame->Draw("SAME APC");

    //                         // TCanvas * temp = MeshCanvas;
    //                         // temp->cd();
    //                         // if(muonTrack)
    //                             // muonframe->Draw("l");
    //                         // electronframe->Draw("P");

    //                         Test->cd(1);
    //                         TPad *newpad=new TPad("newpad","a transparent pad",0,0,1,1);
    //                         newpad->SetFillStyle(4050);
    //                         newpad->Draw();
    //                         newpad->cd();
                            

                            
    //                         TPaveLabel *title = new TPaveLabel(0.8,0.8,0.94,0.94,Form("t = %.2f ns",stepTime*i));
    //                         title->SetFillColor(10);
    //                         title->SetTextSize(.22);
    //                         title->SetTextFont(52);
    //                         title->Draw();
   
                            
    //                         string outname = gifDir + to_string(i) + ".png";
    //                         // temp->SaveAs(outname.c_str());
    //                         //temp->Print("file.gif+4");
    //                         Test->Print("afile.gif+7");
    //                         electronframe->Delete();
    //                     }
    //             }
    //         else{}

    //     }
}


void addFileToIndex()
{

    std::ofstream outfile;

    outfile.open((repoDataPath+"/DataRaw/fileIndex.dat").c_str(), std::ios_base::app); // append line to file
    outfile << fullOutPath<<"\t"<<muonTrack<<"\t"<<GEM_type<<"\t"<<pressureTorr<<"\t"<<temperatureKelvin<<"\t"<<ArgonFraction<<"\t"<<CO2Fraction<<"\t"<<"\t"<<N2Fraction<<"\t"<<O2Fraction<<"\t"<<H2OFraction<<"\t";
    if(muonTrack)
        outfile<<muonEnergy<<endl;
    else
        outfile<<"-"<<endl;


    //TODO implement error managment
    //if(succed)
    cout<<repoDataPath<<"DataRaw/fileIndex.dat"<<" updated"<<endl;
    // else
    //     {
    //         throw std::runtime_error("Unable to update "+repoDataPath+"DataRaw/fileIndex.dat\nExiting");
    //     }


}


bool makeDir(string path){
    DIR *pDIR;
    if( (pDIR=opendir(path.c_str())) )
        {
            if(debug)std::cout << "Directory: \n\t" << path << "\nAlready exsists" << std::endl;
        }
    else
        {
            //Directory created with success
            if(system(("mkdir -p " + path).c_str())==0)
                {
                    if (debug) std::cout << "Directory created: \n\t" << path << std::endl;
                }
            //Directory creation failed
            else
                {
                    std::cout << "There was an error creating the directory:\n\t" << path << "\nEXITING"<<std::endl;
                    return false;
                }
        }
    return true;
}


void PrintPrimariesSummary(int numberOfLines, bool I_want_to_copy_paste=false)
{        // Print Summary of the avalanches

    std::cout << "\n#######################    Primary Electrons Summary    #######################" <<std::endl;
    TablePrinter tp(&std::cout);
    tp.AddColumn("Prim. Elec.", 12); //Column title , column width in number of char
    tp.AddColumn("Cluster", 7);
    tp.AddColumn("x0(um)", 8);
    tp.AddColumn("y0(um)", 8);
    tp.AddColumn("z0(mm)", 8);
    tp.AddColumn("Elec Prod", 8);
    tp.AddColumn("TotElec", 8);
    tp.AddColumn("Ions Prod", 8);
    tp.PrintHeader();
    int temp_cluster=0;
    for(int h = 0; h < numberOfLines; h++)
        {
            if (!I_want_to_copy_paste){
                if(temp_cluster!=trackElectronCollection[h].clusternumber)
                    {tp.PrintFooter(); temp_cluster =trackElectronCollection[h].clusternumber;}
                else{temp_cluster =trackElectronCollection[h].clusternumber;}

                tp << h+1 << trackElectronCollection[h].clusternumber+1
                   << trackElectronCollection[h].x*10000<< trackElectronCollection[h].y*10000
                   << trackElectronCollection[h].z*10<< trackElectronCollection[h].aval_ne
                   << trackElectronCollection[h].aval_np<< trackElectronCollection[h].aval_ni;
            }
            else{
                std::cout << h+1 << "\t"<<trackElectronCollection[h].clusternumber+1
                   << "\t"<< trackElectronCollection[h].x*10000<< "\t"<< trackElectronCollection[h].y*10000
                   << "\t"<< trackElectronCollection[h].z*10<< "\t"<< trackElectronCollection[h].aval_ne
                   << "\t"<< trackElectronCollection[h].aval_np<< "\t"<< trackElectronCollection[h].aval_ni
                          <<std::endl;

            }
        }
    tp.PrintFooter();
    tp << "" << ""<< ""<< ""<< ""<< ""<< elecID<< "";
    tp.PrintFooter();
}

int runSim(){

    
    // // // Load the field map.

    // // Silly way to do. The solution would be to delegate the fm creation to a function
    ComponentAnsys123* fm = new ComponentAnsys123();
    
    if(field_map_origin != "COMSOL")
        {
            fm->Initialise(ansysFilePath +"ELIST_" +ansysFileEnding +".lis",
                           ansysFilePath+"NLIST_" +ansysFileEnding +".lis",
                           ansysFilePath +"MPLIST_" +ansysFileEnding +".lis",
                           ansysFilePath+"PRNSOL_" +ansysFileEnding +".lis",
                           "mm");
            if(plotInducedSignal)
                {
                      fm->SetWeightingField(ansysFilePath+"WSOL_"+ansysFileEnding+".lis", "readout");
                }
        }
    else
        {
            delete fm;
            ComponentComsol* fm = new ComponentComsol();
            fm->Initialise(repoDataPath+"COMSOL/mesh_"+ansysFileEnding+".mphtxt",
                           repoDataPath+"COMSOL/Triple_GEM_dielectrics.dat",
                           repoDataPath+"COMSOL/volt_"+ansysFileEnding+".mphtxt"
                           );            
        }
            
    
    
    fm->EnableMirrorPeriodicityX();
    fm->EnableMirrorPeriodicityY();
    
    if(debug){
        fm->PrintRange();
            }

    //~ ComponentAnsys123* strips = new ComponentAnsys123();
    //strips->Initialise("/afs/cern.ch/user/m/moseidel/private/BachelorSimulation/Ansys/FinalAnsysFiles/ELIST_1L_StripsTest.lis", "/afs/cern.ch/user/m/moseidel/private/BachelorSimulation/Ansys/FinalAnsysFiles/NLIST_1L_StripsTest.lis", "/afs/cern.ch/user/m/moseidel/private/BachelorSimulation/Ansys/FinalAnsysFiles/MPLIST_1L_StripsTest.lis", "/afs/cern.ch/user/m/moseidel/private/BachelorSimulation/Ansys/FinalAnsysFiles/PRNSOL_1L_StripsTest.lis", "mm");
    //~ strips->SetWeightingField("/afs/cern.ch/user/m/moseidel/private/BachelorSimulation/Ansys/FinalAnsysFiles/PRNSOL_1L_StripsTest.lis","readout");
    //~ strips->EnableMirrorPeriodicityX();
    //~ strips->EnableMirrorPeriodicityY();
    //~ if(debug){
        //~ strips->PrintRange();
    //~ }

    // Dimensions of the GEM [cm]
    const double pitch = 0.014;



    double sensorx0, sensorx1, sensory0, sensory1, sensorz0=0, sensorz1=0;

    
    // Set Dimensions of Area of Detector, Sensor etc. GEM and BOX
    sensorx0 = -5*pitch;
    sensorx1 = 5*pitch;
    sensory0 = -5*pitch;
    sensory1 = 5*pitch;
    // setting z limit of the sensor based on the ansys file (TODO)
    sensorz0 = zAnode;
    sensorz1 = zDrift;



    //plots E-FEld and/or Potential
    //FieldCanvas = new TCanvas("EField","EField");
    if (0) {
        FieldCanvas->cd();
        ViewField* fieldView = new ViewField();
        fieldView->SetComponent(fm);
        fieldView->SetPlane(0, -1, 0, 0, 0, 0); //Normal vector in x, front view
        //~ fieldView->SetPlane(0., 0., -1., 0., 0., -0.2); //Normal vector in z, view from above
        fieldView->SetArea(-pitch , zAnode, pitch , zDrift);  //xmin,ymin,xmax,ymax //if normal vector in x or y
        //~ fieldView->SetArea(-pitch, -pitch, pitch, pitch);  //xmin,ymin,xmax,ymax //if normal vector in z
        fieldView->SetNumberOfContours(40);
        fieldView->SetVoltageRange(-3500., 0.);
        fieldView->SetElectricFieldRange(0,100000);
        fieldView->SetCanvas(FieldCanvas);
        fieldView->PlotContour("e"); //"e" plots electric field, "v" plots potential.
    }

    //Now get the Electric Field at middle and close to the edge of the hole.
    if(getElectricFieldHole){
    // pointer to the medium at the given position, just a placeholder here
    Medium * helpMedium = new Medium();
    double centerx = 0.;
    double centery = 0.;
    double centerz = zGem3;
    double topx = 0.0;
    double topy = 0.0;
    double topz = centerz+0.0025;
    double bottomx = 0.0;
    double bottomy = 0.0;
    double bottomz = centerz-0.0025;
    double outerx = MidRadius - 0.0001; 
    double outery = 0.;
    double outerz = 0.0;
    double centerEx, centerEy, centerEz;
    double outerEx, outerEy, outerEz;
    double topEx, topEy, topEz;
    double bottomEx, bottomEy, bottomEz;
    int centerStatus, outerStatus, topStatus, bottomStatus;

    fm->ElectricField(centerx,centery,centerz, centerEx, centerEy, centerEz, helpMedium, centerStatus);
    fm->ElectricField(outerx,outery, outerz, outerEx, outerEy, outerEz, helpMedium, outerStatus);
    fm->ElectricField(topx,topy, topz, topEx, topEy, topEz, helpMedium, topStatus);
    fm->ElectricField(bottomx,bottomy, bottomz, bottomEx, bottomEy, bottomEz, helpMedium, bottomStatus);

    //Compute resulting Electric field. (magnitude of electric field)
    double centerEField = sqrt(pow(centerEx,2)+pow(centerEy,2)+pow(centerEz,2));
    double outerEField = sqrt(pow(outerEx,2)+pow(outerEy,2)+pow(outerEz,2));
    double topEField = sqrt(pow(topEx,2)+pow(topEy,2)+pow(topEz,2));
    double bottomEField = sqrt(pow(bottomEx,2)+pow(bottomEy,2)+pow(bottomEz,2));
    std::cout << "E-Field hole center: " << centerEField/1000<< "kV/cm"  << std::endl;
    std::cout << "E-Field kapton nose: " << outerEField/1000 << "kV/cm" << std::endl;
    std::cout << "E-Field top middle: " << topEField/1000 << "kV/cm" << std::endl;
    std::cout << "E-Field bottom middle: " << bottomEField/1000<< "kV/cm"  << std::endl;


    // // Uncomment to store E-filed values
    // std::ofstream outfile;
    // outfile.open(field_map_origin+".txt", std::ios_base::app);

    // for(int k = 0; k < 7000; k++)
    //     {
    //         centerz=zDrift-(zDrift-zAnode)*k/7000;
    //         fm->ElectricField(centerx,centery,centerz, centerEx, centerEy, centerEz, helpMedium, centerStatus);
    //         double centerEField = sqrt(pow(centerEx,2)+pow(centerEy,2)+pow(centerEz,2));
    //         outfile << (zDrift-zAnode)*k/0.7<<"\t"<<centerEField<<"\n"; 
    //     }
    }

    
    // Setup the gas.
    MediumMagboltz* gas = new MediumMagboltz();
    gas->SetComposition("ar", ArgonFraction, "co2", CO2Fraction, "n2", N2Fraction, "o2", O2Fraction , "h2o", H2OFraction);  //Wird auf 1 normiert.
    gas->SetTemperature(temperatureKelvin);
    //~ const double faktorhPaInTorr = 0.75006375541921;
    gas->SetPressure(pressureTorr); //in torr

    gas->EnableDebugging();
    gas->Initialise();
    gas->DisableDebugging();

    // Set the Penning transfer efficiency.
    const double rPenning = 0.57; //hierzu noch was raussuchen //jetzt mal 0.57 probieren, sonst 0.51
    //~ const double rPenning = 0.3; //hierzu noch was raussuchen
    const double lambdaPenning = 0.;
    gas->EnablePenningTransfer(rPenning, lambdaPenning, "ar");
    // Load the ion mobilities.
    gas->LoadIonMobility(GarfieldPath + "/Data/IonMobility_Ar+_Ar.txt");
    // Associate the gas with the corresponding field map material.
    const unsigned int nMaterials = fm->GetNumberOfMaterials();
    for (unsigned int i = 0; i < nMaterials; ++i) {
        const double eps = fm->GetPermittivity(i);
        if (eps == 1.) fm->SetMedium(i, gas);
    }
    if(debug){
        fm->PrintMaterials();
    }


    // Create the sensor.
    Sensor* sensor = new Sensor();
    //create second sensor that only contains readout?
    sensor->AddComponent(fm);


    sensor->SetArea(sensorx0, sensory0, sensorz0, sensorx1, sensory1,  sensorz1);

    AvalancheMicroscopic* aval = new AvalancheMicroscopic(); //Electrons

    //Enable the storage of driftlines in the AvalancheMicroscopic class
    if(storeElectronsPositions)
        aval->EnableDriftLines();

    aval->SetSensor(sensor);
    ViewSignal* signalView = new ViewSignal();
    if(plotInducedSignal)
        {
            sensor->AddElectrode(fm, "readout");
            aval->EnableSignalCalculation();
            sensor->SetTimeWindow(0.,1., MaxTimeSignal);
            signalView->SetSensor(sensor);
        }


    //Ions
    AvalancheMC* drift = new AvalancheMC();
    drift->SetSensor(sensor);
    if(plotInducedSignal)
        drift->EnableSignalCalculation();
    // at each step, the ions will be propagated by 1μm in the direction
    // of the drift velocity at the local field followed by a randomstep based on the diffusion coefficient.
    drift->SetDistanceSteps(2.e-4);

    ViewDrift* driftView = new ViewDrift();
    if (plotDrift) {
        driftView->SetArea(sensorx0, sensory0, sensorz0, sensorx1, sensory1,  sensorz1);
        //~ driftView->SetArea(-10 * pitch, -10 * pitch, -0.25, 10 * pitch,  10 * pitch,  0.25);
        aval->EnablePlotting(driftView); //Plot electrons avalanche
        //drift->EnablePlotting(driftView); //Plot Ions drift
    }

    int numberClusters = 0;
    ViewDrift* driftViewTrack = new ViewDrift();


    int nEvents;
    //Simulate single electron in drift. No muon track simulated
    if(!muonTrack){nEvents = 1;}

    //Simulate muon track
    else
        {
        //~ // Create the Track of the particle
        //Gives back primaries and secondaries positions, but with 0 energy
        TrackHeed *heed = new TrackHeed();
        //~ heed->DisableDeltaElectronTransport(); //Gives back only the primaries, but with a starting energy
        heed->EnableDeltaElectronTransport();
        heed->SetParticle("muon");
        //~ heed->SetMomentum(muonEnergy); //does not work
        heed->SetEnergy(muonEnergy); //this works
        heed->SetSensor(sensor);        
        //~ beta = 0.99;
        //~ heed->SetBeta(beta);

        //Randomize intial position of the muon (cm units)
        x0Track = pitch;
        y0Track = -pitch/2. + RndmUniform() * pitch;
        z0Track = zDrift-0.0001;

        t0Track = 0.0;

        dx0Track = -0.0; // The impulses are relative to each other
        dy0Track = 0.0;
        dz0Track = -1.;
        dTrackMagnitude = sqrt(pow(dx0Track,2)+pow(dy0Track,2)+pow(dz0Track,2));

        std::cout << "\nStarting Muon Energy: " << muonEnergy/1e9 << " GeV"<<std::endl;
        std::cout << "Starting Muon(x,y,z): (" << x0Track*10000<< ", "<<y0Track*1000 <<", " <<z0Track*10000 <<") um"<<std::endl;
        std::cout << "Muon Momentum versor: (" << dx0Track/dTrackMagnitude<< ", "<<dy0Track/dTrackMagnitude <<", " <<dz0Track/dTrackMagnitude <<")"<<std::endl;
        /***************/ //Photon ionization
        //~ int nel; //Danach einkommentieren */ in 509,305,281,276,346,348 //EnableDelta wichtig.
        //~ TH1D *transportPhoton = new TH1D("transportPhoton","transportPhoton",400,0,400);
        //~ for(int i=0;i<5000000;i++){
            //~ if (i % 1000 == 0) printf ("."); fflush(0);
            //~ heed->TransportPhoton(x0Track,y0Track,z0Track,t0Track,8040,dx0Track,dy0Track,dz0Track,nel);
            //std::cout << "Produced in Photon Collision: " << nel << std::endl;
            //~ transportPhoton->Fill(nel);
        //~ }
        //~ TFile *outputfile = new TFile("TransportPhoton.root","RECREATE");
        //~ outputfile->cd();
        //~ transportPhoton->Write();
        /************/

        if(plotDriftTrack){
            driftViewTrack->SetArea(sensorx0, sensory0, sensorz0, sensorx1, sensory1,  sensorz1);
            heed->EnablePlotting(driftViewTrack);
        }

        // Initialize my muon
        heed->NewTrack(x0Track, y0Track, z0Track, t0Track, dx0Track, dy0Track, dz0Track);


        //Now there are many clusters, each cluster is one ionization step. If list end then GetCluster return false.
        bool moreCluster = true;
        //Loop over the clusters
        while(true){
            double xcls, ycls, zcls, tcls;
            double e, extra;
            int n;
            CLUSTER cluster;
            moreCluster = heed->GetCluster(xcls, ycls, zcls, tcls, n, e, extra); //Always gets the next cluster.
            if(moreCluster==false){break;} // no more cluster in the
                                           // heed track
            cluster.xcls = xcls; cluster.ycls = ycls; cluster.zcls = zcls; cluster.tcls = tcls;
            cluster.n = n; cluster.e = e; cluster.extra = extra;
            clusterCollection.push_back(cluster);
            //~ std::cout << "Number of produced electrons in current cluster: " << n << std::endl;
            //Call heed->GetElectron on present cluster to get all electrons
            for(int i=0;i<n;i++){
                double x,y,z;
                double t,e;
                double dx,dy,dz;

                heed->GetElectron(i, x, y, z, t, e, dx, dy, dz);

                TRACKELECTRON trackElectron;
                trackElectron.x = x;trackElectron.y = y;trackElectron.z = z;
                trackElectron.t = t;trackElectron.e = e;
                trackElectron.dx = dx;trackElectron.dy = dy;trackElectron.dz = dz;
                trackElectron.clusternumber = numberClusters;
                //~ trackElectron.n = n;
                //trackElectronCollection consists of all loccations,
                //time and energy of the primary electrrons created by the muon.
                trackElectronCollection.push_back(trackElectron);
            }
            numberClusters++;
        }
        // exit if the muons doesn't release primaries
        if(numberClusters == 0){
            std::cout << "No Electrons Produced by muon!\n"
                      << "Exiting!" << std::endl;
            return 5;
        }

        std::cout << "\nIn " << numberClusters << " Collisions " << trackElectronCollection.size() << " primary electrons were produced!" << std::endl;


        nEvents = trackElectronCollection.size();
        std::cout<<"nEvents = "<< nEvents << std::endl;
    }


    // nEvents = 1  when no muon track is simulated
    //         = number of total primary electrons otherwise
    for (int i = 0; i < nEvents ;i++) {
        if (debug || i % 1 == 0) std::cout <<"Avalanching electron "<< (i+1) << "/" << nEvents<<std::endl;
        // Electron initial position in the cluster
        double x0=0,y0=0,z0=0,t0=0,e0=0,dx0=0,dy0=0,dz0=0;
        if(muonTrack)//the avalanches of all electrons are computed
            {
                // std::cout << "Electron " << i+1
                //           << ", in the cluster n."<< trackElectronCollection[i].clusternumber+1
                //           << " starts at z = " << trackElectronCollection[i].z
                //           << " cm" << std::endl;
                x0 = trackElectronCollection[i].x;
                y0 = trackElectronCollection[i].y;
                z0 = trackElectronCollection[i].z; // z in cm
                t0 = trackElectronCollection[i].t;  //t in ns.
                e0 = trackElectronCollection[i].e;
                dx0 = trackElectronCollection[i].dx;
                dy0 = trackElectronCollection[i].dy;
                dz0 = trackElectronCollection[i].dz;
            }
        else //electrons avalanches are not computed.
            {

            // This selects randomly an energy out of the distribution that was computed by only using Muons (with AvalancheSizeLimit).
            // We still have the problem with the single Bin peak at 200. Removed in the do while loop.
            //THIS CAN BE SKIPPED IF ONLY SECONDARY SIMULATION IS CARRIED OUT.
            /*********
            // (legacy)Cannot be read. Here the file has to be brought over. // Activate one of these two, depending on what you want to do!
            TFile* inputFile2 = new TFile(randomEnergyHistogram.c_str(), "READ" );
            TH1D* energyDist = (TH1D*) inputFile2->Get("singleHistos/energyDistOnlyPrimary");
            //~ TFile* inputFile2 = new TFile("/afs/cern.ch/user/m/moseidel/private/BachelorSimulation/Gem/FromHomeComputer/Analysis/AnalysisFiles/0619_1L_SimElec_Impure1PercentAir_AnalysisOutputAll.root", "READ" );
            //~ TH1D* energyDist = (TH1D*) inputFile2->Get("singleHistos/energyOfElecAtBottom");
            gRandom = new TRandom3(0);

            //Energy directly from hist
            double energyFromHisto;
            //energyFromHisto = energyDist->GetRandom(); //hiermit peak nicht aussparen

            //Energy from landau distibution with MPV and Sigma

            TF1 *landau = new TF1("landau","TMath::Landau(x,25,13)",0,1000); //Es macht einen Unterschied aus was man sampled! 0,1000 und 0,2000 grosser Unterschied! //Auf 3.48 P&S optimieren
            energyFromHisto = landau->GetRandom(); // Soll man MPV oder Sigma beibehalten oder beides aendern? Zunaechst nur MPV aendern.

            energyFromHisto = 0; //Electron without an energy

            //MPV verscheibt Paek zu sehr, lieber an sigma schrauben um den Peak etwas breiter zu machen, eher sehr viel breiter
            //std::cout << "energyFromHisto ist: " << "energyFromHisto" << std::endl;
            //energyFromHisto = 1; //Wieder zuruecksetzen vor richtiger Sim.

            //~ do{
                //~ energyFromHisto = energyDist->GetRandom();
            //~ } while(energyFromHisto > 199.8 && energyFromHisto < 201.2); //umgeht unphysikalischen Peak bei 200eV der nur von der simulation kommt.
            ************/
            double energyFromHisto = 2; //If only secondary is simulated.
            x0 = -pitch/2. + RndmUniform() * pitch;
            y0 = -sqrt(3)*pitch/2. + RndmUniform() * sqrt(3)*pitch;
            z0 = zDrift-0.05; //Starting z coordinate of the electron = 0.5 mm below the driftboard
            t0 = 0.0;
            e0 = energyFromHisto; //This is 0 if only secondaries are simulated.
            dx0 = 0.0;
            dy0 = 0.0;
            dz0 = -1;
            cout <<"\nPrimary electron position\t ("<<x0<<","<<y0<<","<<z0<<")"<<endl;

            TRACKELECTRON trackElectron;
            trackElectron.x = x0; trackElectron.y = y0;trackElectron.z = z0;
            trackElectron.t = t0;trackElectron.e = e0;
            trackElectron.dx = dx0;trackElectron.dy = dy0;trackElectron.dz = dz0;
            trackElectron.clusternumber = 0;

            trackElectronCollection.push_back(trackElectron);

            x0 = trackElectronCollection[i].x;
            y0 = trackElectronCollection[i].y;
            z0 = trackElectronCollection[i].z; // z in cm
            t0 = trackElectronCollection[i].t;  //t in ns.
            e0 = trackElectronCollection[i].e;
            dx0 = trackElectronCollection[i].dx;
            dy0 = trackElectronCollection[i].dy;
            dz0 = trackElectronCollection[i].dz;
        }
        if(!muonTrack)std::cout << "Energy of the Primary Electron: " << e0 << "eV" << std::endl;
        aval->AvalancheElectron(x0, y0, z0, t0, e0, dx0, dy0, dz0);


        // Plot the induced current.
        //Problem: Current also induced on foils.
        // if(plotInducedSignal){
        //     SignalCanvasPrevious = new TCanvas();
        //     ViewSignal* signalViewPrevious = new ViewSignal();
        //     signalViewPrevious->SetCanvas(SignalCanvasPrevious);
        //     signalViewPrevious->SetSensor(sensor);
        //     signalViewPrevious->PlotSignal("readout", false, true);
        //     aval->EnableSignalCalculation(false); // Maybe not really necessary
        //     sensor->ClearSignal(); //Clears the induced Signal
        // }

        aval->GetAvalancheSize(aval_ne, aval_nh, aval_ni);

        // Total number of electrons
        //recombinated electrons are coutned
        np = aval->GetNumberOfElectronEndpoints();
        //std::cout << "NumberElecEnd: " << np << std::endl;


            trackElectronCollection[i].aval_ne = aval_ne;
            trackElectronCollection[i].aval_ni = aval_ni;
            trackElectronCollection[i].aval_np = np;

        for (int j = np; j--;) {
            // for the jth electron in the avalanche, track initial and
            // final position/energy/time/status
            elecID ++;
            aval->GetElectronEndpoint(j, xe1, ye1, ze1, te1, e1, xe2, ye2, ze2, te2, e2, dxe2, dye2, dze2, estatus);


            
            if(storeElectronsPositions)
                {// // Storing all the drift line points in a TTree
                    double xe,ye,ze,te;
                    numberOfPoints = aval->GetNumberOfElectronDriftLinePoints(j);
                    elecDriftLines.status = estatus;
                    elecDriftLines.PrimaryParent = i+1;
                    for (int nodp = 0; nodp <numberOfPoints; nodp++ )
                        {
                            aval->GetElectronDriftLinePoint(xe,ye,ze,te,nodp,j);
                            elecDriftLines.xe = xe;
                            elecDriftLines.ye = ye;
                            elecDriftLines.ze = ze;
                            elecDriftLines.te = te;
                            elecDriftLines.elecID = elecID;
                            driftLinesTree->Fill();

                            // // Modification on the fly. To make the gif one needs the vector
                            if(plotGifAndFields)
                                electronDrift.push_back({xe,ye,ze,te});
                        }
                }

            // For each (even if recombined) electron there must be an ion
            // Let's drift that ion based on the electron starting position
            drift->DriftIon(xe1, ye1, ze1, te1);
            // Let's take the final position of the ion
            drift->GetIonEndpoint(0, xi1, yi1, zi1, ti1, xi2, yi2, zi2, ti2, istatus);


           

            //Avalanche Radius Calculation:
            radius = pow((xe2-x0),2)+pow((ye2-y0),2); //don't needed


            //Fill tree
            electron.x1 = xe1; electron.y1 = ye1; electron.z1 = ze1;
            electron.x2 = xe2; electron.y2 = ye2; electron.z2 = ze2;
            electron.dx2 = dxe2; electron.dy2 = dye2; electron.dz2 = dze2;
            electron.time1 = te1; electron.time2 = te2;
            electron.energy1 = e1; electron.energy2 = e2;
            electron.radius = radius;
            electron.status = estatus;
            electron.PrimaryParent = i+1;

            ion.x1 = xi1; ion.y1 = yi1; ion.z1 = zi1;
            ion.x2 = xi2; ion.y2 = yi2; ion.z2 = zi2;
            ion.time1 = ti1; ion.time2 = ti2;
            ion.status = istatus;

            //~ std::cout << "Impulse:   x: " << dx0 << "  y: " << dy0 << "  z: " << dz0 << std::endl;

            info.cnt = j;
            info.numberElec = aval_ne; info.numberIon = aval_ni; info.numberElecAll = np;
            info.x0 = x0; info.y0 = y0; info.z0 = z0; info.dx0 = dx0; info.dy0 = dy0; info.dz0 = dz0;
            info.t0 = t0; info.e0 = e0;

            if(muonTrack){
                muon.beta = beta;
                muon.x0Track = x0Track; muon.y0Track = y0Track; muon.z0Track = z0Track;
                muon.t0Track = t0Track;
                muon.dx0Track = dx0Track; muon.dy0Track = dy0Track; muon.dz0Track = dz0Track;
                muon.numberClusters = numberClusters; muon.numberElecTrack = trackElectronCollection.size();

            }

            t1->Fill();
        }



    }


    if(plotInducedSignal)
        {
            inducedSignal = new TCanvas("Signal","Signal");
            signalView->SetCanvas(inducedSignal);
            signalView->PlotSignal("readout"); // Plot the signal as Histogram
            double value;
            for (int i = 0; i < int(MaxTimeSignal); i++) // for bin in the signal, store Induced Current 
                {
                    value = sensor->GetSignal("readout", i);
                    value = value * 1E-15; // Signal amplitude is expressed in fC. Transforming value in C!
                    // cout<<"IONE =  " << sensor -> GetIonSignal("readout", i) << endl;
                    // cout<<"Electron =  " << sensor -> GetElectronSignal("readout", i) << endl;
                    signalIntegral+= 1.*value;    //Base of the bin x Bin content
                    if(value < threshold)
                        {
                            timeBin_over_the_threshold.push_back(i);
                            cout<<"Time " << i << " over threshold"<<endl;
                            cout<<"Value  " << value << " charge"<<endl;
                        }
                    
                    if(TMath::Abs(value) < 1E-10) value = 0.0;
                    electrodeSignal.push_back(value);

                }
            cout<<"Integrate = " << signalIntegral << " fC" << endl;
            if(timeBin_over_the_threshold.size()>1) 
                {
                    signalWidth = timeBin_over_the_threshold.back()-timeBin_over_the_threshold.front();
                    cout<<"TimeWidth = " << signalWidth <<endl;

                }
            t2->Fill();
        }
    
    

    if(plotMesh || storeElectronsPositions) // We need the  Canvas with the mesh if we want to storeElectronsPositions
        {
            MeshCanvas = new TCanvas("Mesh","Mesh",1920,1080);
            ViewFEMesh* meshView = new ViewFEMesh();
            meshView->SetArea(-5 * pitch, -5 * pitch, zAnode , 
                              5 * pitch,  5 * pitch, zDrift);
            meshView->SetCanvas(MeshCanvas);
            meshView->SetComponent(fm);
            meshView->SetPlane(0, -1,0, 0, 0, 0);
            meshView->SetFillMesh(true);
            meshView->SetColor(0, kYellow);
            // Set the color of the kapton.
            meshView->EnableAxes();
            meshView->SetXaxisTitle("x [cm]");
            meshView->SetYaxisTitle("z [cm]");
            //meshView->SetViewDrift(&driftView);
            meshView->Plot();
        }

    if(plotGifAndFields)
        {
            if(!muonTrack)
                {
                    AvalancheGif* gif = new AvalancheGif("./",fm,electronDrift,electrodeSignal,TripleGEM);
                    gif->MakeGif();
                }
            if(muonTrack)
                {
                    AvalancheGif* gif = new AvalancheGif("./",fm,electronDrift,electrodeSignal,x0Track,dx0Track,z0Track,dz0Track,TripleGEM);
                    gif->MakeGif();
                }
        }

    
    //Print a summary of the primaries in table form
    PrintPrimariesSummary(nEvents);

    return 0;
}
