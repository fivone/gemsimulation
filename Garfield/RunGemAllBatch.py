#!/usr/bin/env python
import os, re
import commands
import math, time
import sys
import random
import subprocess
import numpy as np

print 
print 'START'
print 
########   YOU ONLY NEED TO FILL THE AREA BELOW   #########
########   customization  area #########

##########
# Example
#########
# NumberOfJobs = 10
# interval = 50
# 2 values of pressure
# 2 values of T
# 5 values of ANSYSFILE
## TOTAL = 20 different simulation conditions

## 10 cluster node involved for each condition
## Each produces 50 simulation .root files for each condition
## 500 Simulations for each conditions
## It means you will run 10x20 = 200 jobs that will produce 10000 simulation .root files


### CAREFULLY CHOSE WHAT YOU WANT TO SIMULATE, DON'T CLOG THE GRID

NumberOfJobs = 10  # number of cluster node to use (machines that will run the simulation executable)
interval = 2 # number of times that each job will execute the executable
job_flavour = "longlunch" #longlunch=2h, workday=8h

#~ espresso 	20min 	8nm
#~ microcentury 	1h 	1nh
#~ longlunch 	2h 	8nh
#~ workday 	8h 	1nd
#~ tomorrow 	1d 	2nd
#~ testmatch 	3d 	1nw
#~ nextweek 	1w 	2nw 

if os.getenv("REPODATAPATH"):
    eos_dir = str(os.getenv("REPODATAPATH")) #where your job should save the data and create the subfolders.
else:
    print("No env variable found for storage directory")
    print("Exit...")
    sys.exit()
if os.getenv("REPO_HOME"):
    afs_dir = str(os.getenv("REPO_HOME") + "/Garfield/") #where gemAllBatch.cpp and this file are located.
else:
    print("No env variable found for afs_dir (working directory)")
    print("Exit...")
    sys.exit()
if os.getenv("GARFIELD_HOME"):
    GARFIELD_HOME_Location = str(os.getenv("GARFIELD_HOME")) #To export GARFIELD_HOME and HEED_DATABASE
else:
    print("No env variable found for GARFIELD_HOME")
    print("Exit...")
    sys.exit()
if os.getenv("HOME"):
    home_dir = str(os.getenv("HOME")) #your home folder
else:
    print("No env variable found for home directory")
    print("Exit...")
    sys.exit()
########   customization end   #########

path = os.getcwd()

print
print

### Needs to be done only the first time running the script  ####
#~ os.system("mkdir -p " + afs_dir + "/Executables/")
#~ os.system("mkdir -p " + afs_dir + "/BatchScripts/")

jobCnt=0

# define everything in lists. Only vary one (otherwise computing time gets too long)

# date tag
dateTag = 'GemSimulation_{0}'.format(time.strftime("%Y-%m-%d_%H-%M-%S"))

# Automatically normalizes to 1.
### changed compared to previous versions
### MAKE SURE all lists with same length
ARGON = [70.]
CO2 =   [30.]
N2 =    [0.]
O2 =    [0.]
H2O =   [0.]

# Standard conditions
#~ PRESSURE = [760.] # in torr
#~ TEMPERATURE = [293.15] # in Kelvin

# CMS P5 conditions
PRESSURE = [723.] # in torr
TEMPERATURE = [297.1] # in Kelvin

# CMS GE1/1 magnetic field 3T and 8-9 deg polar angle alpha(E-B)
# CMS GE2/1 magnetic field 1.5 T and 2-15 deg polar angle alpha(E-B)
BMag = 3.0
PolarAngle = 9. 
#~ MAGNETICFIELD = [BMag*np.sin(PolarAngle * np.pi / 180.),0.,BMag*np.cos(PolarAngle * np.pi / 180.)]
MAGNETICFIELD = [0,0,0]


#~ ANSYSFILES=["_1L_70_70_53_85_85_DeltaV_394V"]
#~ ANSYSFILES=["_Hole_70_50_70_VD_664_VGEM_394_VIND_437_1L"]
ANSYSFILES=[
    #~ ### single layer
    #"Hole_70_50_70_VD_664_VGEM_394_VIND_437_1L",
    #~ "_Hole_70_53_70_VD_664_VGEM_394_VIND_437_1L",
    #~ "_Hole_70_53_85_VD_664_VGEM_394_VIND_437_1L",
    #~ "_Hole_70_50_85_VD_664_VGEM_394_VIND_437_1L",
    #~ "_Hole_85_53_70_VD_664_VGEM_394_VIND_437_1L",
    ### triple layer
    ## double mask
    #"Hole_70_50_70_VDRIFT_2500_3L",
    #"Hole_70_50_70_VDRIFT_2700_3L",
    #"Hole_70_50_70_VDRIFT_2900_3L",
    #"Hole_70_50_70_VDRIFT_3100_3L",
    #"Hole_70_50_70_VDRIFT_3290_3L",
    #"Hole_70_50_70_VDRIFT_3400_3L",
    # "TAMUQ_Hole_70_50_70_VDRIFT_2500_3L",
    # "TAMUQ_Hole_70_50_70_VDRIFT_2700_3L",
    #"TAMUQ_Hole_70_50_70_VDRIFT_2900_3L",
    # "TAMUQ_Hole_70_50_70_VDRIFT_3100_3L",
    #"TAMUQ_Hole_70_50_70_VDRIFT_3290_3L",
    # "TAMUQ_Hole_70_50_70_VDRIFT_3400_3L",

    ## DM OA
    #"Hole_85_50_70_VDRIFT_3290_3L",
    #"Hole_85_50_70_VDRIFT_3100_3L", 
    #"Hole_85_50_70_VDRIFT_2900_3L" ,
    #"Hole_85_50_70_VDRIFT_2950_3L" ,
    #"Hole_85_50_70_VDRIFT_2700_3L", 
    #"Hole_85_50_70_VDRIFT_2500_3L",
    

    ## BASIN EFFECT STUDY
    "Hole_85_50_70_VDRIFT_3281_3L",
    #"Hole_85_50_70_VDRIFT_3282_3L",
    #"Hole_85_50_70_VDRIFT_3283_3L",

    "Hole_85_50_70_VDRIFT_3351_3L",
    #"Hole_85_50_70_VDRIFT_3352_3L",
    #"Hole_85_50_70_VDRIFT_3353_3L",

    ## COMSOL
    #  "COMSOL_Hole_70_50_70_VDRIFT_2500_3L",
    # "COMSOL_Hole_70_50_70_VDRIFT_2600_3L",
    # "COMSOL_Hole_70_50_70_VDRIFT_2700_3L",
    # "COMSOL_Hole_70_50_70_VDRIFT_2800_3L",
    # "COMSOL_Hole_70_50_70_VDRIFT_2900_3L",
    
    ## DM OB
    #"Hole_70_50_85_VDRIFT_3290_3L",
    #"Hole_70_50_85_VDRIFT_3100_3L", 
    #"Hole_70_50_85_VDRIFT_2900_3L" ,
    # "Hole_70_50_85_VDRIFT_2700_3L ", 
    # "Hole_70_50_85_VDRIFT_2500_3L",


    #~ ## single mask (down)
    #~ "_Hole_70_53_85_VDRIFT_2500_3L",
    #~ "_Hole_70_53_85_VDRIFT_2700_3L",
    #"Hole_70_53_85_VDRIFT_2900_3L",
    #"Hole_70_53_85_VDRIFT_3100_3L",
    #"Hole_70_50_85_VDRIFT_3290_3L",
    #"_Hole_70_53_85_VDRIFT_3400_3L",
    #~ ## single mask (up)
    #~ "_Hole_85_53_70_VDRIFT_2500_3L",
    #~ "_Hole_85_53_70_VDRIFT_2700_3L",
    #~ "_Hole_85_53_70_VDRIFT_2900_3L",
    #~ "_Hole_85_53_70_VDRIFT_3100_3L",
    #~ "_Hole_85_53_70_VDRIFT_3290_3L",
    #~ "_Hole_85_53_70_VDRIFT_3400_3L",
    #~ ## VDRIFT variations
    #~ "_Hole_70_53_85_VDRIFT_2430V_DeltaVDRIFT_0V_3L",
    #~ "_Hole_70_53_85_VDRIFT_2440V_DeltaVDRIFT_10V_3L",
    #~ "_Hole_70_53_85_VDRIFT_2480V_DeltaVDRIFT_50V_3L",
    #~ "_Hole_70_53_85_VDRIFT_2530V_DeltaVDRIFT_100V_3L",
    #~ "_Hole_70_53_85_VDRIFT_2730V_DeltaVDRIFT_300V_3L",
    #~ "_Hole_70_53_85_VDRIFT_2930V_DeltaVDRIFT_500V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3130V_DeltaVDRIFT_700V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3200V_DeltaVDRIFT_770V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3330V_DeltaVDRIFT_900V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3530V_DeltaVDRIFT_1100V_3L",
    ## VGEM1 variations
    #~ "_Hole_70_53_85_VDRIFT_3170V_DeltaVGEM1_350V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3180V_DeltaVGEM1_360V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3190V_DeltaVGEM1_370V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3200V_DeltaVGEM1_380V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3210V_DeltaVGEM1_390V_3L",
    #~ "_Hole_70_53_85_VDRIFT_3220V_DeltaVGEM1_400V_3L",
]


varied = ANSYSFILES # also part of naming. Default: ANSYSFILES
    

for gasCnt in range(0, len(ARGON)):
    for pressureCnt in range(0, len(PRESSURE)):
        for temperatureCnt in range(0, len(TEMPERATURE)):
            for ansysCnt in range(0, len(ANSYSFILES)):
            
            
                variedCnt = ansysCnt # For naming purposes.
                NAME="%s"%(str(ANSYSFILES[ansysCnt]))
                
                hourTAG='{0}'.format(time.strftime("%H.%M.%S"))
                MUON="y" ### options: "muon", "gamma" for the HEED particles and "n" if you want to simulate only avalanche
                MuonEnergy=300000000 #300 MeV
                PenningTransfer = 0.57

                ######### Customization end

                #Make executable
                os.chdir(afs_dir)
                os.system("make gemAllBatch")

                #Move executable
                os.system("cp "+afs_dir+"/gemAllBatch "+afs_dir+"/Executables/gemAllBatch%s"%(str(NAME)))

                StartJobNumber = 0

                for x in range(StartJobNumber, int(NumberOfJobs)):
                    
                    RUN = x
                    ContinueFromJobNumber = 0

                    ##### creates jobs ####### makes bash file
                    with open(afs_dir+'/BatchScripts/job_%s_Run_'%(str(NAME)) + str(x) + ".sh", 'w') as fout:
                        
                        
                       ####### Write the instruction for each job
                      fout.write("#!/bin/sh\n")
                      fout.write("echo\n")
                      fout.write("echo %s_Run%s\n"%(NAME, str(x)))
                      fout.write("echo\n")
                      fout.write("echo 'START---------------'\n")				
                      fout.write("ITERATOR=\"%s\""%(str(ContinueFromJobNumber))+"\n")			  
                      fout.write("cd "+afs_dir+"/Executables"+"\n")

                      ## sourceing the right gcc version to compile the source code
                      fout.write("source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8.3.0.multilib/x86_64-centos7/setup.sh"+"\n")
                      fout.write("source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.04/x86_64-centos7-gcc48-opt/bin/thisroot.sh"+"\n")
                      fout.write("export GARFIELD_HOME="+GARFIELD_HOME_Location+"\n")
                      fout.write("export HEED_DATABASE=$GARFIELD_HOME/Heed/heed++/database/"+"\n")

                      #generate kerberos ticket to access EOS folder and link folder
                      fout.write("sleep 15\n")
                      fout.write("kinit -kt " +afs_dir+"/fivone.keytab  fivone\n")
                      fout.write("eosfusebind -g\n")

                    ## Run the same job interval number of time
                      fout.write("while [ ${ITERATOR} -lt %i ]"%(interval)+"\n")
                      fout.write("do"+"\n")
                      fout.write("echo \"$ITERATOR\"\n");
                      fout.write("ClusterId=$1\n")
                      fout.write("ProcId=$2\n")
                      fout.write("hourTAG=$(date +%H.%M.%S)\n");
                      fout.write("\t"+"echo %s_Run%s_\"$ITERATOR\"\n"%(NAME, str(x)))
                      fout.write("\t"+"./gemAllBatch%s "%(str(NAME))+"\"$ClusterId\".\"$ITERATOR\"_\"$hourTAG\".root %s %s %d %d %d %d %d %d %d %d" %(str(MUON), str(ANSYSFILES[ansysCnt]), ARGON[gasCnt], CO2[gasCnt], N2[gasCnt], O2[gasCnt], PRESSURE[pressureCnt], TEMPERATURE[temperatureCnt], H2O[gasCnt], MuonEnergy)+"\n")
                      fout.write("\t"+"(( ITERATOR++ ))"+"\n")
                      fout.write("done"+"\n")

                      fout.write("cd "+afs_dir+"\n")
                      #Destroy kerberos ticket that allows to eccess EOS folder
                      fout.write("kdestroy\n") 
                      fout.write("echo 'STOP---------------'\n")
                      fout.write("echo\n")
                      fout.write("echo\n")
                      os.system("chmod 755 "+afs_dir+"/BatchScripts/job_%s_Run_"%(str(NAME)) + str(x) + ".sh")

                    ##### creates .sub file for condor submit ######
                    with open(afs_dir+'/BatchScripts/subfile_job_%s_Run_'%(str(NAME)) + str(x) + ".sub", 'w') as sout:
                        sout.write("executable              = job_%s_Run_"%(str(NAME)) + str(x)+".sh"+"\n")
                        sout.write("getenv                  = true"+"\n")
                        sout.write("arguments               = $(ClusterId) $(ProcId)"+"\n")
                        ## Black List of the machines not good for EOS mounting
                        sout.write("Rank = ((machine != \"lx3a20.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a23.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a48.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a55.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a09.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3a46.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a04.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b10.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b12.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b52.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b68.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b69.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b7.physik.rwth-aachen.de\")   && \\"+"\n")
                        sout.write("(machine != \"lx3b99.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lxcip06.physik.RWTH-Aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip30.physik.rwth-aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip50.physik.RWTH-Aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip51.physik.rwth-aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip52.physik.RWTH-Aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip53.physik.rwth-aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip55.physik.RWTH-Aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxblade01.physik.RWTH-Aachen.de\" )&& \\"+"\n")
                        sout.write("(machine != \"lxblade12.physik.RWTH-Aachen.de\" )&& \\"+"\n")
                        sout.write("(machine != \"lxblade26.physik.RWTH-Aachen.de\" )&& \\"+"\n")
                        sout.write("(machine != \"lxcip02.physik.rwth-aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lxcip03.physik.rwth-aachen.de\"   )&& \\"+"\n")
                        sout.write("(machine != \"lx1b19.physik.rwth-aachen.de\")&& \\"+"\n")
                        sout.write("(machine != \"lx1b32.physik.rwth-aachen.de\")&& \\"+"\n")
                        sout.write("(machine != \"lx1b34.physik.RWTH-Aachen.de\")&& \\"+"\n")
                        sout.write("(machine != \"lx1b40.physik.rwth-aachen.de\")&& \\"+"\n")
                        sout.write("(machine != \"lx1b10.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx1b27.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lxcip17.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lxcip54.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b44.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3a12.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3a49.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b15.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b20.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b24.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b33.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b34.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b41.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b47.physik.RWTH-Aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lx3b56.physik.rwth-aachen.de\")  && \\"+"\n")
                        sout.write("(machine != \"lxcip20.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip22.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip24.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip25.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip26.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip28.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip31.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip32.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip33.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip34.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a14.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a44.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a51.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3a54.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b07.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b70.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b71.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b72.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b73.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b74.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b75.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b76.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b77.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b78.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b79.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b08.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b81.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b82.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b83.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b85.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b86.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b87.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b88.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx3b89.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip13.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip14.physik.RWTH-Aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip15.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip16.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lxcip19.physik.rwth-aachen.de\") && \\"+"\n")
                        sout.write("(machine != \"lx1b16.physik.RWTH-Aachen.de\") && \\"+"\n")                        
                        sout.write("(machine != \"lxcip35.physik.rwth-aachen.de\"))"+"\n")
                        

                        sout.write("output                  = out.$(ClusterId).dat"+"\n")
                        sout.write("error                   = error.$(ClusterId).err"+"\n")
                        sout.write("log                     = log.$(ClusterId).log"+"\n")
                        sout.write("+JobFlavour             = \""+job_flavour+"\" "+"\n")
                        sout.write("queue"+"\n")
                          

                    ###### sends jobs ######
                    os.chdir(afs_dir+"/BatchScripts")
                    os.system("condor_submit "+afs_dir+"/BatchScripts/subfile_job_%s_Run_"%(str(NAME)) + str(x) + ".sub") 
                    print "job nr " + str(jobCnt) + " submitted"

                    os.chdir(path)
                    
                    jobCnt = jobCnt+1
   
print
print "your jobs:"
os.system("condor_q")
print
