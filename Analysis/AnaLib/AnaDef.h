#ifndef DEF_H
#define DEF_H

#include "TROOT.h"
#include "TFile.h"
#include "TString.h"
#include "TCanvas.h"
#include "TBranch.h"
#include "TChain.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include <TH1D.h>
#include <TF1.h>
#include "TROOT.h"
#include <TStyle.h>
#include <ctime>


#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <regex>

using namespace std;
//STRUCT
//Set up structs for different kinds of info
struct ELECTRON{
    double x1,y1,z1;
    double x2,y2,z2;
    double dx2, dy2, dz2;
    double time1, energy1;
    double time2, energy2;
    double radius;
    int status;
    int PrimaryParent;
};

struct ION{
    double x1,y1,z1;
    double x2,y2,z2;
    double time1;
    double time2;
    int status;
};

struct INFO{
    int cnt;
    int numberElec, numberIon, numberElecAll;
    double x0,y0,z0,dx0,dy0,dz0,t0,e0;
};

struct MUON{
    double beta;
    double x0Track, y0Track, z0Track;
    double t0Track;
    double dx0Track, dy0Track, dz0Track;
    int numberClusters, numberElecTrack;
};

struct SUMMARY{
    Double_t anodeTime=0.;
    Double_t anodeEnergy=0.;
    Double_t gain;
    Double_t extendedGain=0;
    int number_of_primaries=0;
    int number_of_clusters=0;
    int total_electrons=0;

    int tot_electron_lost=0;
    int electron_recombined=0;
    int electron_on_material=0;
    int electron_exited_simulated_region=0;
    int electron_other=0;
    
    int gem_1_top=0,gem_1_kap=0,gem_1_bot=0,gem_2_top=0,gem_2_kap=0,gem_2_bot=0,gem_3_top=0,gem_3_kap=0,gem_3_bot=0;
    int elec_inductiongap=0;
    int anode=0;
};

struct AVALANCHESIZE{
    double x;
    double y;
    double z;

    double x2;
    double y2;
    double z2;

    double avaldirection;
    double distance;
    double aval_xsize;
    double aval_ysize;
    double aval_radius;

    double gain;
};



//Func Definition
void printTime();
void fillChains();
bool newChainFile();
bool newchainIntegratedSignalFile();
struct SUMMARY CountTotalElectron(struct SUMMARY a);
struct SUMMARY CountAnode(struct SUMMARY a);
struct SUMMARY CountGEM1Top(struct SUMMARY a);
struct SUMMARY CountGEM1Bot(struct SUMMARY a);

void histoStyle(int a, TH1D* hist);
void writeFile();
void fitgain();


// Object definition
bool muonTrack;
bool singleElectron;


int i = 0;
int number_of_file;
int totalSimulatedElectrons;
int totalIntegratedSignals;
int gem_stages;
int Up_Radius,Mid_Radius,Bot_Radius;
int Vdrift;

double zDrift=0.4,zGem1,zGem2,zGem3,zAnode=-0.3;
double integratedSignal;
double signalWidth;
double anode_xAVG=0, anode_yAVG=0;
double anode_xSumSq=0, anode_ySumSq=0;

string foldername, fieldMapOrigin;
string data_path = getenv("REPODATAPATH");
string currentName="";
string currentNameIntegratedSignal="";
string dateSimu;

vector <string> files;


struct ELECTRON electron;
struct ION ion;
struct INFO info;
struct MUON muon;
struct SUMMARY Summary;
struct AVALANCHESIZE avalsize;


TH1D* xProduction = new TH1D("xProduction","x Production [cm]",100,-0.05,0.05);
TH1D* yProduction = new TH1D("yProduction","y Production [cm]",100,-0.05,0.05);
TH1D* zProduction = new TH1D("zProduction","z Production [cm]",5000,zAnode,zDrift);
TH1D* xCollection_mat = new TH1D("xCollection_mat","x Collection on material [cm]",100,-0.05,0.05);
TH1D* yCollection_mat = new TH1D("yCollection_mat","y Collection on material [cm]",100,-0.05,0.05);
TH1D* zCollection_mat = new TH1D("zCollection_mat","z Collection on material [cm]",5000,zAnode,zDrift);
TH1D* xCollection_rec = new TH1D("xCollection_rec","x Collection by recombination [cm]",100,-0.05,0.05);
TH1D* yCollection_rec = new TH1D("yCollection_rec","y Collection by recombination [cm]",100,-0.05,0.05);
TH1D* zCollection_rec = new TH1D("zCollection_rec","z Collection by recombination [cm]",5000,zAnode,zDrift);
TH1D* xCollection_ext = new TH1D("xCollection_ext","x exiting gas volume [cm]",100,-0.05,0.05);
TH1D* yCollection_ext = new TH1D("yCollection_ext","y exiting gas volume  [cm]",100,-0.05,0.05);
TH1D* zCollection_ext = new TH1D("zCollection_ext","z exiting gas volume [cm]",5000,zAnode,zDrift);
TH1D* xCollection_oth = new TH1D("xCollection_oth","x lost other [cm]",100,-0.05,0.05);
TH1D* yCollection_oth = new TH1D("yCollection_oth","y lost other [cm]",100,-0.05,0.05);
TH1D* zCollection_oth = new TH1D("zCollection_oth","z lost other [cm]",500,zAnode,zDrift);


TH1D* collecTimeDistr = new TH1D("collecTimeDistr","collection Time [ns]",100,0,100);
TH1D* collecEnergyDist = new TH1D("collecEnergyDistr","collection Energy [eV]",100,0,100);
TH1D* InitialClusters = new TH1D("InitialClusters","InitialClusters",100,3,3);
TH1D* PrimaryElectrons = new TH1D("PrimaryElectrons","PrimaryElectrons",100,3,3);

TFitResultPtr gainFit =0;
TFitResultPtr GainFromSignalFit =0;



TH1D* gain = new TH1D("gain","gain",100,3,3);
TH1D* gain_with_bottom = new TH1D("extendedGain","extendedGain",100,3,3);
TH1D* signalHeightDistribution = new TH1D("signalHeightDistribution","signalHeightDistribution",100,3,3);
TH1D* signalWidth_distr = new TH1D("pulseWidth","pulseWidth",30,-0.5*1E-9,29.5*1E-9);
TH1D* total = new TH1D("totalElectrons","totalElectrons",200,1,1);
TH1D* GEM1TopFractionLost = new TH1D("GEM1TopFractionLost","GEM1TopFractionLost",201,-0.0025,1.0025);
TH1D* GEM1KapFractionLost = new TH1D("GEM1KapFractionLost","GEM1KapFractionLost",201,-0.0025,1.0025);
TH1D* GEM1BotFractionLost = new TH1D("GEM1BotFractionLost","GEM1BotFractionLost",201,-0.0025,1.0025);
TH1D* GEM2TopFractionLost = new TH1D("GEM2TopFractionLost","GEM2TopFractionLost",201,-0.0025,1.0025);
TH1D* GEM2KapFractionLost = new TH1D("GEM2KapFractionLost","GEM2KapFractionLost",201,-0.0025,1.0025);
TH1D* GEM2BotFractionLost = new TH1D("GEM2BotFractionLost","GEM2BotFractionLost",201,-0.0025,1.0025);
TH1D* GEM3TopFractionLost = new TH1D("GEM3TopFractionLost","GEM3TopFractionLost",201,-0.0025,1.0025);
TH1D* GEM3KapFractionLost = new TH1D("GEM3KapFractionLost","GEM3KapFractionLost",201,-0.0025,1.0025);
TH1D* GEM3BotFractionLost = new TH1D("GEM3BotFractionLost","GEM3BotFractionLost",201,-0.0025,1.0025);
TH1D* AnodeFraction = new TH1D("AnodeFraction","AnodeFraction",201,-0.0025,1.0025);

TBranch* endLayer = new TBranch();
TBranch* startCoord = new TBranch();
TBranch* customBranch = new TBranch();
TTree* AvalancheSizeTree = new TTree("avalanchesize","avalanchesize"); 
TFile* currFile = new TFile();
TChain* chain = new TChain("t1");
TChain* chainIntegratedSignal = new TChain("t2");

TF1  * polya = new TF1("polya","[0]*TMath::Power((1+[2]),1+[2])/TMath::Gamma(1+[2])*TMath::Power((x/[1]),[2])*TMath::Exp(-(1+[2])*x/[1])",0,90000);



//FUNCTION IMPLEMENTATION
void fillChains(){
	TFile *f;
    //TTree *tptrsignal;
    TTree *tptrelectron;
    int countsSimuElectron = 0;
    //int countsSimuSignal = 0;
    for (int j=1; j<=number_of_file;j++)
        {
            chain->Add(files[j-1].c_str());
            chainIntegratedSignal->Add(files[j-1].c_str());
            f = TFile::Open(files[j-1].c_str(),"READ");
            tptrelectron = (TTree*)f->Get("t1");
            //tptrsignal = (TTree*)f->Get("t2");
            countsSimuElectron += tptrelectron->GetEntries();
            //countsSimuSignal += tptrsignal->GetEntries();
            f->Close();
                
            
            if (j%1000==0) 
                {
                    printTime();                   
                    cout << "Loading files... " << j << "/"<<number_of_file << "\tTotalElectrons = " << countsSimuElectron<<endl;
                }
        }

    printTime();
    cout << "Loading files... " << number_of_file << "/"<<number_of_file << "\tTotalElectrons = " << countsSimuElectron<<endl;

    totalSimulatedElectrons = countsSimuElectron;
    //totalIntegratedSignals = countsSimuSignal;
}
SUMMARY CountTotalElectron(SUMMARY a)
{
    a.total_electrons++;
    a.number_of_primaries=1;

    return a;
}
SUMMARY CountTotalLost(SUMMARY a)
{

    if (electron.status == -1){
        a.electron_exited_simulated_region++;
        xCollection_ext->Fill(electron.x2);
        yCollection_ext->Fill(electron.y2);
        zCollection_ext->Fill(electron.z2);
    }
    if (electron.status == -5){
        a.electron_on_material++;
        xCollection_mat->Fill(electron.x2);
        yCollection_mat->Fill(electron.y2);
        zCollection_mat->Fill(electron.z2);
    }
    if (electron.status == -7){
        a.electron_recombined++;
        xCollection_rec->Fill(electron.x2);
        yCollection_rec->Fill(electron.y2);
        zCollection_rec->Fill(electron.z2);        
            }
    if (electron.status != -7 && electron.status != -5 && electron.status != -1){
        a.electron_other++;
        xCollection_oth->Fill(electron.x2);
        yCollection_oth->Fill(electron.y2);
        zCollection_oth->Fill(electron.z2);        
            }
    return a;
}
SUMMARY CountAnode(SUMMARY a)
{
    if(electron.z2 < (zGem3-0.01)) // If it is in the inductiongap, it contributes to gain
        {
            a.elec_inductiongap++;
            a.gain = a.elec_inductiongap;
            a.extendedGain++;
            a.anodeTime += electron.time2;
            a.anodeEnergy += electron.energy2;

        }
    if(electron.z2 < (zAnode+0.004) && electron.status==-5) // only if it ends on the anode board 
        {
            a.anode++;
            anode_xAVG += electron.x2;
            anode_yAVG += electron.y2;

            anode_xSumSq += pow(electron.x2,2);
            anode_ySumSq += pow(electron.y2,2);
        }
    
    return a;
}

SUMMARY CountGEM1Top(SUMMARY a)
{
    if(electron.z2 <= zGem1+0.003 && electron.z2 > zGem1 +0.0025 && electron.status==-5  )
        {
            a.gem_1_top++;
        }
    return a;
}
SUMMARY CountGEM1Kap(SUMMARY a)
    {
    if(electron.z2 <= zGem1+0.0025 && electron.z2 > zGem1-0.0025 && electron.status==-5 )
    {
        a.gem_1_kap++;
    }
    return a;
}
SUMMARY CountGEM1Bot(SUMMARY a)
{
    if(electron.z2 <= zGem1-0.0025 && electron.z2 >=zGem1-0.003 && electron.status==-5 )
        {
            a.gem_1_bot++;
        }
    return a;
}

SUMMARY CountGEM2Top(SUMMARY a)
{
    if(electron.z2 <= zGem2+0.003 && electron.z2 >zGem2+0.0025 && electron.status == -5)
    {
        a.gem_2_top++;
    }
    return a;
}

SUMMARY CountGEM2Kap(SUMMARY a)
    {
    if(electron.z2 < zGem2+0.0025 && electron.z2 > zGem2-0.0025 && electron.status==-5 )
    {
        a.gem_2_kap++;
    }
    return a;
}

SUMMARY CountGEM2Bot(SUMMARY a)
{
    if(electron.z2 <= zGem2-0.0025 && electron.z2 >= zGem2-0.003 && electron.status==-5 ) //0.003 == 30 um
    {
        a.gem_2_bot++;
    }
    return a;
}


SUMMARY CountGEM3Top(SUMMARY a)
{
    if(electron.z2 <= zGem3+0.0030 && electron.z2 >zGem3+0.0025  && electron.status==-5)
    {
        a.gem_3_top++;
    }
    return a;
}

SUMMARY CountGEM3Kap(SUMMARY a)
    {
    if(electron.z2 < zGem3+0.0025 && electron.z2 > zGem3-0.0025 && electron.status==-5 ) // from kapton nose down to 40 um
    {
        a.gem_3_kap++;
        
    }
    return a;
}
SUMMARY CountGEM3Bot(SUMMARY a)
{
    if(electron.z2 <= zGem3-0.0025 && electron.z2 >=(zGem3-0.0030) && electron.status==-5) // from kapton nose down to 40 um
    {
        a.gem_3_bot++;
        a.extendedGain++;
        
    }
    return a;
}


void histoStyle(int a, TH1D* hist)
{
    hist->BufferEmpty();//SetAutorange on the x axis
    if(a ==1)
        {
            hist->SetFillColor(kMagenta);
            hist->SetLineColor(kMagenta+1);
            hist->SetFillStyle(3002);
        }

        if(a ==2)
        {
            hist->SetFillColor(kGreen);
            hist->SetLineColor(kGreen+1);
            hist->SetFillStyle(3002);
        }
        if(a ==3)
            {
                hist->SetFillColor(kBlack);
                hist->SetLineColor(kBlack);
                hist->SetFillStyle(3002);
            }
        if(a ==4)
        {
            hist->SetFillColor(kOrange+8);
            hist->SetLineColor(kOrange+9);
            hist->SetFillStyle(3002);
        }
}

//fit gain with a polya distribution
void fitgain()
{
    double mean = gain->GetMean();
    double rms= gain->GetRMS();
    double entries = gain->GetEntries();
    polya->SetParName(0,"Normaliz.");
    polya->SetParName(1,"#mu");
    polya->SetParName(2,"#mu^{2}/#sigma^{2} -1");
    //Rule of thumb
    polya->SetParameter(0,entries/40);
    polya->SetParameter(1,mean);
    polya->SetParameter(2,TMath::Power(mean/rms,2)-1);
    polya->SetLineColor(4);
    gainFit = gain->Fit("polya","LS","",0,mean*6);
    gPad->Modified();
    gPad->Update();

    mean = gain_with_bottom->GetMean();
    rms= gain_with_bottom->GetRMS();
    entries = gain_with_bottom->GetEntries();
    //Rule of thumb
    polya->SetParameter(0,entries/40);
    polya->SetParameter(1,mean);
    polya->SetParameter(2,TMath::Power(mean/rms,2)-1);
    polya->SetLineColor(4);
    gain_with_bottom->Fit("polya","L","",0,mean*6);
    gPad->Modified();
    gPad->Update();


    
    mean = signalHeightDistribution->GetMean();
    rms= signalHeightDistribution->GetRMS();
    entries = signalHeightDistribution->GetEntries();
    //Rule of thumb
    polya->SetParameter(0,entries/40);
    polya->SetParameter(1,mean);
    polya->SetParameter(2,TMath::Power(mean/rms,2)-1);
    polya->SetLineColor(4);
    GainFromSignalFit=signalHeightDistribution->Fit("polya","LS","",0,mean*6);
    gPad->Modified();
    gPad->Update();
}


void writeFile()
{
// TODO: add a log file to store Pressure/Temperature Gas Mixture git
// commit version of the analysis and other details

        gROOT->ForceStyle();
        TFile* outF = new TFile(Form("%sDataAna/%s_%s_%dV_Hole_%d_%d_%d.root",data_path.c_str(),fieldMapOrigin.c_str(),dateSimu.c_str(), Vdrift,Up_Radius,Mid_Radius,Bot_Radius),"RECREATE");
        outF->cd();


        TDirectory *dir = outF->mkdir("Summary");
        dir->cd();

        //t1->Write();
        AvalancheSizeTree->Write();
        gainFit->Write();
        GainFromSignalFit->Write();

        histoStyle(4,gain);
        histoStyle(4,gain_with_bottom);
        histoStyle(4,signalWidth_distr);
        histoStyle(4,signalHeightDistribution);
		histoStyle(4,total);

        histoStyle(4,GEM1TopFractionLost);
        histoStyle(4,GEM1KapFractionLost);
        histoStyle(4,GEM1BotFractionLost);
        histoStyle(4,GEM2TopFractionLost);
        histoStyle(4,GEM2KapFractionLost);
        histoStyle(4,GEM2BotFractionLost);
        histoStyle(4,GEM3TopFractionLost);
        histoStyle(4,GEM3KapFractionLost);
        histoStyle(4,GEM3BotFractionLost);
		histoStyle(4,AnodeFraction);
        histoStyle(4,InitialClusters);
        histoStyle(4,PrimaryElectrons);

		gain->Write();
        gain_with_bottom->Write();
        signalHeightDistribution->Write();
        signalWidth_distr->Write();
		total->Write();

        GEM1TopFractionLost->Write();;
        GEM1KapFractionLost->Write();
        GEM1BotFractionLost->Write();

        GEM2TopFractionLost->Write();
        GEM2KapFractionLost->Write();
        GEM2BotFractionLost->Write();

        GEM3TopFractionLost->Write();
        GEM3KapFractionLost->Write();
        GEM3BotFractionLost->Write();

		AnodeFraction->Write();
        if (muonTrack){
        InitialClusters->Write();
        PrimaryElectrons->Write();
        }

        TDirectory *dir2 = outF->mkdir("OverallDist");
        dir2->cd();

        histoStyle(1,xProduction);
        histoStyle(1,yProduction);
        histoStyle(1,zProduction);
        xProduction->Write();
        yProduction->Write();
        zProduction->Write();
        histoStyle(2,xCollection_rec);
        histoStyle(2,yCollection_rec);
        histoStyle(2,zCollection_rec);
        histoStyle(2,xCollection_mat);
        histoStyle(2,yCollection_mat);
        histoStyle(2,zCollection_mat);
        histoStyle(2,xCollection_ext);
        histoStyle(2,yCollection_ext);
        histoStyle(2,zCollection_ext);
        histoStyle(2,xCollection_oth);
        histoStyle(2,yCollection_oth);
        histoStyle(2,zCollection_oth);
        
        xCollection_mat->Write();
        yCollection_mat->Write();
        zCollection_mat->Write();
        xCollection_rec->Write();
        yCollection_rec->Write();
        zCollection_rec->Write();
        xCollection_ext->Write();
        yCollection_ext->Write();
        zCollection_ext->Write();
        xCollection_oth->Write();
        yCollection_oth->Write();
        zCollection_oth->Write();

        
        histoStyle(3,collecTimeDistr);
        histoStyle(3,collecEnergyDist);
        collecTimeDistr->Write();
        collecEnergyDist->Write();
        
		outF->Write();
        cout<<"Results stored here: "<<
            data_path+"DataAna/"+fieldMapOrigin+"_"+ dateSimu +"_"+ to_string(Vdrift)+"V_Hole_"+ to_string(Up_Radius)+"_"+ to_string(Mid_Radius)
            +"_"+to_string(Bot_Radius) +".root"<<endl;
}


bool newChainFile()
{

    if(chain->GetFile()->GetName() != currentName)
        {
        currentName = chain->GetFile()->GetName();
        return true;
    }
    else{
        return false;
    }


}


bool newchainIntegratedSignalFile()
{

    if(chainIntegratedSignal->GetFile()->GetName() != currentNameIntegratedSignal)
    {
        currentNameIntegratedSignal = chainIntegratedSignal->GetFile()->GetName();
        return true;
    }
    else{
        return false;
    }

}

 void printTime()
{
     time_t now = time(0);
     char* dt  = ctime(&now);
     *std::remove(dt, dt+strlen(dt), '\n') = '\0';
     cout << "["<<dt<<"] ";
}

#endif


