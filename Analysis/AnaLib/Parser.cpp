#include "Parser.h"

// constructor of AnaParser,
AnaParser::AnaParser(int argc, char** argv)
{
    parser(argc, argv);
}

// destructor, just an example
AnaParser::~AnaParser()
{

}

void AnaParser::parser(int argc, char** argv)
{
    DIR *pDIR;
    struct dirent *entry;

    // The program itself is the first argument, it is argv[0]
    if(argc > 2){
		foldername = argv[1];
        // collect all the simudate the user wants to analyze
        for (int k=2; k<argc; k++)
            {
                if(strcmp(argv[k], ".")==0)
                    {
                        simudate=".";
                        break;
                    }
                simudate_names.push_back(argv[k]);
                if(!(pDIR=opendir((foldername+argv[k]).c_str())))
                    {
                        cout << "directory " << foldername+argv[k]<< "\n does not exsist.\nExiting"<<endl;
                        throw std::invalid_argument( "Invalid input folder" );
                    }
                else
                    {
                        //foldername + simudate is a valid folder, continue   
                    }
            }

        //if foldername is a valid folder
            if ((pDIR=opendir((foldername).c_str())))
            {                
                // if user specifies . as simudate, analyze all of them
                if (simudate == ".")
                    simudate_names=getAllSimuDates(pDIR);
                else
                    {}

                //Print info to the user
                cout << "\nAnalyzing data from:"<<endl;
                for (int k = 0 ; k < int(simudate_names.size());k++)
                    cout <<foldername+simudate_names[k]<<endl;


                
                for (unsigned int v = 0; v<simudate_names.size(); v++ ) // loop over sub directories list)
                    {
                        path = foldername + simudate_names[v]+"/";
                        if (debug)
                            cout <<"Reading data from folder: "<< path << endl;
                        pDIR=opendir((path).c_str());
                        
                        //Read all filenames
                        while((entry=readdir(pDIR))){
                            // Skip the directory ./ and ../
                            if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 )
                                file_names.push_back((path+entry->d_name));
                        }
                        number_of_file = file_names.size();
                    }
                
                
                
                if(foldername.find("3L") != string::npos)
                    {
                        tripleGem.zDrift = 0.4;
                        tripleGem.zGem1 = 0.1;
                        tripleGem.zGem2 = 0.0;
                        tripleGem.zGem3 = -0.2;
                        tripleGem.zAnode = -0.3;
                        
                        number_of_layer = 3;
                    }
                

                if(foldername.find("TAMUQ") != string::npos)
                    {
                        field_map_origin = "TAMUQ";
                        
                        // TAMUQ is using -0.4 < z < 0.3
                        if(foldername.find("3L") != string::npos)
                            {
                                tripleGem.zDrift = 0.3;
                                tripleGem.zGem1 = 0.0;
                                tripleGem.zGem2 = -0.1;
                                tripleGem.zGem3 = -0.3;
                                tripleGem.zAnode = -0.4;
                            }
                    }
                else if(foldername.find("COMSOL") != string::npos)
                    {
                        field_map_origin = "COMSOL";
                        
                    }
                else
                    {
                        field_map_origin = "AACHEN";
                    }

                if(foldername.find("SingleElectron") != string::npos || foldername.find("MuonTrack")!= string::npos)
                    {
                        if(foldername.find("SingleElectron") != string::npos) SingleElectron = true;
                        else if(foldername.find("MuonTrack") != string::npos) MuonTrack = true;
                    }
                else
                    {
                        throw std::invalid_argument( "Neither SingleElectron nor MuonTrack folder found.\nExiting" );
                    }

                //Initializing gem HOLE//
                // Find all sequence of numbers that are preceeded by _
                // and have at least 2 numbers
                string pattern = "_([0-9]{2,})";
                regex reg1(pattern);
                sregex_iterator it(foldername.begin(), foldername.end(), reg1);
                sregex_iterator it_end;
                vector <int> values;
                while(it != it_end) {
                    smatch match = *it;
                    string temp = match.str();
                    temp.erase(0,1);
                    values.push_back(stoi(temp));
                    ++it;
                }

                hole.Up_Radius=values[0];
                hole.Mid_Radius=values[1];
                hole.Bot_Radius=values[2];

                Vdrift = values[3];

            }//closing folder name is a valid folder
        else
            {
                cout << "The folder\n" <<foldername << "\nis not found"<<endl;
                throw std::invalid_argument( "Invalid Folder" );
            }
    }

    else throw std::invalid_argument( "Missing argument: foldername" );

}


string AnaParser::GetDateSimu()
{
    return simudate_names[0];
}
int AnaParser::GetNumberOfLayer()
{
    return number_of_layer;
}
bool AnaParser::electron()
{
    return SingleElectron;
}
bool AnaParser::muonTrack()
{
    return MuonTrack;
}
HOLE* AnaParser::GetHole()
{
    return &hole;
}
GEM3L* AnaParser::GetTripleGem()
{
    return &tripleGem;
}

int AnaParser::GetVdrift()
{
    return Vdrift;
}
int AnaParser::GetEntries()
{
    return number_of_file;
}

vector<string> AnaParser::GetFileNames()
{
    return file_names;
}

string AnaParser::GetFolder()
{
    return foldername;
}

// return all the subfolder (except ./ and ../) in dir
vector<string> AnaParser::getAllSimuDates(DIR *dir)
{
    vector<string> out;
    struct dirent *entry = readdir(dir);
    while (entry != NULL)
    {
        if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 )
            out.push_back(entry->d_name);
        entry = readdir(dir);
    }

    closedir(dir);
    return out;
}

string AnaParser::GetFieldMapOrigin()
{
    return field_map_origin;

} 

void AnaParser::PrintSummary()
{
    if(MuonTrack)
        std::cout<<"Muon Tracks"  << endl;

    if(SingleElectron)
        std::cout<<"Single Electron"  << endl;

    std::cout<<"Number of simulations: " << number_of_file << endl;
    std::cout<<"Gem Layer(s): " << number_of_layer << endl;
    std::cout<<"Vdrift: " << Vdrift <<" V"<< endl;
    std::cout<<"Hole: " << hole.Up_Radius<<", "<<hole.Mid_Radius << ", "<< hole.Bot_Radius<< " um"<<endl;
}



// constructor of SimuParser,
SimuParser::SimuParser(int argc, char** argv)
{
    parser(argc, argv);
}

// destructor, just an example
SimuParser::~SimuParser()
{

}

void SimuParser::parser(int argc, char** argv)
{


    hostname = SetHostName();
    repoDataPath = getenv("REPODATAPATH");

    if(argc==12)
        {

            outFileName = argv[1];
            
            if ( strcmp(argv[2], "y") == 0)
                {
                    SingleElectron = false;
                    MuonTrack = true;    
                }
            else if ( strcmp(argv[2], "n") == 0)
                {
                    SingleElectron = true;
                    MuonTrack = false;    
                }
            else
                {
                    throw std::invalid_argument("\nInvalid invalid option on 2nd option [y,n].\nExiting");
                }

            fieldMapID = argv[3];
            ArFraction = atof(argv[4]);
            CO2Fraction = atof(argv[5]);
            N2Fraction = atof(argv[6]);
            O2Fraction = atof(argv[7]);
            pressureTorr = atof(argv[8]);
            tempKelvin = atof(argv[9]);
            H2OFraction = atof(argv[10]);
            muonEnergy = atoll(argv[11]);

            
            if(fieldMapID.find("3L") != string::npos)
                {
                    tripleGem.zDrift = 0.4;
                    tripleGem.zGem1 = 0.1;
                    tripleGem.zGem2 = 0.;
                    tripleGem.zGem3 = -0.2;
                    tripleGem.zAnode = -0.3;
                    
                    number_of_layer = 3;
                }
            else if(fieldMapID.find("1L") != string::npos)
                {
                    throw std::invalid_argument("1L simulation not fully implemented.\nExiting");
                }
            
            if(fieldMapID.find("TAMUQ") != string::npos)
                {
                    field_map_origin = "TAMUQ";
                        
                    // TAMUQ is using -0.4 < z < 0.3
                    if(fieldMapID.find("3L") != string::npos)
                        {
                            tripleGem.zDrift = 0.3;
                            tripleGem.zGem1 = 0.0;
                            tripleGem.zGem2 = -0.1;
                            tripleGem.zGem3 = -0.3;
                            tripleGem.zAnode = -0.4;
                        }
                }
            else if(fieldMapID.find("COMSOL") != string::npos)
                {
                    field_map_origin = "COMSOL";   
                }
            else
                {
                    field_map_origin = "AACHEN";
                }

            //Check if the field map does exsist
            checkFileExsistence();

            //Initializing gem HOLE//
            // Find all sequence of numbers that are preceeded by _
            // and have at least 2 numbers
            // TODO if stable, remove initial _
            string pattern = "_([0-9]{2,})";
            regex reg1(pattern);
            sregex_iterator it(fieldMapID.begin(), fieldMapID.end(), reg1);
            sregex_iterator it_end;
            vector <int> values;
            while(it != it_end) {
                smatch match = *it;
                string temp = match.str();
                //remove initial _
                temp.erase(0,1);
                values.push_back(stoi(temp));
                ++it;
            }
            
            hole.Up_Radius=values[0];
            hole.Mid_Radius=values[1];
            hole.Bot_Radius=values[2];
            
            Vdrift = values[3];
            
        }
    else
        {
            throw std::invalid_argument( "\nNot enough paramaters.\nExiting" );   
        }

}

void SimuParser::checkFileExsistence()
{
    if( field_map_origin == "AACHEN" || field_map_origin == "TAMUQ")
        {
            //sleep to ensure EOS is mounted properly
            sleep(15);
            ifstream ifile(repoDataPath + "Ansys/FinalAnsysFiles/ELIST_"+fieldMapID+".lis");
            ifstream ifile1(repoDataPath + "Ansys/FinalAnsysFiles/NLIST_"+fieldMapID+".lis");
            ifstream ifile2(repoDataPath + "Ansys/FinalAnsysFiles/MPLIST_"+fieldMapID+".lis");
            ifstream ifile3(repoDataPath + "Ansys/FinalAnsysFiles/PRNSOL_"+fieldMapID+".lis");
            if(!ifile || !ifile1|| !ifile2 || !ifile3 )
            {
                cout<<"No field map associated to this fieldMapID: "<< fieldMapID << "\nExiting"<<endl;
                if(!ifile)
                    cout << "Not found: "<<repoDataPath+"Ansys/FinalAnsysFiles/ELIST_"+fieldMapID+".lis"<<endl;
                if(!ifile1)
                    cout << "Not found: "<<repoDataPath+"Ansys/FinalAnsysFiles/NLIST_"+fieldMapID+".lis"<<endl;
                if(!ifile2)
                    cout << "Not found: "<<repoDataPath+"Ansys/FinalAnsysFiles/MPLIST_"+fieldMapID+".lis"<<endl;
                if(!ifile3)
                    cout << "Not found: "<<repoDataPath+"Ansys/FinalAnsysFiles/PRNSOL_"+fieldMapID+".lis"<<endl;
                throw std::invalid_argument("Field map found not found or incomplete");
            }
        }
    else if (field_map_origin == "COMSOL")
    {
        ifstream ifile(repoDataPath + "COMSOL/volt_"+fieldMapID+".mphtxt");
        if(!ifile)
            {
                cout<<"No field map associated to this fieldMapID: "<< fieldMapID << "\nExiting"<<endl;
                throw std::invalid_argument("Field map found not found or incomplete");
            }
    }                  
}


string SimuParser::SetHostName()
{
	FILE* F = popen("hostname","r");
	string hostname;
	char temp[128];
	
	while(fgets(temp, sizeof(temp),F)!=NULL)
	{
		hostname += temp;
	}

	pclose(F);
	return hostname;
}

bool SimuParser::electron()
{
    return SingleElectron;
}
bool SimuParser::muonTrack()
{
    return MuonTrack;
}
int SimuParser::GetNumberOfLayer()
{
    return number_of_layer;
}
int SimuParser::GetVdrift()
{
    return Vdrift;
}
long long int SimuParser::GetMuonEnergy()
{
    return muonEnergy;
}
HOLE* SimuParser::GetHole()
{
    return &hole;
}
GEM3L* SimuParser::GetTripleGem()
{
    return &tripleGem;
}

string SimuParser::GetFieldMapID()
{
    return fieldMapID;
}
string SimuParser::GetFieldMapOrigin()
{
    return field_map_origin;

} 
string SimuParser::GetOutFileName()
{
    return outFileName;
}
string SimuParser::GetHostName()
{
    return hostname;
}
double SimuParser::GetArFraction()
{
    return ArFraction;
}
double SimuParser::GetCO2Fraction()
{
    return CO2Fraction;
}
double SimuParser::GetN2Fraction()
{
    return N2Fraction;
}
double SimuParser::GetO2Fraction()
{
    return O2Fraction;
}
double SimuParser::GetH2OFraction()
{
    return H2OFraction;
}
double SimuParser::GetPressureTorr()
{
    return pressureTorr;
}
double SimuParser::GetTempKelvin()
{
    return tempKelvin;
}

void SimuParser::PrintSummary()
{
    std::cout<<"Muon Tracks:\t\t\t"<<MuonTrack<<endl;
    std::cout<<"Single Electron:\t\t"<<SingleElectron<< endl;
    std::cout<<"Gem Layers:\t\t\t" << number_of_layer << endl;
    std::cout<<"Vdrift:\t\t\t\t" << Vdrift <<" V"<< endl;
    std::cout<<"Hole:\t\t\t\t" << hole.Up_Radius<<"/"<<hole.Mid_Radius << "/"<< hole.Bot_Radius<< " um"<<endl;
    std::cout<<"ArFraction:\t\t\t"<<ArFraction<<endl;
    std::cout<<"CO2Fraction:\t\t\t"<<CO2Fraction<<endl;
    std::cout<<"N2Fraction:\t\t\t"<<N2Fraction<<endl;
    std::cout<<"O2Fraction:\t\t\t"<<O2Fraction<<endl;
    std::cout<<"H2OFraction:\t\t\t"<<H2OFraction<<endl;
    std::cout<<"Pressure:\t\t\t"<<pressureTorr<< " Torr"<<endl;
    std::cout<<"Temperature:\t\t\t"<<tempKelvin<< " °K"<<endl;
    std::cout<<"FieldMapType:\t\t\t"<<field_map_origin<<endl;
}
