#ifndef TESTF_H
#define TESTF_H

#include "TChain.h"


#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <dirent.h>
#include <regex>

using namespace std;

struct GEM3L{
    double zDrift,zGem1,zGem2,zGem3,zAnode;
	double GetzDrift(){return zDrift;}
	double GetzGem1(){return zGem1;}
	double GetzGem2(){return zGem2;}
	double GetzGem3(){return zGem3;}
	double GetzAnode(){return zAnode;}
};

struct HOLE {
    int Up_Radius,Mid_Radius,Bot_Radius;
	int GetUpRadius()  {return Up_Radius;}
	int GetMidRadius()  {return Mid_Radius;}
	int GetBotRadius()  {return Bot_Radius;}
};


class AnaParser                      // begin declaration of the class
{


  public:                      // begin public section
    AnaParser(int argc, char** argv);       // constructor
    AnaParser(const AnaParser& copy_from); //copy constructor
    AnaParser& operator=(const AnaParser& copy_from); //copy assignment
    ~AnaParser();                    // destructor

    void parser(int argc, char** argv);        // accessor function
    int GetNumberOfLayer();
    bool electron();
    bool muonTrack();
    string GetFolder();
    string GetFieldMapOrigin();
    string GetDateSimu();
	HOLE* GetHole();
    GEM3L* GetTripleGem();
    int GetVdrift();
    int GetEntries();
    vector<string> GetFileNames();
    void PrintSummary();


 private:                      // begin private section
    string foldername,simudate;                // member variable
    string field_map_origin;
    string path;
    int number_of_file=-1;
    int number_of_layer=0;
    int Vdrift=0;
    bool SingleElectron = false, MuonTrack = false;
    bool debug = false;

    vector<string> file_names;
    vector<string> simudate_names;
    vector<string> getAllSimuDates(DIR *dir);

    HOLE hole;
    GEM3L tripleGem;
};

class SimuParser                      // begin declaration of the class
{


  public:                      // begin public section
    SimuParser(int argc, char** argv);       // constructor
    SimuParser(const SimuParser& copy_from); //copy constructor
    SimuParser& operator=(const SimuParser& copy_from); //copy assignment
    ~SimuParser();                    // destructor


    bool electron();
    bool muonTrack();
    int GetNumberOfLayer();
    int GetVdrift();
    long long int GetMuonEnergy();
    double GetArFraction();
    double GetCO2Fraction();
    double GetN2Fraction();
    double GetO2Fraction();
    double GetH2OFraction();
    double GetPressureTorr();
    double GetTempKelvin();
    string GetFieldMapID();
    string GetFieldMapOrigin();
    string GetOutFileName();
	HOLE* GetHole();
    GEM3L* GetTripleGem();
    void PrintSummary();
    string GetHostName();

 private:                      // begin private section
    

    string repoDataPath;
    string outFileName;
    string fieldMapID;
    string hostname;
    string field_map_origin;
    bool SingleElectron = false, MuonTrack = false;    
    int number_of_layer=0;
    double ArFraction,CO2Fraction,N2Fraction,O2Fraction,H2OFraction;
    double pressureTorr, tempKelvin;
    long long int muonEnergy;
    int Vdrift;
    HOLE hole;
    GEM3L tripleGem;

    void parser(int argc, char** argv);        // accessor function
    void checkFileExsistence();
    string SetHostName();
};


#endif

