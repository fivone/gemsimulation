from os import listdir
from os.path import isfile, join
import ROOT
ROOT.gErrorIgnoreLevel = ROOT.kFatal

mypath="/eos/user/f/fivone/gemsimulation/DataRaw/SingleElectron/3L/Hole_70_50_70_VDRIFT_3400_3L/292K/"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
for num,i in enumerate(onlyfiles):
    file = ROOT.TFile.Open(mypath+i)
    if num%100 == 0:
        print num , " files processed"
    if file:
        file.Close()
    else:
        raise ValueError( mypath+i, " cannot be opened")
