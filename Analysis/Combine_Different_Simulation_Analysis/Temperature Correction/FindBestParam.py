import ROOT
import array
import csv
import os.path
import numpy as np
import math
import xlrd
import sys
### Let's add some more from different folder
framework = os.path.expandvars('$REPO_HOME')
sys.path.insert(1, framework+'/Analysis/AnaLib/')
import CMS_lumi, tdrstyle
import collections


####### Maps functions from here https://root-forum.cern.ch/t/loop-over-all-objects-in-a-root-file/10807/5
def Map(tf, browsable_to, tpath=None):
    """
    Maps objets as dict[obj_name][0] using a TFile (tf) and TObject to browse.
    """
    m = {}
    for k in browsable_to.GetListOfKeys():
        n = k.GetName()
        if tpath == None:
            m[n] = [tf.Get(n)]
        else:
            m[n] = [tf.Get(tpath + "/" + n)]
    return m

def Expand_deep_TDirs(tf, to_map, tpath=None):
    """
    A recursive deep-mapping function that expands into TDirectory(ies)
    """
    names = sorted(to_map.keys())
    for n in names:
        if len(to_map[n]) != 1:
            continue
        if tpath == None:
            tpath_ = n
        else:
            tpath_ = tpath + "/" + n

        tobject = to_map[n][0]
        if type(tobject) is ROOT.TDirectoryFile:
            m = Map(tf, tobject, tpath_)
            to_map[n].append(m)
            Expand_deep_TDirs(tf, m, tpath_)

def Map_TFile(filename, deep_maps=None):
    """
    Maps an input file as TFile into a dictionary(ies) of objects and names.
    Structure: dict[name][0] == object, dict[name][1] == deeper dict.
    """
    if deep_maps == None:
        deep_maps = {}
    if not type(deep_maps) is dict:
        return deep_maps

    f = ROOT.TFile(filename)
    m = Map(f, f)
    Expand_deep_TDirs(f, m)

    deep_maps[filename] = [f]
    deep_maps[filename].append(m)

    return deep_maps

####### DEMO
#filename = "myroot.root"
#mp = Map_TFile(filename)
# Now mp is a dict that has this struct
#mp{filename} is a list
#mp{filename}[0] this level of folder name
#mp{filename}[1] are the  dict(s) that pooints to the sublevel
#mp{filename}[1] has as keys the sublevel object names
#mp{filename}[1] has as values  the sub-sublevel dict


def printMap (dictMap, lvl=0):
    for key0, value0 in dictMap.items():
        print '\t'*lvl, key0
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            printMap(value0[1],lvl+1)

## Returns  object that has objName. If not found returns false
def getTFileObject(dictMap, objectName):
    ObjToReturn = False
    for key0, value0 in dictMap.items():
        if key0==objectName:
            ObjToReturn = value0[0]
            return ObjToReturn
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
           ObjToReturn= getTFileObject(value0[1],objectName)
    return ObjToReturn

## Returns parameter 1 and 2 from the object fit that has fitobjName. If not found returns false
def getTFileFitResult(dictMap, fitobjName):
    Param1 = False
    Param2 = False

    for key0, value0 in dictMap.items():
        if key0==fitobjName:
            Param1 = value0[0].Parameter(1)
            Param2 = value0[0].Parameter(2)
            return Param1,Param2
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            Param1,Param2 = getTFileFitResult(value0[1],fitobjName)

    return Param1,Param2


def fit(TGraphError,xstart,xstop,color,linestyle=1,Normalized_Data=False,seed_param_0=-1.,seed_param_1=-1.):

    if seed_param_0 != -1. and seed_param_1 != -1:
        fit_param_0 = seed_param_0
        fit_param_1 = seed_param_1
    elif Normalized_Data :
        fit_param_0 = 1**(-10)
        fit_param_1 = 7*(10**(-3))
    else:
        fit_param_0 = 1**(-7)
        fit_param_1 = 7*(10**(-3))


    f1 = ROOT.TF1("f1", "[0]*exp(x*[1])",xstart,xstop)
    f1.SetParNames("Constant", "Exponent")
    f1.SetParameters(fit_param_0,fit_param_1)
    f1.SetLineColor(color)
    f1.SetLineStyle(linestyle)
    if linestyle != 1:
        f1.SetLineWidth(2)

    TGraphError.Fit(f1,"RMEX0Q")
    return f1.GetParameter(0),f1.GetParameter(1),f1.GetChisquare()



def RPC_Correction(list_of_imons,Temperatures,Pressures,press_factor,temp_factor):
    T0 =297.1
    P0 = 964.4
    output_imons = []

    for i in range(0,len(list_of_imons)):
        pcorrection = 1 + press_factor*((Pressures[i]-P0)/P0)
        tcorrection = 1 - temp_factor *((Temperatures[i]-T0)/(Temperatures[i]))
        output_imons.append(list_of_imons[i]*pcorrection*tcorrection)
    return output_imons


def STD_Correction(list_of_imons,Temperatures,Pressures,press_factor,temp_factor):
    T0 =297.1
    P0 = 964.4
    output_imons = []
    for i in range(0,len(list_of_imons)):
        output_imons.append(list_of_imons[i] * ((Temperatures[i]/T0)**(temp_factor)) * (P0/Pressures[i])**press_factor)

    return output_imons

#set the tdr style
tdrstyle.setTDRStyle()
#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Private Study"


iPos = 0
if( iPos==0 ): CMS_lumi.relPosX = 0.12

H_ref = 1080
W_ref = 1200
W = W_ref
H  = H_ref

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref

### ROOT style settings
ROOT.gStyle.SetPalette(ROOT.kRainBow)
ROOT.gStyle.SetEndErrorSize(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetOptFit(0)  # Remove Fit stats from plot
ROOT.gStyle.SetLabelSize(.04, "XY")



canvas = ROOT.TCanvas("c2","c2",850,850,W,H)
canvas_Projection = ROOT.TCanvas("projection","projection",850,850,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)
#canvas.SetLogy()

#canvas.SetGridy()
canvas.SetFillStyle(4000)


p1 = ROOT.TPad("p1","p1",0.,0.3,1.,1.)
p1.SetLeftMargin( L/W )
p2 = ROOT.TPad("p2","p2",0.,0.,1.,0.3);
p2.SetBottomMargin(0.25)
p2.SetLeftMargin( L/W )
p1.Draw()
p2.Draw()
p1.cd()
mg = ROOT.TMultiGraph()

markers_style = [
    ROOT.kFullCircle,
    ROOT.kFullSquare,
    ROOT.kOpenCircle,
    ROOT.kFullSquare,
    ROOT.kOpenCircle,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
]

## Initializing axis max and min
max_x = 10**-30
min_x = 10**30
max_y = 10**-30
min_y = 10**30

list_of_tgrapherrors = []
dict_of_fit = {}

filename_dict =collections.OrderedDict()

filename_dict['288K'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_288K_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_288K_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_288K_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_288K_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_288K_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_288K_3400V_Hole_70_50_70.root",
]

filename_dict['292K'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_292K_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_292K_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_292K_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_292K_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_292K_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_292K_3400V_Hole_70_50_70.root",

]

filename_dict['296K'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_296K_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_296K_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_296K_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_296K_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_296K_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_296K_3400V_Hole_70_50_70.root",
]
filename_dict['300K'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_300K_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_300K_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_300K_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_300K_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_300K_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_300K_3400V_Hole_70_50_70.root",
]


filename_dict['304K']=[
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_304K_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_304K_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_304K_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_304K_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_304K_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_304K_3400V_Hole_70_50_70.root",
]


filename_dict['308K']=[
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_308K_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_308K_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_308K_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_308K_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_308K_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/TempSweep/AACHEN_308K_3400V_Hole_70_50_70.root",
]


VDrift = [2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400]
Imon = [ float(VDrift[i])/4.7 for i in range(0,len(VDrift))]
Gain = []
GainErr = []
Temperature = []
Press = [964.4 for i in range(0,len(VDrift))]
for temperature,HVList in filename_dict.items():
    for HVSimulation in HVList:
        mp = Map_TFile(HVSimulation)
        obj= getTFileObject(mp,"totalElectrons")
        param1,param2 = getTFileFitResult(mp,"TFitResult-gain-polya")
        mu = param1
        sigma = (mu**2/(param2+1))**0.5
        entries=obj.GetEntries()
        mu_error = sigma/entries**0.5
        Gain.append(mu)
        GainErr.append(mu_error)
        Temperature.append(int(temperature.replace("K","")))



Tfac_Max = 2.
Tfac_Min = -2.
Pfac_Max = 2.
Pfac_Min = -2.
bins = 200

chi2_Fit= ROOT.TH2F("Chi2", "Chi2", bins, Tfac_Min, Tfac_Max, bins, Pfac_Min, Pfac_Max)
A_Fit= ROOT.TH2F("A", "A", bins, Tfac_Min, Tfac_Max, bins, Pfac_Min, Pfac_Max)
B_Fit= ROOT.TH2F("B", "B", bins, Tfac_Min, Tfac_Max, bins, Pfac_Min, Pfac_Max)
chi2_Fit.GetXaxis().SetTitle("#beta param")
chi2_Fit.GetYaxis().SetTitle("#alpha param")
chi2_Fit.GetYaxis().SetTitleOffset(0.8)

chi2_list = [1000000,100,100]


for t_factor in np.linspace(Tfac_Min,Tfac_Max,bins):
    Imon_corr = STD_Correction(Imon,Temperature,Press,1,t_factor)

    n = len(Imon)
    x = np.asarray(Imon_corr,dtype=float)
    y = np.asarray(Gain,dtype=float)
    ey = np.asarray(GainErr,dtype=float)
    ex = np.zeros(n,dtype=float)
    gerror = ROOT.TGraphErrors(n,x,y,ex,ey)
    A,B,chi2= fit(gerror,np.min(x),np.max(x),ROOT.kRed,linestyle=1)
    chi2 = chi2 / (n-2)
    # if(chi2 > 200 ):
    #     continue
    if chi2 < chi2_list[0]:
        chi2_list = [chi2,t_factor,1]
        print chi2_list
    for p_factor in np.linspace(Pfac_Min,Pfac_Max,bins):
        binx = int(round((t_factor-Tfac_Min)*(bins-1)/(Tfac_Max-Tfac_Min)))+1
        biny = int(round((p_factor-Pfac_Min)*(bins-1)/(Pfac_Max-Pfac_Min)))+1
        chi2_Fit.SetBinContent(binx,biny,chi2)
        A_Fit.SetBinContent(binx,biny,A)
        B_Fit.SetBinContent(binx,biny,B)


        # xAxis = gerror.GetXaxis()
        # xAxis.SetTitle("I_{div} (uA)")
        # yAxis = gerror.GetYaxis()
        # yAxis.SetTitle("G_{Eff}")
        # yAxis.SetTitleOffset(.6)
        # canvas.cd()
        # gerror.Draw("AP")


print
print "MINIMUM chi2, tfactor , pfactor "
print chi2_list
print

canvas.cd()
chi2_Fit.Draw("colz")
canvas.Modified()
canvas.Update()
latex = ROOT.TLatex()
latex.SetTextFont(42)
latex.SetTextAngle(0)
latex.SetTextColor(ROOT.kBlack)
latex.SetTextAlign(12)
latex.SetTextSize(0.03)
latex.DrawTextNDC(0.15, 0.9, "Triple-GEM")
latex.DrawTextNDC(0.15, 0.86, "Spacing: 3/1/2/1 mm")
latex.DrawTextNDC(0.15, 0.82, "Gas: Ar/CO2 (70/30)")
latex.DrawTextNDC(0.15, 0.78, "p = 964.4 mBar")
latex.DrawTextNDC(0.15, 0.74, "T = [288,292,296,300,304,308] K")
latex.Draw("SAME")
canvas.Modified()
canvas.Update()
pngName = "./Sauron.png"
pdfName = "./Sauron.pdf"
canvas.SaveAs(pdfName)
canvas.SaveAs(pngName)



canvas_Projection.cd()
secondPlot = chi2_Fit.ProjectionX(" ",0,-1,"do")
secondPlot.GetYaxis().SetTitle("#chi^{2} of the fit")
secondPlot.Draw()
latex = ROOT.TLatex()
latex.SetTextFont(42)
latex.SetTextAngle(0)
latex.SetTextColor(ROOT.kBlack)
latex.SetTextAlign(12)
latex.SetTextSize(0.03)
latex.DrawTextNDC(0.18, 0.34, "Triple-GEM")
latex.DrawTextNDC(0.18, 0.3, "Spacing: 3/1/2/1 mm")
latex.DrawTextNDC(0.18, 0.26, "Gas: Ar/CO2 (70/30)")
latex.DrawTextNDC(0.18, 0.22, "p = 964.4 mBar")
latex.DrawTextNDC(0.18, 0.18, "T = [288,292,296,300,304,308] K")
latex.Draw("SAME")
canvas_Projection.Modified()
canvas_Projection.Update()
canvas_Projection.SaveAs("./Sauron_projection.png")
canvas_Projection.SaveAs("./Sauron_projection.pdf")
raw_input()
