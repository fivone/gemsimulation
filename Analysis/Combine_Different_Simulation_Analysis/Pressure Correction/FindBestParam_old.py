import ROOT
import array
import csv
import os.path
import numpy as np
import math
import xlrd
import sys
### Let's add some more from different folder
framework = os.path.expandvars('$REPO_HOME')
sys.path.insert(1, framework+'/Analysis/AnaLib/')
import CMS_lumi, tdrstyle
import collections


def fit(TGraphError,xstart,xstop,color,linestyle=1,Normalized_Data=False,seed_param_0=-1.,seed_param_1=-1.):

    if seed_param_0 != -1. and seed_param_1 != -1:
        fit_param_0 = seed_param_0
        fit_param_1 = seed_param_1
    elif Normalized_Data :
        fit_param_0 = 1**(-10)
        fit_param_1 = 7*(10**(-3))
    else:
        fit_param_0 = 1**(-7)
        fit_param_1 = 7*(10**(-3))

    
    f1 = ROOT.TF1("f1", "[0]*exp(x*[1])",xstart,xstop)
    f1.SetParNames("Constant", "Exponent")
    f1.SetParameters(fit_param_0,fit_param_1)
    f1.SetLineColor(color)
    f1.SetLineStyle(linestyle)
    if linestyle != 1:
        f1.SetLineWidth(2)

    TGraphError.Fit(f1,"RMEX0Q")
    return f1.GetParameter(0),f1.GetParameter(1),f1.GetChisquare()



def RPC_Correction(list_of_imons,Temperatures,Pressures,press_factor,temp_factor):
    T0 =297.1
    P0 = 964.4
    output_imons = []

    for i in range(0,len(list_of_imons)):
        pcorrection = 1 + press_factor*((Pressures[i]-P0)/P0)
        tcorrection = 1 - temp_factor *((Temperatures[i]-T0)/(Temperatures[i]))
        output_imons.append(list_of_imons[i]*pcorrection*tcorrection)
    return output_imons


def STD_Correction(list_of_imons,Temperatures,Pressures,press_factor,temp_factor):
    T0 =297.1
    P0 = 964.4
    output_imons = []
    for i in range(0,len(list_of_imons)):
        output_imons.append(list_of_imons[i] * ((Temperatures[i]/T0)**(temp_factor)) * (P0/Pressures[i])**press_factor)

    return output_imons

#set the tdr style
tdrstyle.setTDRStyle()
#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Private Study"


iPos = 0
if( iPos==0 ): CMS_lumi.relPosX = 0.12

H_ref = 1080 
W_ref = 1200
W = W_ref
H  = H_ref

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref 
L = 0.12*W_ref
R = 0.04*W_ref

### ROOT style settings
ROOT.gStyle.SetPalette(ROOT.kRainBow)
ROOT.gStyle.SetEndErrorSize(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetOptFit(0)  # Remove Fit stats from plot
ROOT.gStyle.SetLabelSize(.04, "XY")



canvas = ROOT.TCanvas("c2","c2",850,850,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)
#canvas.SetLogy()

#canvas.SetGridy()
canvas.SetFillStyle(4000)


p1 = ROOT.TPad("p1","p1",0.,0.3,1.,1.)
p1.SetLeftMargin( L/W )
p2 = ROOT.TPad("p2","p2",0.,0.,1.,0.3);
p2.SetBottomMargin(0.25)
p2.SetLeftMargin( L/W )
p1.Draw()
p2.Draw()
p1.cd()
mg = ROOT.TMultiGraph()

markers_style = [
    ROOT.kFullCircle,
    ROOT.kFullSquare,
    ROOT.kOpenCircle,
    ROOT.kFullSquare,
    ROOT.kOpenCircle,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
]

## Initializing axis max and min
max_x = 10**-30
min_x = 10**30
max_y = 10**-30
min_y = 10**30

list_of_tgrapherrors = []
dict_of_fit = {}



VDrift = [2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400,2500,2700,2900,3100,3290,3400]
Imon =  [591, 602, 613, 623, 634, 645, 655, 666, 677, 687, 698, 592, 602, 613, 624, 634, 645, 655, 666, 677, 687, 698, 591, 602, 613, 624, 634, 645, 656, 666, 677, 688, 698, 591, 602, 613, 624, 634, 645, 656, 666, 677, 687, 698, 591, 602, 613, 624, 634, 645, 656, 666, 677, 687, 698, 591, 602, 613, 624, 634, 645, 656, 666, 677, 687, 698, 591, 602, 613, 624, 634, 645, 656, 666, 677, 687, 698]

Imon = [ float(VDrift[i])/4.7 for i in range(0,len(VDrift))]

Gain = [47.9588032373, 164.455334235, 648.079075366, 2793.08189966, 10995.3508338, 26098.7546702, 44.3769023031, 149.465187261, 574.303557558, 2463.64235217, 10121.4312406, 25369.3246382, 39.987090022, 137.29660042, 538.56139669, 2204.27197047, 9238.25130706, 20396.8127645, 39.0589952217, 130.344200095, 487.693648783, 2029.59654892, 8044.61418907, 18367.9699312, 35.2637825372, 120.946591134, 444.039402735, 1810.76115458, 7246.65453557, 16612.3581364, 33.8428904137, 106.086552578, 407.976394958, 1654.26734254, 6478.72990324, 15285.7731582]
    
GainErr =[0.814967061539, 2.50351276517, 9.25823658503, 41.3876698077, 146.665614429, 339.039623874, 0.727398679193, 2.19256969873, 10.5991644271, 33.7906085887, 170.976746709, 312.124757707, 0.653697020781, 2.09129779704, 6.59126647778, 30.9043107205, 119.993074889, 371.036973286, 0.652539148904, 1.97920865534, 6.9097198891, 27.7385944587, 106.652764761, 251.716583172, 0.595325328846, 1.88560450763, 6.44833378644, 24.7332571013, 98.686291511, 219.26004145, 0.556368236327, 1.62030530566, 5.99892353531, 18.3027916605, 80.286366772, 194.911602373]



Temp =[22.87, 22.86, 22.87, 22.87, 22.87, 22.87, 22.87, 22.87, 22.87, 22.86, 22.86, 23.23, 23.23, 23.23, 23.23, 23.23, 23.23, 23.23, 23.23, 23.23, 23.23, 23.23, 22.55, 22.54, 22.53, 22.53, 22.52, 22.52, 22.51, 22.51, 22.51, 22.51, 22.51, 21.42, 21.42, 21.42, 21.42, 21.41, 21.42, 21.42, 21.42, 21.42, 21.41, 21.41, 22.87, 22.87, 22.87, 22.86, 22.86, 22.86, 22.86, 22.86, 22.86, 22.85, 22.84, 23.28, 23.29, 23.29, 23.3, 23.3, 23.3, 23.31, 23.3, 23.29, 23.27, 23.25, 23.96, 23.96, 23.96, 23.96, 23.96, 23.96, 23.96, 23.96, 23.96, 23.96, 23.96]
Temp = [297 for i in range(0,len(VDrift))]

Press = [950, 950, 950, 950, 950, 950, 960, 960, 960, 960, 960, 960, 970, 970, 970, 970, 970, 970, 980, 980, 980, 980, 980, 980, 990, 990, 990, 990, 990, 990, 1000, 1000, 1000, 1000, 1000, 1000, ]


Tfac_Max = 1.
Tfac_Min = 0.
Pfac_Max = 1.
Pfac_Min = 0.
bins = 100

chi2_Fit= ROOT.TH2F("Chi2", "Chi2", bins, Tfac_Min, Tfac_Max, bins, Pfac_Min, Pfac_Max)
A_Fit= ROOT.TH2F("A", "A", bins, Tfac_Min, Tfac_Max, bins, Pfac_Min, Pfac_Max)
B_Fit= ROOT.TH2F("B", "B", bins, Tfac_Min, Tfac_Max, bins, Pfac_Min, Pfac_Max)
chi2_Fit.GetXaxis().SetTitle("#beta param")
chi2_Fit.GetYaxis().SetTitle("#alpha param")
chi2_Fit.GetYaxis().SetTitleOffset(0.8)

chi2_list = [1000000,100,100]


for p_factor in np.linspace(Tfac_Min,Tfac_Max,bins):
    for t_factor in np.linspace(Pfac_Min,Pfac_Max,bins):
        Imon_corr = STD_Correction(Imon,Temp,Press,p_factor,t_factor)

        n = len(Imon)
        x = np.asarray(Imon_corr,dtype=float)
        y = np.asarray(Gain,dtype=float)
        ey = np.asarray(GainErr,dtype=float)
        ex = np.zeros(n,dtype=float)
        gerror = ROOT.TGraphErrors(n,x,y,ex,ey)
        A,B,chi2= fit(gerror,np.min(x),np.max(x),ROOT.kRed,linestyle=1)
        chi2 = chi2 / (n-2)
        if(chi2 > 200 ):
            continue
        if chi2 < chi2_list[0]:
            chi2_list = [chi2,t_factor,p_factor]
        
        binx = int(round((t_factor-Tfac_Min)*(bins-1)/(Tfac_Max-Tfac_Min)))+1
        biny = int(round((p_factor-Pfac_Min)*(bins-1)/(Pfac_Max-Pfac_Min)))+1
        chi2_Fit.SetBinContent(binx,biny,chi2)
        A_Fit.SetBinContent(binx,biny,A)
        B_Fit.SetBinContent(binx,biny,B)
                                                      
        
        # xAxis = gerror.GetXaxis()
        # xAxis.SetTitle("I_{div} (uA)")
        # yAxis = gerror.GetYaxis()
        # yAxis.SetTitle("G_{Eff}")
        # yAxis.SetTitleOffset(.6)
        # canvas.cd()
        # gerror.Draw("AP")


print
print "MINIMUM chi2, tfactor , pfactor "
print chi2_list
print

canvas.cd()
chi2_Fit.Draw("colz2")
canvas.Modified()
canvas.Update()
raw_input()
sys.exit(0)




latex = ROOT.TLatex()
latex.SetTextFont(42)
latex.SetTextAngle(0)
latex.SetTextColor(ROOT.kBlack)    
latex.SetTextAlign(12)
latex.SetTextSize(0.03)
latex.DrawTextNDC(0.15, 0.9, "Triple-GEM chamber")
latex.DrawTextNDC(0.15, 0.86, "Spacing: 3/1/2/1 mm")
latex.DrawTextNDC(0.15, 0.82, "Gas: Ar/CO2 (70/30)")
latex.DrawTextNDC(0.15, 0.78, "T,p = 297.1 K, 964.4 mbar")
latex.Draw("SAME")


p1.Modified()
p1.Update()
p2.Modified()
p2.Update()
canvas.Update()
pngName = "./SimuComparison.png"
pdfName = "./SimuComparison.pdf"
canvas.SaveAs(pdfName)
canvas.SaveAs(pngName)
raw_input()
