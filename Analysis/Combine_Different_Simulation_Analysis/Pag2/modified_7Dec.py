import ROOT
import array
import csv
import os.path
import numpy as np
import math
import xlrd
import sys
### Let's add some more from different folder
framework = os.path.expandvars('$REPO_HOME')
sys.path.insert(1, framework+'/Analysis/AnaLib/')
import CMS_lumi, tdrstyle
import collections 



def import_from_QC5_template(full_path_to_file,left_points_to_be_cut = 0,right_points_to_be_cut = 0):
    print("Reading File: \n"+full_path_to_file+ " ...")
    workbook = xlrd.open_workbook(full_path_to_file)
    worksheet = workbook.sheet_by_name('Data Summary')

    #### In order to import data, use worksheet.cell(row_number,column_number).value . All the counters start from 0
    AVG_T = worksheet.cell(47,5).value
    AVG_P = worksheet.cell(49,5).value
    P0 = worksheet.cell(47,8).value
    T0 = worksheet.cell(48,8).value
    Req = worksheet.cell(16,1).value
    Rdivider = worksheet.cell(17,1).value
    
    VMon = []
    IMon = []
    VDrift_equiv = []
    Gain = []
    GainError = []
    VDrift = []

    for i in range (5+right_points_to_be_cut,21-left_points_to_be_cut):
        VMon.append(worksheet.cell(i,5).value)
        IMon.append(worksheet.cell(i,6).value)
        VDrift.append(IMon[-1]*Rdivider)
    for i in range (29+right_points_to_be_cut,45-left_points_to_be_cut):
        VDrift_equiv.append(worksheet.cell(i,6).value)
        Gain.append(worksheet.cell(i,11).value)
        GainError.append(worksheet.cell(i,12).value)


    return VDrift,Gain,GainError,AVG_T,AVG_P


def fit(TGraphError,xstart,xstop,color,linestyle=1,Normalized_Data=False,seed_param_0=-1.,seed_param_1=-1.):

    if seed_param_0 != -1. and seed_param_1 != -1:
        fit_param_0 = seed_param_0
        fit_param_1 = seed_param_1
    elif Normalized_Data :
        fit_param_0 = 1**(-10)
        fit_param_1 = 7*(10**(-3))
    else:
        fit_param_0 = 1**(-7)
        fit_param_1 = 7*(10**(-3))

    
    f1 = ROOT.TF1("f1", "[0]*exp(x*[1])",xstart,xstop)
    f1.SetParNames("Constant", "Exponent")
    f1.SetParameters(fit_param_0,fit_param_1)
    f1.SetLineColor(color)
    f1.SetLineStyle(linestyle)
    f1.SetLineWidth(1)
    if linestyle != 1:
        f1.SetLineWidth(3)

    TGraphError.Fit(f1,"RMEQ")
    return f1.GetParameter(0),f1.GetParameter(1),f1

def refit(fit_loops,tgraph,x_min,x_max,fit_color,linestyle=1,Normalized_Data=False):

    constant, exponent, f1 = fit(tgraph,x_min,x_max,fit_color,linestyle,Normalized_Data)
    for i in range(1,fit_loops):
        constant, exponent, f1 = fit(tgraph,x_min,x_max,fit_color,linestyle,Normalized_Data,constant,exponent)

    return constant,exponent, f1


####### Maps functions from here https://root-forum.cern.ch/t/loop-over-all-objects-in-a-root-file/10807/5
def Map(tf, browsable_to, tpath=None):
    """
    Maps objets as dict[obj_name][0] using a TFile (tf) and TObject to browse.
    """
    m = {}
    for k in browsable_to.GetListOfKeys():
        n = k.GetName()
        if tpath == None:
            m[n] = [tf.Get(n)]
        else:
            m[n] = [tf.Get(tpath + "/" + n)]
    return m

def Expand_deep_TDirs(tf, to_map, tpath=None):
    """
    A recursive deep-mapping function that expands into TDirectory(ies)
    """
    names = sorted(to_map.keys())
    for n in names:
        if len(to_map[n]) != 1:
            continue
        if tpath == None:
            tpath_ = n
        else:
            tpath_ = tpath + "/" + n
        
        tobject = to_map[n][0]
        if type(tobject) is ROOT.TDirectoryFile:
            m = Map(tf, tobject, tpath_)
            to_map[n].append(m)
            Expand_deep_TDirs(tf, m, tpath_)

def Map_TFile(filename, deep_maps=None):
    """
    Maps an input file as TFile into a dictionary(ies) of objects and names.
    Structure: dict[name][0] == object, dict[name][1] == deeper dict.
    """
    if deep_maps == None:
        deep_maps = {}
    if not type(deep_maps) is dict:
        return deep_maps
    
    f = ROOT.TFile(filename)
    m = Map(f, f)
    Expand_deep_TDirs(f, m)

    deep_maps[filename] = [f]
    deep_maps[filename].append(m)
    
    return deep_maps

####### DEMO
#filename = "myroot.root"
#mp = Map_TFile(filename)
# Now mp is a dict that has this struct
#mp{filename} is a list
#mp{filename}[0] this level of folder name
#mp{filename}[1] are the  dict(s) that pooints to the sublevel
#mp{filename}[1] has as keys the sublevel object names
#mp{filename}[1] has as values  the sub-sublevel dict


def squareSum(a,b):
    a = float(a)
    b = float(b)
    return (a**2 + b**2)**0.5

def printMap (dictMap, lvl=0):
    for key0, value0 in dictMap.items():
        print '\t'*lvl, key0
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            printMap(value0[1],lvl+1)

## Returns  object that has objName. If not found returns false
def getTFileObject(dictMap, objectName):
    ObjToReturn = False
    for key0, value0 in dictMap.items():
        if key0==objectName:
            ObjToReturn = value0[0]
            return ObjToReturn 
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
           ObjToReturn= getTFileObject(value0[1],objectName)
    return ObjToReturn

## Returns parameter 1 and 2 from the object fit that has fitobjName. If not found returns false
def getTFileFitResult(dictMap, fitobjName):
    Param1 = False
    Param2 = False
    
    for key0, value0 in dictMap.items():
        if key0==fitobjName:
            Param1 = value0[0].Parameter(1)
            Param2 = value0[0].Parameter(2)
            return Param1,Param2
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            Param1,Param2 = getTFileFitResult(value0[1],fitobjName)

    return Param1,Param2

filename_dict =collections.OrderedDict()


##################
## Hole Asimmetry Study
###################

# filename_dict['85_50_85']=[
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.16_2500V_Hole_85_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.16_2700V_Hole_85_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.16_2900V_Hole_85_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.16_3100V_Hole_85_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.16_3290V_Hole_85_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.16_3400V_Hole_85_50_85.root"
# ]
# filename_dict['73_53_88']=[
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_2500V_Hole_73_53_88.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_2700V_Hole_73_53_88.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_2900V_Hole_73_53_88.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.08_3100V_Hole_73_53_88.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.08_3290V_Hole_73_53_88.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.08_3400V_Hole_73_53_88.root"
# ]
# filename_dict['67_47_82']=[
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_2500V_Hole_67_47_82.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_2700V_Hole_67_47_82.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_2900V_Hole_67_47_82.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.07_3100V_Hole_67_47_82.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.08_3290V_Hole_67_47_82.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.08_3400V_Hole_67_47_82.root"
# ]
# filename_dict['70_50_85']=[
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.13_2500V_Hole_70_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.13_2700V_Hole_70_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.13_2900V_Hole_70_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.13_3100V_Hole_70_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.14_3290V_Hole_70_50_85.root",
# "/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/HoleUncertainity/AACHEN_2020.09.14_3400V_Hole_70_50_85.root"
# ]


##################
# Meas Sim Comparison
###################

filename_dict['Meas Sym. DM (70_50_70) 293K  997mBar'] = [ "/home/francesco/Desktop/gemsimulation/DataAna/Comparison/Gain/Techtra_Meas/QC5_Techtra-10x10-AACHEN-0001_70_50_70_20191031.xlsx"]
filename_dict['Meas Asym. DM OB (70_50_85) 977mBar'] = [ "/home/francesco/Desktop/gemsimulation/DataAna/Comparison/Gain/Techtra_Meas/QC5_Techtra-10x10-AACHEN-0001_70_50_85_20191126.xlsx"]
filename_dict['Meas SM OB (70_53_85) 999mBar'] = [ "/home/francesco/Desktop/gemsimulation/DataAna/Comparison/Gain/Techtra_Meas/QC5_Techtra-10x10-AACHEN-0001_70_53_85_20191029.xlsx"]
filename_dict['Meas SM OA (85_53_70) 996mBar'] = [ "/home/francesco/Desktop/gemsimulation/DataAna/Comparison/Gain/Techtra_Meas/QC5_Techtra-10x10-AACHEN-0001_85_53_70_20191204.xlsx"]

filename_dict['Meas Asym. DM OA (85_50_70) 986mBar'] = [ "/home/francesco/Desktop/gemsimulation/DataAna/Comparison/Gain/Techtra_Meas/QC5_Techtra-10x10-AACHEN-0001_85_50_70_20191206.xlsx"]


filename_dict['Sim Sym. DM (70_50_70) 997mBar'] = [
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_70/AACHEN_2020.09.17_2500V_Hole_70_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_70/AACHEN_2020.09.17_2700V_Hole_70_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_70/AACHEN_2020.10.01_2900V_Hole_70_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_70/AACHEN_2020.09.17_3100V_Hole_70_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_70/AACHEN_2020.09.17_3290V_Hole_70_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_70/AACHEN_2020.09.17_3400V_Hole_70_50_70.root",
]

filename_dict['Sim Asym. DM OB (70_50_85) 977mBar'] = [
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_85/AACHEN_2020.09.18_2500V_Hole_70_50_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_85/AACHEN_2020.09.18_2700V_Hole_70_50_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_85/AACHEN_2020.10.07_2900V_Hole_70_50_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_85/AACHEN_2020.09.18_3100V_Hole_70_50_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_85/AACHEN_2020.09.18_3290V_Hole_70_50_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_50_85/AACHEN_2020.09.18_3400V_Hole_70_50_85.root",
]
filename_dict['Sim SM OB (70_53_85)  999mBar']=[
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_53_85/AACHEN_2020.09.18_2500V_Hole_70_53_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_53_85/AACHEN_2020.09.18_2700V_Hole_70_53_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_53_85/AACHEN_2020.10.07_2900V_Hole_70_53_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_53_85/AACHEN_2020.09.18_3100V_Hole_70_53_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_53_85/AACHEN_2020.09.18_3290V_Hole_70_53_85.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/70_53_85/AACHEN_2020.09.18_3400V_Hole_70_53_85.root"
]
filename_dict['Sim SM OA (85_53_70) 996mBar'] = [
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_53_70/AACHEN_2020.09.22_2500V_Hole_85_53_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_53_70/AACHEN_2020.09.22_2700V_Hole_85_53_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_53_70/AACHEN_2020.10.07_2900V_Hole_85_53_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_53_70/AACHEN_2020.09.22_3100V_Hole_85_53_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_53_70/AACHEN_2020.09.22_3290V_Hole_85_53_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_53_70/AACHEN_2020.09.22_3400V_Hole_85_53_70.root"
]
filename_dict['Sim Asym. DM OA (85_50_70) 986mBar'] = [
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_50_70/AACHEN_2020.09.21_2500V_Hole_85_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_50_70/AACHEN_2020.09.21_2700V_Hole_85_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_50_70/AACHEN_2020.10.07_2900V_Hole_85_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_50_70/AACHEN_2020.09.21_3100V_Hole_85_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_50_70/AACHEN_2020.09.21_3290V_Hole_85_50_70.root",
"/home/francesco/Desktop/gemsimulation/DataAna/LabTP/85_50_70/AACHEN_2020.09.21_3400V_Hole_85_50_70.root"
]



legend_names_meas= []
legend_names_simu= []
x_list = [2500,2700,2900,3100,3290,3400]
y_list = []
ey_list = []



#set the tdr style
tdrstyle.setTDRStyle()
NORMALIZE = False
#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.writeExtraText = 1
#CMS_lumi.extraText = "Private Study"


iPos = 0
if( iPos==0 ): CMS_lumi.relPosX = 0.12

H_ref = 600 
W_ref = 800 
W = W_ref
H  = H_ref

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref 
L = 0.12*W_ref
R = 0.04*W_ref

### ROOT style settings
Number = 5
Red = np.asarray(  [0., .0, .6, .0, 1.],dtype=float)
Green = np.asarray([0., .6, .0, .0, .5],dtype=float)
Blue = np.asarray( [0., .0, .0, 1., .0],dtype=float)
Stops = np.asarray([.2, .4, .6, .8, 1.],dtype=float)
nb=255;
palette = ROOT.TColor.CreateGradientColorTable(Number,Stops,Red,Green,Blue,nb)
## TEST PALETTE ##
# f2 = ROOT.TF2("f2","0.1+(1-(x-2)*(x-2))*(1-(y-2)*(y-2))",1,3,1,3)
# f2.SetContour(20)
# f2.Draw("surf1z")
# raw_input()
##

ROOT.gStyle.SetEndErrorSize(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetOptFit(0)  # Remove Fit stats from plot
ROOT.gStyle.SetLabelSize(.04, "XY")



canvas = ROOT.TCanvas("c2","c2",50,50,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)

#canvas.SetGridy()
canvas.SetFillStyle(4000)

mg = ROOT.TMultiGraph()

markers_style = [
    28,
    30,
    31,
    32,
    33,
    34,
    35,
]

## Initializing axis max and min
max_x = 10**-30
min_x = 10**30
max_y = 10**-30
min_y = 10**30

list_of_tgrapherrors_meas = []
list_of_tgrapherrors_simu = []

## Data from hole uncertainity study
Ap =  9.72018923976*10**(-7)
Bp =  0.00679394437768
A = 7.79897383753*10**(-7)
B = 0.00680070960251
Am = 8.78137299909*10**(-7)
Bm = 0.00669689029451



file_number = 0
for geometry,HVList in filename_dict.items():
    del y_list[:]
    del ey_list[:]
    if "Meas" in geometry:
        legend_names_meas.append(geometry)
        item = HVList[0]
        if "85_53_70" in geometry:
            x,y,ey,AVG_T,AVG_P = import_from_QC5_template(item,1,4)
        elif "85_50_70" in geometry:
            x,y,ey,AVG_T,AVG_P = import_from_QC5_template(item,1,2)
        elif "70_50_85" in geometry:
            x,y,ey,AVG_T,AVG_P = import_from_QC5_template(item,0,1)
        else:
            x,y,ey,AVG_T,AVG_P = import_from_QC5_template(item)

        n = len(x)
        x = np.asarray(x,dtype=float)
        y = np.asarray(y,dtype=float)
        
        eyh = [ squareSum(((A/Am)*np.exp((B-Bm)*x[i])-1)*y[i],ey[i]) for i in range(0,len(x))]
        eyl = [squareSum((1-(A/Ap)*np.exp((B-Bp)*x[i]))*y[i],ey[i]) for i in range(0,len(x))]
                                                       
        eyh = np.asarray(eyh,dtype=float)
        eyl = np.asarray(eyl,dtype=float)
        ex = np.zeros(n,dtype=float)
        gerror = ROOT.TGraphAsymmErrors(n,x,y,ex,ex,eyl,eyh)
        myColor = ROOT.TColor.GetColorPalette(254*(file_number)/5)
        gerror.SetMarkerColor(myColor)
        gerror.SetMarkerStyle(markers_style[file_number])
        gerror.SetLineColor(myColor)
        gerror.SetMarkerSize(1)
        gerror.SetFillColorAlpha(myColor,.5)
        gerror.SetLineColorAlpha(myColor,.5)
        gerror.SetFillStyle(1001)
        constant, exponent, f1 = refit(2,gerror,x[0],x[-1],myColor)
        

        ## Finding x axis range
        max_x = max(max(x),max_x)
        max_y = max(max(y),max_y)
        min_y = min(min(y),min_y)
        
        print("Adding "+str(geometry)+" to  MultiGraph\n")
        mg.Add(gerror)
        list_of_tgrapherrors_meas.append(gerror)
        file_number = file_number +1

file_number = 0
for geometry,HVList in filename_dict.items():
    del y_list[:]
    del ey_list[:]
    if "Sim" in geometry:
        legend_names_simu.append(geometry)
        for HVSimulation in HVList:
            mp = Map_TFile(HVSimulation)
            #printMap(mp)
            
            param1,param2 = getTFileFitResult(mp,"TFitResult-gain-polya")
            obj= getTFileObject(mp,"gain")
            mu = param1
            sigma = (mu**2/(param2+1))**0.5
            entries=obj.GetEntries()
            mu_error = sigma/entries**0.5            
            y_list.append(mu)
            ey_list.append(mu_error)
            
            
        n = len(x_list)
        x = np.asarray(x_list,dtype=float)
        y = np.asarray(y_list,dtype=float)
        ey = np.asarray(ey_list,dtype=float)
        ex = np.zeros(n,dtype=float)
        gerror = ROOT.TGraphErrors(n,x,y,ex,ey)
        
        myColor = ROOT.TColor.GetColorPalette(254*(file_number)/5)
        gerror.SetMarkerColor(myColor)
        gerror.SetLineColor(myColor)
        gerror.SetMarkerStyle(2)
        gerror.SetLineStyle(2)
        gerror.SetLineWidth(0)
        gerror.SetMarkerSize(1)
        #gerror.SetFillStyle(4050)
        constant, exponent, f1 = refit(2,gerror,2600,x[-1],myColor,linestyle=(file_number)*2+1)

        ## Finding x axis range
        max_x = max(max(x),max_x)
        min_x = min(min(x),min_x)
        max_y = max(max(y),max_y)
        min_y = min(min(y),min_y)
        
        print("Adding "+str(geometry)+" to  MultiGraph\n")
        #mg.Add(gerror)
        list_of_tgrapherrors_simu.append(f1)
        file_number = file_number +1

xAxisTitle = "V_{Drift} (V)"
xAxis = mg.GetXaxis()
xAxis.SetNdivisions(6,5,0)
xAxis.SetTitle(xAxisTitle)
xAxis.SetTitleOffset(1)
xAxis.SetTitleSize(0.05)
yAxis = mg.GetYaxis()
yAxis.SetNdivisions(6,5,0)
yAxis.SetTitleSize(0.05)
yAxis.SetTitle("Effective Gain")
yAxis.SetTitleOffset(1.25)

mg.Draw("a3p")   ## ADD PMC to Palette Marker Color: marker color is taken in the current palette
mg.GetXaxis().SetLimits(min_x-100,max_x+100)
mg.SetMinimum(min_y*0.9)
mg.SetMaximum(max_y*1.1)
for i in list_of_tgrapherrors_simu:
    i.Draw("SAME")
# if NORMALIZE:
#     mg.SetMinimum(0.100)
#     mg.SetMaximum(20)
    

#canvas.BuildLegend()
#draw the lumi text on the canvas
CMS_lumi.CMS_lumi(canvas, iPeriod, iPos)


legend_meas = ROOT.TLegend(0.18,0.7,0.43,0.55)
legend_simu = ROOT.TLegend(0.18,0.55,0.43,0.4)
legend_meas.SetTextSize(0.02)
legend_simu.SetTextSize(0.02)

for i,val in enumerate(legend_names_meas):
    legend_meas.AddEntry(list_of_tgrapherrors_meas[i],legend_names_meas[i],"lepf")
for i,val in enumerate(legend_names_simu):
    legend_simu.AddEntry(list_of_tgrapherrors_simu[i],legend_names_simu[i],"l")

legend_meas.Draw("SAME")
legend_simu.Draw("SAME")

canvas.cd()
canvas.Update()
canvas.RedrawAxis()
frame = canvas.GetFrame()
frame.Draw()


latex = ROOT.TLatex()
latex.SetTextFont(42)
latex.SetTextAngle(0)
latex.SetTextColor(ROOT.kBlack)    
latex.SetTextAlign(12)
latex.SetTextSize(0.03)
latex.DrawTextNDC(0.15, 0.9, "Triple-GEM chamber")
latex.DrawTextNDC(0.15, 0.86, "Spacing: 3/1/2/1 mm")
latex.DrawTextNDC(0.15, 0.82, "Gas: Ar/CO2 (70/30)")
latex.DrawTextNDC(0.15, 0.78, "Temp = 295 K ")
#latex.DrawTextNDC(0.15, 0.78, "Garfield Simulation")
#latex.DrawTextNDC(0.15, 0.74, "T=297.1K, P=723Torr")
latex.Draw("SAME")

#update the canvas to draw the legend
canvas.Update()
pngName = "./Pag2Plot.png"
pdfName = "./Pag2Plot.pdf"
canvas.SaveAs(pdfName)
canvas.SaveAs(pngName)

canvas.SetLogy()

legend_meas = ROOT.TLegend(0.6,0.3,0.95,0.45)
legend_simu = ROOT.TLegend(0.6,0.15,0.95,0.3)
legend_meas.SetTextSize(0.02)
legend_simu.SetTextSize(0.02)
for i,val in enumerate(legend_names_meas):
    legend_meas.AddEntry(list_of_tgrapherrors_meas[i],legend_names_meas[i],"lepf")
for i,val in enumerate(legend_names_simu):
    legend_simu.AddEntry(list_of_tgrapherrors_simu[i],legend_names_simu[i],"l")

legend_meas.Draw("SAME")
legend_simu.Draw("SAME")
canvas.Update()
pngName = "./Pag2Plot_LogScale.png"
pdfName = "./Pag2Plot_LogScale.pdf"
canvas.SaveAs(pdfName)
canvas.SaveAs(pngName)
raw_input("Press Enter to end")
