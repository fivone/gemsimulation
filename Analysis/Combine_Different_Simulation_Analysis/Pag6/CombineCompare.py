import ROOT
import array
import csv
import os.path
import numpy as np
import math
import xlrd
import sys
import collections 
### Let's add some more from different folder
framework = os.path.expandvars('$myLIB')
sys.path.insert(1, framework+'/ROOT_Utils/')
try:
    import CMS_lumi, tdrstyle
except:
  print("ERROR:\n\tCan't find the package CMS_lumi and tdrstlye\n\tPlease verify that this file are placed in the path $myLIB/ROOT_Utils/ \n\tAdditionally keep in mind to export the environmental variable $myLIB\nEXITING...\n") 
  sys.exit(0)

def fit(TGraphError,xstart,xstop,color,linestyle=1,Normalized_Data=False,seed_param_0=-1.,seed_param_1=-1.):

    if seed_param_0 != -1. and seed_param_1 != -1:
        fit_param_0 = seed_param_0
        fit_param_1 = seed_param_1
    elif Normalized_Data :
        fit_param_0 = 1**(-10)
        fit_param_1 = 7*(10**(-3))
    else:
        fit_param_0 = 1**(-7)
        fit_param_1 = 7*(10**(-3))

    
    f1 = ROOT.TF1("f1", "[0]*exp(x*[1])",xstart,xstop)
    f1.SetParNames("Constant", "Exponent")
    f1.SetParameters(fit_param_0,fit_param_1)
    f1.SetLineColor(color)
    f1.SetLineStyle(linestyle)
    if linestyle != 1:
        f1.SetLineWidth(2)

    TGraphError.Fit(f1,"RMEQ")
    TGraphError.GetListOfFunctions().FindObject("f1").SetRange(2500, xstop)
    return f1.GetParameter(0),f1.GetParameter(1)

def refit(fit_loops,tgraph,x_min,x_max,fit_color,linestyle=6,Normalized_Data=False):

    constant, exponent = fit(tgraph,x_min,x_max,fit_color,Normalized_Data)
    for i in range(1,fit_loops):
        constant, exponent = fit(tgraph,x_min,x_max,fit_color,linestyle,Normalized_Data,constant,exponent)

    return constant,exponent


####### Maps functions from here https://root-forum.cern.ch/t/loop-over-all-objects-in-a-root-file/10807/5
def Map(tf, browsable_to, tpath=None):
    """
    Maps objets as dict[obj_name][0] using a TFile (tf) and TObject to browse.
    """
    m = {}
    for k in browsable_to.GetListOfKeys():
        n = k.GetName()
        if tpath == None:
            m[n] = [tf.Get(n)]
        else:
            m[n] = [tf.Get(tpath + "/" + n)]
    return m

def Expand_deep_TDirs(tf, to_map, tpath=None):
    """
    A recursive deep-mapping function that expands into TDirectory(ies)
    """
    names = sorted(to_map.keys())
    for n in names:
        if len(to_map[n]) != 1:
            continue
        if tpath == None:
            tpath_ = n
        else:
            tpath_ = tpath + "/" + n
        
        tobject = to_map[n][0]
        if type(tobject) is ROOT.TDirectoryFile:
            m = Map(tf, tobject, tpath_)
            to_map[n].append(m)
            Expand_deep_TDirs(tf, m, tpath_)

def Map_TFile(filename, deep_maps=None):
    """
    Maps an input file as TFile into a dictionary(ies) of objects and names.
    Structure: dict[name][0] == object, dict[name][1] == deeper dict.
    """
    if deep_maps == None:
        deep_maps = {}
    if not type(deep_maps) is dict:
        return deep_maps
    
    f = ROOT.TFile(filename)
    m = Map(f, f)
    Expand_deep_TDirs(f, m)

    deep_maps[filename] = [f]
    deep_maps[filename].append(m)
    
    return deep_maps

####### DEMO
#filename = "myroot.root"
#mp = Map_TFile(filename)
# Now mp is a dict that has this struct
#mp{filename} is a list
#mp{filename}[0] this level of folder name
#mp{filename}[1] are the  dict(s) that pooints to the sublevel
#mp{filename}[1] has as keys the sublevel object names
#mp{filename}[1] has as values  the sub-sublevel dict
                    

def printMap (dictMap, lvl=0):
    for key0, value0 in dictMap.items():
        print '\t'*lvl, key0
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            printMap(value0[1],lvl+1)

## Returns  object that has objName. If not found returns false
def getTFileObject(dictMap, objectName):
    ObjToReturn = False
    for key0, value0 in dictMap.items():
        if key0==objectName:
            ObjToReturn = value0[0]
            return ObjToReturn 
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
           ObjToReturn= getTFileObject(value0[1],objectName)
    return ObjToReturn

## Returns parameter 1 and 2 from the object fit that has fitobjName. If not found returns false
def getTFileFitResult(dictMap, fitobjName):
    Param1 = False
    Param2 = False
    
    for key0, value0 in dictMap.items():
        if key0==fitobjName:
            Param1 = value0[0].Parameter(1)
            Param2 = value0[0].Parameter(2)
            return Param1,Param2
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            Param1,Param2 = getTFileFitResult(value0[1],fitobjName)

    return Param1,Param2

def Generate_Uncertainity_Plot(dict_of_fit,x_values,reference_geometry):
    multigraph = ROOT.TMultiGraph()
    A = dict_of_fit[reference_geometry][0]
    B = dict_of_fit[reference_geometry][1]


    for geometry,fit_list in dict_of_fit.items():
        y = []
        x = x_values
        if geometry == reference_geometry:

            for i in x:
                y.append(1)
            n = len(x_values)
            y = np.asarray(y,dtype=float)
            ey = np.zeros(n,dtype=float)
            ex = np.zeros(n,dtype=float)
            gerror = ROOT.TGraphErrors(n,x,y,ex,ey)
            gerror.SetMarkerSize(0)
            myColor = dict_of_fit[geometry][2]
            myMarker = dict_of_fit[geometry][3]
            gerror.SetMarkerColor(myColor)
            gerror.SetLineColor(myColor)
            gerror.SetMarkerStyle(myMarker)
            gerror.SetLineStyle(10)
            gerror.SetLineWidth(3)
            multigraph.Add(gerror)
        else:
            temp_A = dict_of_fit[geometry][0]
            temp_B = dict_of_fit[geometry][1]

            for i in x:
                y.append((temp_A/A)*np.exp(temp_B*i-B*i))
            n = len(x_values)
            y = np.asarray(y,dtype=float)
            ey = np.zeros(n,dtype=float)
            ex = np.zeros(n,dtype=float)
            gerror = ROOT.TGraphErrors(n,x,y,ex,ey)
            myColor = dict_of_fit[geometry][2]
            myMarker = dict_of_fit[geometry][3]
            gerror.SetMarkerColor(myColor)
            gerror.SetLineColor(myColor)
            gerror.SetMarkerStyle(myMarker)
            gerror.SetLineStyle(6)
            gerror.SetLineWidth(3)
            multigraph.Add(gerror)
    return multigraph




filename_dict =collections.OrderedDict()

filename_dict['70_50_70'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_70/AACHEN_2020.10.07_2500V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_70/AACHEN_2020.10.07_2700V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_70/AACHEN_2020.10.07_2900V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_70/AACHEN_2020.10.07_3100V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_70/AACHEN_2020.10.07_3290V_Hole_70_50_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_70/AACHEN_2020.10.07_3400V_Hole_70_50_70.root",   
    ]


filename_dict['70_50_85'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_85/AACHEN_2020.09.13_2500V_Hole_70_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_85/AACHEN_2020.09.13_2700V_Hole_70_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_85/AACHEN_2020.10.07_2900V_Hole_70_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_85/AACHEN_2020.09.13_3100V_Hole_70_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_85/AACHEN_2020.09.14_3290V_Hole_70_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/70_50_85/AACHEN_2020.09.14_3400V_Hole_70_50_85.root",

    ]

filename_dict['85_50_85'] = [
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/85_50_85/AACHEN_2020.09.16_2500V_Hole_85_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/85_50_85/AACHEN_2020.09.16_2700V_Hole_85_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/85_50_85/AACHEN_2020.10.07_2900V_Hole_85_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/85_50_85/AACHEN_2020.09.16_3100V_Hole_85_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/85_50_85/AACHEN_2020.09.16_3290V_Hole_85_50_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/P5TP/85_50_85/AACHEN_2020.09.16_3400V_Hole_85_50_85.root",
]

legend_names= []
x_list = [2500,2700,2900,3100,3290,3400]
y_list = []
ey_list = []



#set the tdr style
tdrstyle.setTDRStyle()
NORMALIZE = False
#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.writeExtraText = 1
#CMS_lumi.extraText = "Private Study"


iPos = 0
if( iPos==0 ): CMS_lumi.relPosX = 0.12

H_ref = 1080 
W_ref = 1200
W = W_ref
H  = H_ref

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref 
L = 0.12*W_ref
R = 0.04*W_ref

### ROOT style settings
ROOT.gStyle.SetPalette(ROOT.kRainBow)
ROOT.gStyle.SetEndErrorSize(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetOptFit(0)  # Remove Fit stats from plot
ROOT.gStyle.SetLabelSize(.04, "XY")



canvas = ROOT.TCanvas("c2","c2",50,50,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)

#canvas.SetGridy()
canvas.SetFillStyle(4000)


p1 = ROOT.TPad("p1","p1",0.,0.3,1.,1.)
p1.SetLogy()
p1.SetLeftMargin( L/W )
p2 = ROOT.TPad("p2","p2",0.,0.,1.,0.3);
p2.SetBottomMargin(0.25)
p2.SetLeftMargin( L/W )
p1.Draw()
p2.Draw()
p1.cd()
mg = ROOT.TMultiGraph()
uncertainity = ROOT.TMultiGraph()

markers_style = [
    ROOT.kFullCircle,
    ROOT.kFullSquare,
    ROOT.kOpenCircle,
    ROOT.kFullSquare,
    ROOT.kOpenCircle,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
]

## Initializing axis max and min
max_x = 10**-30
min_x = 10**30
max_y = 10**-30
min_y = 10**30

list_of_tgrapherrors = []
dict_of_fit = {}


file_number = 0
## Is a map of all levels of TFile with objects
for geometry,HVList in filename_dict.items():
    del y_list[:]
    del ey_list[:]
    legend_names.append(geometry)
    for HVSimulation in HVList:
        mp = Map_TFile(HVSimulation)
        # printMap(mp)
        obj= getTFileObject(mp,"totalElectrons")
        
        param1,param2 = getTFileFitResult(mp,"TFitResult-gain-polya")
        # print "Param1 = " , param1 , "\nParam2 = " , param2
        mu = param1
        sigma = (mu**2/(param2+1))**0.5
        entries=obj.GetEntries()
        mu_error = sigma/entries**0.5            
        y_list.append(mu)
        ey_list.append(mu_error)


    n = len(x_list)
    x = np.asarray(x_list,dtype=float)
    y = np.asarray(y_list,dtype=float)
    ey = np.asarray(ey_list,dtype=float)
    ex = np.zeros(n,dtype=float)
    gerror = ROOT.TGraphErrors(n,x,y,ex,ey)
    
    myColor = ROOT.TColor.GetColorPalette(255/len(filename_dict)*(file_number))
    myMarker = markers_style[file_number]
    gerror.SetMarkerColor(myColor)
    gerror.SetLineColor(myColor)
    gerror.SetMarkerStyle(myMarker)
    gerror.SetLineStyle(1)
    gerror.SetLineWidth(2)
    constant, exponent = refit(1,gerror,x[1],x[-1]*1.1,myColor,linestyle=1)
    print geometry + "\tA = ", constant, "\tB = ", exponent
    dict_of_fit[geometry] = [constant,exponent,myColor,myMarker]

    ## Finding x axis range
    max_x = max(max(x),max_x)
    min_x = min(min(x),min_x)
    max_y = max(max(y),max_y)
    min_y = min(min(y),min_y)

    print("Adding "+str(geometry)+" to  MultiGraph\n")
    mg.Add(gerror)
    list_of_tgrapherrors.append(gerror)
    file_number = file_number +1

print dict_of_fit
uncertainity = Generate_Uncertainity_Plot(dict_of_fit,x,'70_50_85')




        

xAxisTitle = "V_{Drift} (V)"
for j in [uncertainity,mg]:
    if j == uncertainity:
        SizeMult = 0.7/0.3
        y_title = "Ratio"
    else:
        SizeMult = 1.
        y_title= "Simulated Effective Gain"
    xAxis = j.GetXaxis()
    xAxis.SetLabelSize(0.04*SizeMult)
    xAxis.SetNdivisions(6,5,0)
    xAxis.SetTitle(xAxisTitle)
    xAxis.SetTitleOffset(1)
    xAxis.SetTitleSize(0.05*SizeMult)
    yAxis = j.GetYaxis()
    yAxis.SetNdivisions(6,5,0)
    yAxis.SetLabelSize(0.04*SizeMult)
    yAxis.SetTitleOffset(1.1/SizeMult)
    yAxis.SetTitleSize(0.05*SizeMult)
    yAxis.SetTitle(y_title)

mg.Draw("0AP")   ## ADD PMC to Palette Marker Color: marker color is taken in the current palette
mg.GetXaxis().SetLimits(min_x-50,max_x+100)
mg.SetMinimum(min_y*0.9)
mg.SetMaximum(max_y*1.1)
# if NORMALIZE:
#     mg.SetMinimum(0.100)
#     mg.SetMaximum(20)
    
p2.cd()
uncertainity.Draw("0APL")   ## ADD PMC to Palette Marker Color: marker color is taken in the current palette
uncertainity.GetXaxis().SetLimits(min_x-50,max_x+100)
uncertainity.SetMinimum(0.)
uncertainity.SetMaximum(3.)
#draw the lumi text on the canvas
CMS_lumi.CMS_lumi(canvas, iPeriod, iPos)


legend = ROOT.TLegend(0.15,0.65,0.45,0.5)
for i,val in enumerate(legend_names):
    legend.AddEntry(list_of_tgrapherrors[i],legend_names[i],"lep")

p1.cd()
legend.Draw("SAME")

latex = ROOT.TLatex()
latex.SetTextFont(42)
latex.SetTextAngle(0)
latex.SetTextColor(ROOT.kBlack)    
latex.SetTextAlign(12)
latex.SetTextSize(0.03)
latex.DrawTextNDC(0.15, 0.9, "Triple-GEM chamber")
latex.DrawTextNDC(0.15, 0.86, "Spacing: 3/1/2/1 mm")
latex.DrawTextNDC(0.15, 0.82, "Gas: Ar/CO2 (70/30)")
latex.DrawTextNDC(0.15, 0.78, "T, p = 297 K, 964 mBar")
latex.Draw("SAME")


p1.Modified()
p1.Update()
p2.Modified()
p2.Update()
canvas.Update()
pngName = "./SimuComparison_log.png"
pdfName = "./SimuComparison_log.pdf"
canvas.SaveAs(pdfName)
canvas.SaveAs(pngName)
raw_input()
