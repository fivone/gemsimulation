import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.font_manager import FontProperties
import sys
import numpy as np


number_of_lines= 5
cm_subsection = np.linspace(0.0, 1.0, number_of_lines) 
colors = [ cm.Spectral(x) for x in cm_subsection ]
fontP = FontProperties()
fontP.set_size('xx-large')

def to_percentage(f):
    f = f*100
    return f

d = {}
d['Triple GEM OB (70/53/85)'] = {}
d['Triple GEM OB (70/53/85)']['totalElectrons'] = [68.3710443038,173.546575787,523.50336159,1760.65155131,6122.9516671,13531.4296954]
d['Triple GEM OB (70/53/85)']['Anode'] = [0.302104036956,0.350747723392,0.400040121051,0.440462060263,0.4622996673,0.470258783957]
d['Triple GEM OB (70/53/85)']['GEM3Bottom'] = [0.189230959583,0.232985880232,0.283354896241,0.325891181406,0.355574676946,0.370438400204]
d['Triple GEM OB (70/53/85)']['GEM3Kapton'] = [0.131461916183,0.111262089092,0.0907543019742,0.0732670917661,0.0601741341509,0.0538530900324]
d['Triple GEM OB (70/53/85)']['GEM2Bottom'] = [0.176601138986,0.152883237558,0.119936969898,0.0852575769467,0.0608889696358,0.0485296928188]
d['Triple GEM OB (70/53/85)']['OtherLayers'] = [1-d['Triple GEM OB (70/53/85)']['GEM2Bottom'][i]-d['Triple GEM OB (70/53/85)']['GEM3Kapton'][i]-d['Triple GEM OB (70/53/85)']['GEM3Bottom'][i]-d['Triple GEM OB (70/53/85)']['Anode'][i] for i in range(0,len(d['Triple GEM OB (70/53/85)']['Anode']))]

d['Triple GEM OA (85/53/70)'] = {}
d['Triple GEM OA (85/53/70)']['totalElectrons'] = [75.801161919,191.67549121,569.173272358,1929.23393622,6845.46153846,14492.211121]
d['Triple GEM OA (85/53/70)']['Anode'] = [0.245782373338,0.281251429769,0.319324057702,0.350277214516,0.368786272879,0.375493755616]
d['Triple GEM OA (85/53/70)']['GEM3Bottom'] = [0.19426681261,0.239340944089,0.286175903567,0.33178285489,0.365556153729,0.382320780616]
d['Triple GEM OA (85/53/70)']['GEM3Kapton'] = [0.288847873674,0.26275010289,0.231744606728,0.20115156538,0.177008996551,0.164186251726]
d['Triple GEM OA (85/53/70)']['GEM2Bottom'] =[0.139483135584,0.119735350507,0.0963121916131,0.0692169546496,0.0491510711173,0.0396408217574]
d['Triple GEM OA (85/53/70)']['OtherLayers'] = [1-d['Triple GEM OA (85/53/70)']['GEM2Bottom'][i]-d['Triple GEM OA (85/53/70)']['GEM3Kapton'][i]-d['Triple GEM OA (85/53/70)']['GEM3Bottom'][i]-d['Triple GEM OA (85/53/70)']['Anode'][i] for i in range(0,len(d['Triple GEM OA (85/53/70)']['Anode']))]

legend = ["Anode","GEM3Bottom","GEM3Kapton","GEM2Bottom","OtherLayers"]
## Percentage
for key,dumb   in d.items():
    for layer,dumb in d[key].items():
        if layer == 'totalelElectrons': continue
        d[key][layer] = map(to_percentage, d[key][layer])

## X axis
X = np.array([2500,2700,2900,3100,3290,3400])
##2 subplots that share x,y axis and leave space for legend at the right
f,ax  = plt.subplots(1,2,sharex='all', sharey='all',gridspec_kw=dict(left=0.08, right=0.85,bottom=0.1, top=0.9),figsize=(18,10))
ax[0].set_prop_cycle(color=colors)
ax[1].set_prop_cycle(color=colors)

## Loop over the 2 orientations
for plotnumber,key in enumerate(d.keys()):
    cumulative_size = np.zeros(len(X))
    axes = []
    ## Add title
    ax[plotnumber].text(0.5, 1.08, key,horizontalalignment='center',fontsize=20,transform = ax[plotnumber].transAxes,fontweight='bold')
    ## Loop over the GEM ending layers
    for index,label in enumerate(legend):
        axes.append(ax[plotnumber].bar(X,d[key][label],width=100,bottom=cumulative_size,label=legend[index]))
        cumulative_size+= np.asarray(d[key][label])

        ## Add numbers in the bar 
        for axis in axes:
            for bar in axis:
                text_color = 'black'
                w, h = bar.get_width(), bar.get_height()
                ax[plotnumber].text(bar.get_x() + w/2, bar.get_y() + h/2, 
                         "{:.2f}".format(h), ha="center",weight="bold",color=text_color,fontsize=11,
                         va="center")
## Axis cosmectic and legend 
for i in [0,1]:
    if i == 0:
        ax[i].set_ylabel("Stacked Percentage (%)",fontsize=18,labelpad=14)
        ## Sorting legend labels
        handles, labels = ax[i].get_legend_handles_labels()
        handles.reverse()
        labels.reverse()
        f.legend(handles,labels,loc = 'center right',prop=fontP)

    ax[i].set_ylim(0.,100.)
    ax[i].tick_params(axis='both', which='major', labelsize=15)
    ax[i].set_xlabel(r'V$_{\rm Drift}$ (V)',fontsize=18,labelpad=18)

plt.show()
f.savefig("Percentage.png")
f.savefig("Percentage.pdf")
