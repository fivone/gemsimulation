import ROOT
import array
import csv
import os.path
import numpy as np
import math
import xlrd
import sys
### Let's add some more from different folder
framework = os.path.expandvars('$REPO_HOME')
sys.path.insert(1, framework+'/Analysis/AnaLib/')
import CMS_lumi, tdrstyle
import collections


####### Maps functions from here https://root-forum.cern.ch/t/loop-over-all-objects-in-a-root-file/10807/5
def Map(tf, browsable_to, tpath=None):
    """
    Maps objets as dict[obj_name][0] using a TFile (tf) and TObject to browse.
    """
    m = {}
    for k in browsable_to.GetListOfKeys():
        n = k.GetName()
        if tpath == None:
            m[n] = [tf.Get(n)]
        else:
            m[n] = [tf.Get(tpath + "/" + n)]
    return m

def Expand_deep_TDirs(tf, to_map, tpath=None):
    """
    A recursive deep-mapping function that expands into TDirectory(ies)
    """
    names = sorted(to_map.keys())
    for n in names:
        if len(to_map[n]) != 1:
            continue
        if tpath == None:
            tpath_ = n
        else:
            tpath_ = tpath + "/" + n
        
        tobject = to_map[n][0]
        if type(tobject) is ROOT.TDirectoryFile:
            m = Map(tf, tobject, tpath_)
            to_map[n].append(m)
            Expand_deep_TDirs(tf, m, tpath_)

def Map_TFile(filename, deep_maps=None):
    """
    Maps an input file as TFile into a dictionary(ies) of objects and names.
    Structure: dict[name][0] == object, dict[name][1] == deeper dict.
    """
    if deep_maps == None:
        deep_maps = {}
    if not type(deep_maps) is dict:
        return deep_maps
    
    f = ROOT.TFile(filename)
    m = Map(f, f)
    Expand_deep_TDirs(f, m)

    deep_maps[filename] = [f]
    deep_maps[filename].append(m)
    
    return deep_maps

####### DEMO
#filename = "myroot.root"
#mp = Map_TFile(filename)
# Now mp is a dict that has this struct
#mp{filename} is a list
#mp{filename}[0] this level of folder name
#mp{filename}[1] are the  dict(s) that pooints to the sublevel
#mp{filename}[1] has as keys the sublevel object names
#mp{filename}[1] has as values  the sub-sublevel dict
                    
def printMap (dictMap, lvl=0):
    for key0, value0 in dictMap.items():
        print '\t'*lvl, key0
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            printMap(value0[1],lvl+1)

## Returns  object that has objName. If not found returns false
def getTFileObject(dictMap, objectName):
    ObjToReturn = False
    for key0, value0 in dictMap.items():
        if key0==objectName:
            ObjToReturn = value0[0]
            return ObjToReturn 
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
           ObjToReturn= getTFileObject(value0[1],objectName)
    return ObjToReturn

## Returns parameter 1 and 2 from the object fit that has fitobjName. If not found returns false
def getTFileFitResult(dictMap, fitobjName):
    Param1 = False
    Param2 = False
    
    for key0, value0 in dictMap.items():
        if key0==fitobjName:
            Param1 = value0[0].Parameter(1)
            Param2 = value0[0].Parameter(2)
            return Param1,Param2
        if len(value0)>1 and type(value0[1]) is dict:  ### There is a sublevel
            Param1,Param2 = getTFileFitResult(value0[1],fitobjName)

    return Param1,Param2

filename_dict =collections.OrderedDict()


filename_dict['70_53_85']=[
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.18_2500V_Hole_70_53_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.18_2700V_Hole_70_53_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.10.07_2900V_Hole_70_53_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.18_3100V_Hole_70_53_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.18_3290V_Hole_70_53_85.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.18_3400V_Hole_70_53_85.root",
]
filename_dict['85_53_70']=[
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.22_2500V_Hole_85_53_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.22_2700V_Hole_85_53_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.10.07_2900V_Hole_85_53_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.22_3100V_Hole_85_53_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.22_3290V_Hole_85_53_70.root",
"/media/francesco/Samsung_T5/cernbox/gemsimulation/DataAna/AACHEN_2020.09.22_3400V_Hole_85_53_70.root",
]
legend_names= []
x_list = [2500,2700,2900,3100,3290,3400]
y_list = []
ey_list = []


voltages = [2500,2700,2900,3100,3290,3400]

for geometry,HVList in filename_dict.items():
    print
    for item,HVSimulation in enumerate(HVList):
        # print geometry 
        # print "TotElec Anode GEM3B GEM3K GEM3T GEM2B GEM2K GEM2T GEM1B GEM1K GEM1T Reco"
        mp = Map_TFile(HVSimulation)
        # printMap(mp)
        totalElectrons = getTFileObject(mp,"totalElectrons").GetMean()
        GEM3Botmean = getTFileObject(mp,"GEM3BotFractionLost").GetMean()
        GEM3Kapmean = getTFileObject(mp,"GEM3KapFractionLost").GetMean()
        GEM3Topmean = getTFileObject(mp,"GEM3TopFractionLost").GetMean()
        GEM2Botmean = getTFileObject(mp,"GEM2BotFractionLost").GetMean()
        GEM2Kapmean = getTFileObject(mp,"GEM2KapFractionLost").GetMean()
        GEM2Topmean = getTFileObject(mp,"GEM2TopFractionLost").GetMean()
        GEM1Botmean = getTFileObject(mp,"GEM1BotFractionLost").GetMean()
        GEM1Kapmean = getTFileObject(mp,"GEM1KapFractionLost").GetMean()
        GEM1Topmean = getTFileObject(mp,"GEM1TopFractionLost").GetMean()
        Anodefrmean = getTFileObject(mp,"AnodeFraction").GetMean()
        RecomFrmean = 1-(GEM3Botmean+GEM3Kapmean+GEM3Topmean+GEM2Botmean+GEM2Kapmean+GEM2Topmean+GEM1Botmean+GEM1Kapmean+GEM1Topmean+Anodefrmean)
        print voltages[item], totalElectrons, Anodefrmean, GEM3Botmean, GEM3Kapmean, GEM3Topmean, GEM2Botmean, GEM2Kapmean, GEM2Topmean, GEM1Botmean, GEM1Kapmean, GEM1Topmean, RecomFrmean

raw_input("Press Enter to end")
