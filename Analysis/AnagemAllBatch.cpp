#include "Parser.h"
#include "AnaDef.h"
#include "TROOT.h"



int main(int argc, char** argv) {
    
    gROOT->Time();
    gStyle->SetOptFit(101);
    AnaParser* Init = new AnaParser(argc, argv);
    printTime();
    cout <<"Start\n---"<<endl;


    files = Init->GetFileNames();
    foldername = Init->GetFolder();
    fieldMapOrigin = Init->GetFieldMapOrigin();
    dateSimu = Init->GetDateSimu();
    number_of_file = Init->GetEntries();
    Vdrift = Init->GetVdrift();
    muonTrack = Init->muonTrack();
    singleElectron = Init->electron();
    gem_stages=Init->GetNumberOfLayer();

    Up_Radius=Init->GetHole()->GetUpRadius();
    Mid_Radius=Init->GetHole()->GetMidRadius();
    Bot_Radius=Init->GetHole()->GetBotRadius();

    fillChains();
    //totalSimulatedElectrons = chain->GetEntries();
    totalIntegratedSignals = chainIntegratedSignal->GetEntries();
    if (gem_stages == 3)
    { 
        //electrode positions
        zDrift=Init->GetTripleGem()->GetzDrift();
        zGem1=Init->GetTripleGem()->GetzGem1();
        zGem2=Init->GetTripleGem()->GetzGem2();
        zGem3=Init->GetTripleGem()->GetzGem3();
        zAnode=Init->GetTripleGem()->GetzAnode();

        if(singleElectron)
        {
            //Setting which Branches to read...Only Useful Ones
            chain->SetBranchAddress("electronBranch",&electron);
            chain->SetBranchAddress("infoBranch", &info);
            //chain->SetBranchAddress("ionBranch",&ion);
            chainIntegratedSignal->SetBranchAddress("integratedsignal",&integratedSignal);
            chainIntegratedSignal->SetBranchAddress("SignalWidth",&signalWidth);

            // Getting name of the file...place holder
            currentName=chain->GetFile()->GetName();
            //endLayer = t1->Branch("collectionLayer", &Summary, "anodeTime/D:anodeEnergy/D:gain/D:number_of_primaries/I:totalelectrons/I:elec_on_gem_1_top/I:elec_on_gem_1_bot/I:elec_on_gem_2_top/I:elec_on_gem_2_bot/I:elec_on_gem_3_top/I:elec_on_gem_3_bot/I:elec_on_anode/I");
            //startCoord = t1->Branch("startCoord", &info.x0, "x0/D:y0/D:z0/D:dx0/D:dy0/D:dz0/D");
            customBranch = AvalancheSizeTree->Branch("AvalancheSize",&avalsize,"x0/D:y0/D:z0/D:x2/D:y2/D:z2/D:avalDirect/D:avalDistance/D:aval_x_size/D:aval_y_size/D:aval_radius/D:gain/D");

            do
            {                  
                chain->GetEntry(i);
                if(newChainFile() || i==totalSimulatedElectrons-1) //detects the end of one root File
                {
                    Summary.anodeTime = Summary.anodeTime/Summary.anode;
                    Summary.anodeEnergy = Summary.anodeEnergy/Summary.anode;

                    if(Summary.gain != 0) // don't take into account the detection efficiency
                        {
                            gain->Fill(double(Summary.gain));
                            total->Fill(Summary.total_electrons);
                            GEM1BotFractionLost->Fill(double(Summary.gem_1_bot)/double(Summary.total_electrons));
                            GEM1KapFractionLost->Fill(double(Summary.gem_1_kap)/double(Summary.total_electrons));
                            GEM1TopFractionLost->Fill(double(Summary.gem_1_top)/double(Summary.total_electrons));
                            GEM2TopFractionLost->Fill(double(Summary.gem_2_top)/double(Summary.total_electrons));
                            GEM2KapFractionLost->Fill(double(Summary.gem_2_kap)/double(Summary.total_electrons));
                            GEM2BotFractionLost->Fill(double(Summary.gem_2_bot)/double(Summary.total_electrons));
                            GEM3BotFractionLost->Fill(double(Summary.gem_3_bot)/double(Summary.total_electrons));
                            GEM3KapFractionLost->Fill(double(Summary.gem_3_kap)/double(Summary.total_electrons));
                            GEM3TopFractionLost->Fill(double(Summary.gem_3_top)/double(Summary.total_electrons));
                            AnodeFraction->Fill(double(Summary.anode)/double(Summary.total_electrons));

                            // Filling avalanche detail
                            // all distances in cm
                            avalsize.x=info.x0;
                            avalsize.y=info.y0;
                            avalsize.z=info.z0;
                            // Evaluating the center of each avalanche
                            avalsize.x2=anode_xAVG/double(Summary.anode);
                            avalsize.y2=anode_yAVG/double(Summary.anode);
                            avalsize.z2=-0.3;
                            // Evaluating the distance travled by the avalanche and its direction as cos(angle)
                            avalsize.distance = sqrt(pow(avalsize.z+0.3,2)+pow((avalsize.x2-avalsize.x),2)+pow((avalsize.y2-avalsize.y),2));
                            avalsize.avaldirection = (avalsize.z+0.3)/avalsize.distance;
                            
                            avalsize.gain = double(Summary.gain);
                            // Evaluating the size of the avalanche as the STD of the anode electron position wrt the AVG
                            avalsize.aval_xsize = sqrt(anode_xSumSq/double(Summary.anode)-avalsize.x2*avalsize.x2);
                            avalsize.aval_ysize = sqrt(anode_ySumSq/double(Summary.anode)-avalsize.y2*avalsize.y2);
                            avalsize.aval_radius = sqrt((anode_xSumSq/double(Summary.anode)-avalsize.x2*avalsize.x2) + (anode_ySumSq/double(Summary.anode)-avalsize.y2*avalsize.y2));

                            
                            AvalancheSizeTree->Fill();
                            anode_xAVG = 0;
                            anode_yAVG = 0;
                            anode_xSumSq = 0;
                            anode_ySumSq = 0;
                        }

                    
                    //t1->Fill();
                    
                    
					// Clear previous data
                    Summary={};
                }


                

                xProduction->Fill(electron.x1);
                yProduction->Fill(electron.y1);
                zProduction->Fill(electron.z1);

                // xCollection->Fill(electron.x2);
                // yCollection->Fill(electron.y2);
                //zCollection->Fill(electron.z2);

                collecTimeDistr ->Fill(electron.time2);
                collecEnergyDist->Fill(electron.energy2);


                Summary = CountTotalElectron(Summary);
                Summary = CountTotalLost(Summary);
                

                Summary = CountGEM1Top(Summary);
                Summary = CountGEM1Kap(Summary);
                Summary = CountGEM1Bot(Summary);
				Summary = CountGEM2Top(Summary);
                Summary = CountGEM2Kap(Summary);
                Summary = CountGEM2Bot(Summary);
				Summary = CountGEM3Top(Summary);
                Summary = CountGEM3Kap(Summary);
                Summary = CountGEM3Bot(Summary);
				Summary = CountAnode(Summary);

                i++;
                if (i%1000000 == 0) {
                    printTime();
                    cout << "Analyzed " << i<< "/"<<totalSimulatedElectrons << " electron tracks"<<endl; 
                }

            }while(i<totalSimulatedElectrons);
            printTime();
            cout << "Analyzed " <<totalSimulatedElectrons<< "/"<<totalSimulatedElectrons << " electron tracks\n---\n\n"<<endl; 

            i = 0;
            currentNameIntegratedSignal = chainIntegratedSignal->GetFile()->GetName();

            // looping over the  chain that contains signals infos
            do
                {
                    chainIntegratedSignal->GetEntry(i);
                    
                    signalWidth = signalWidth*1E-9; //  Pulse width express in seconds
                    if (signalWidth>=1E-9){
                        signalWidth_distr->Fill(double(signalWidth));}
                    integratedSignal = integratedSignal / (-1.6*1E-19); // Integratedsignal / electron charge = gain
                    if(integratedSignal > 1.)
                        signalHeightDistribution->Fill(double(integratedSignal));
                    i++;
                }while(i<totalIntegratedSignals);

         
            //TODO save data also as unbinned branches
            fitgain();
            writeFile();

        }
        if(muonTrack)
        {
            //throw std::invalid_argument( "Sorry, the analysis for muon track is not impemented.\nExiting" );
            //Setting Branches
            //Setting Branches, Only Useful Ones
            chain->SetBranchAddress("electronBranch",&electron);
            chain->SetBranchAddress("infoBranch", &info);
            chain->SetBranchAddress("muonBranch",&muon);
            //chain->SetBranchAddress("ionBranch",&ion);

            chainIntegratedSignal->SetBranchAddress("integratedsignal",&integratedSignal);
            chainIntegratedSignal->SetBranchAddress("SignalWidth",&signalWidth);

            currentName=chain->GetFile()->GetName();
            //endLayer = t1->Branch("collectionLayer", &Summary, "anodeTime/D:anodeEnergy/D:gain/D:number_of_primaries/I:totalelectrons/I:elec_on_gem_1_top/I:elec_on_gem_1_bot/I:elec_on_gem_2_top/I:elec_on_gem_2_bot/I:elec_on_gem_3_top/I:elec_on_gem_3_bot/I:elec_on_anode/I");
            //startCoord = t1->Branch("startCoord", &info.x0, "x0/D:y0/D:z0/D:dx0/D:dy0/D:dz0/D");
            customBranch = AvalancheSizeTree->Branch("AvalancheSize",&avalsize,"x0/D:y0/D:z0/D");

            do
            {
                chain->GetEntry(i);
                if(newChainFile() || i==totalSimulatedElectrons-1)//detects the end of one root File
                {
                    Summary.anodeTime = Summary.anodeTime/Summary.anode;
                    Summary.anodeEnergy = Summary.anodeEnergy/Summary.anode;

                    if(Summary.gain != 0) // discard the cases in which gain == 0
                        {
                            gain->Fill(double(Summary.gain)/double(Summary.number_of_primaries));
                            gain_with_bottom->Fill(Summary.extendedGain);
                        }
                    if (Summary.gain != 0) {
					total->Fill(Summary.total_electrons);
                    GEM1BotFractionLost->Fill(double(Summary.gem_1_bot)/double(Summary.total_electrons));
                    GEM1KapFractionLost->Fill(double(Summary.gem_1_kap)/double(Summary.total_electrons));
                    GEM1TopFractionLost->Fill(double(Summary.gem_1_top)/double(Summary.total_electrons));
                    GEM2TopFractionLost->Fill(double(Summary.gem_2_top)/double(Summary.total_electrons));
                    GEM2KapFractionLost->Fill(double(Summary.gem_2_kap)/double(Summary.total_electrons));
					GEM2BotFractionLost->Fill(double(Summary.gem_2_bot)/double(Summary.total_electrons));
					GEM3BotFractionLost->Fill(double(Summary.gem_3_bot)/double(Summary.total_electrons));
                    GEM3KapFractionLost->Fill(double(Summary.gem_3_kap)/double(Summary.total_electrons));
					GEM3TopFractionLost->Fill(double(Summary.gem_3_top)/double(Summary.total_electrons));
					AnodeFraction->Fill(double(Summary.anode)/double(Summary.total_electrons));
                    }

                    InitialClusters->Fill(Summary.number_of_clusters);                    
                    PrimaryElectrons->Fill(Summary.number_of_primaries);

                    //t1->Fill();
                    AvalancheSizeTree->Fill();
                    
					// Clear previous data
                    Summary={};
                }

                //avalanchesize->Fill();
                avalsize.x=info.x0;
                avalsize.y=info.y0;
                avalsize.z=info.z0;
                

                xProduction->Fill(electron.x1);
                yProduction->Fill(electron.y1);
                zProduction->Fill(electron.z1);

                // xCollection->Fill(electron.x2);
                // yCollection->Fill(electron.y2);                
                //zCollection->Fill(electron.z2);

                collecTimeDistr->Fill(electron.time2);
                collecEnergyDist->Fill(electron.energy2);

                
                Summary.number_of_primaries=muon.numberElecTrack;
                Summary.number_of_primaries=muon.numberClusters;

                Summary = CountTotalElectron(Summary);
                
                Summary = CountGEM1Top(Summary);
                Summary = CountGEM1Kap(Summary);
                Summary = CountGEM1Bot(Summary);
				Summary = CountGEM2Top(Summary);
                Summary = CountGEM2Kap(Summary);
                Summary = CountGEM2Bot(Summary);
				Summary = CountGEM3Top(Summary);
                Summary = CountGEM3Kap(Summary);
                Summary = CountGEM3Bot(Summary);
				Summary = CountAnode(Summary);

                i++;
            }while(i<totalSimulatedElectrons);

            i = 0;
            currentNameIntegratedSignal = chainIntegratedSignal->GetFile()->GetName();

            do
                {
                    chainIntegratedSignal->GetEntry(i);

                    signalWidth = signalWidth*1E-9; //  Pulse width express in seconds
                    cout << "sig = " << signalWidth << endl;
                    if (signalWidth>=1E-9){ 
                        signalWidth_distr->Fill(double(signalWidth));}
                    integratedSignal = integratedSignal / (-1.6*1E-19); // Integratedsignal / electron charge = gain
                    if(integratedSignal > 1.)
                        signalHeightDistribution->Fill(double(integratedSignal));
                    i++;
                }while(i<totalIntegratedSignals);

         
            //TODO save data also as unbinned branches
            fitgain();
            writeFile();

            
        }
    }

    else
        {
            //Implementation of analysis for single gem
            throw std::invalid_argument( "Sorry, the analysis for single gem is not impemented.\nExiting" );
        }
    printTime();
    cout  << " Done \n---\n\n\n"<<endl;
    

}

