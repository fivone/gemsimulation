- [GEMSimulation](#gemsimulation)
  * [Project Description](#project-description)
  * [Local machine setup](#local-machine-setup)
    + [Initial setup](#initial-setup)
      - [Environment Variables](#environment-variables)
      - [Compiler](#compiler)
      - [Field Map](#field-map)
    + [Execute simulation](#execute-simulation)
    + [Analyze simulation](#analyze-simulation)
  * [CONDOR batch system setup](#condor-batch-system-setup)
    + [Initial setup](#initial-setup-1)
      - [EOS folder](#eos-folder)
      - [Environment Variables](#environment-variables-1)
      - [Compiler](#compiler-1)
      - [Field Map](#field-map-1)
    + [Run batch of simulation as jobs](#run-batch-of-simulation-as-jobs)
    + [Analyze batch simulations](#analyze-batch-simulations)
  * [What's inside a simulation output file](#what-s-inside-a-simulation-output-file)
  * [What's inside an analysis output file](#what-s-inside-an-analysis-output-file)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


# GEMSimulation

This is the repository for GEM Simulations.
This branch (PostHenning) is a copy of the master branch, that runs now on:
* ROOT 6.18/04
* Garfield++ commit 15 April 2020

It allows you to run simulation of GEM detectors. This repo can be executed:
* On your local machine
* On a CONDOR batch system [implementation for PhysikZentrum Cluster] 

Please refer to the section of the documentation which is relevant for your installation.
In case of trouble you can write me an email [Francesco Ivone](mailto:francesco.ivone@cern.ch?subject=[GitLab]%20Source%20Han%20Sans)
***
## Project Description
Folder | Description|
|--------|-------------|
|Analysis |Contains the file to analyze the simulation, the related library.|
|ANSYS|Contains various ANSYS Field Map Calculation <sup>[__1__](#myfootnote1)</sup>  for **Single** and **Triple-GEM** with 2 template ANSYS scripts.|
|Garfield| Contains the GEM simulation file, the related library, the python script for CONDOR submission.|



## Local machine setup

### Initial setup
To get started, you have to download and install:
* [Garfield++](https://garfieldpp.web.cern.ch/garfieldpp/documentation/) ([commit](https://gitlab.cern.ch/garfield/garfieldpp/-/tree/db8d7814e628cbba0aaec82a044b043042581273/) 15 April 2020<sup>[__2__](#myfootnote2)</sup>, follow the [installation guide](http://garfieldpp.web.cern.ch/garfieldpp/getting-started/)).
* ROOT Cern  (v. 6.18/04, follow the [installation guide](https://root.cern.ch/building-root)).


ROOT Cern v6.20 should also be compatible. Other software versions combinations have not been tested.
#### Environment Variables
Please export the following environment variables. I recommend to add them to your `$HOME/.bashrc`
1. **GARFIELD_HOME** pointing to your Garfield build directory;
2. **HEED_DATABASE**=$GARFIELD_HOME/Heed/heed++/database;
3. **REPO_HOME**  pointing to the root folder of this project;
4. **REPODATAPATH** pointing to folder in which you want to store data; I store data in my EOS space (CERNBOX)
#### Compiler
In order to run a simulation you have to compile the `*.cpp` code into an executable. I am using gcc compiler version 7.5.0. You can check your compiler version issuing `gcc --version`. Compatibility with older version is not guaranteed.

You can compile the whole project by running:
* `cd $REPO_HOME`
* `make`
#### Field Map
In order to run a simulation, the executable needs to know the Electric Field map and the geometry of the GEM. It will search it in the folder `$REPODATAPATH/Ansys/Finalansysfiles/`. A set of ANSYS files<sup>[__1__](#myfootnote1)</sup> is stored in the Ansys folder of this repo. Execute:<br>
`cp $REPO_HOME/Ansys $REPODATAPATH/`<br>
to store them properly.

The Electric Field Map files have a name convention that is needed for file/folder naming. If you disrespect the naming convention this repo will kick you hard. In case you generate new ANSYS Field Map files save them with the following name convention:<br>
`<ANSYS Type>`<sup>[__1__](#myfootnote1)</sup>`_Hole_<GEMTop Hole Diameter>_<GEMMiddle Hole Diameter>_<GEMBot Hole Diameter>_VDRIFT_<DRIFT voltage>_<number of layer>L.lis`<br>
Check the ANSYS folder for examples.

### Execute simulation
The executable for the simulation is `$REPO_HOME/Garfield/gemAllbatch`. Execute: 
* `./gemAllBatch test.root n Hole_70_50_70_VDRIFT_2500_3L  0.7 0.3 0 0 760 293.15 0 10000000000`

to run a simulation with the following parameters:

- `[ROOT Output-Filename] = test.root` 
- `[Muon Track] = n ` simulate a single electron starting in the GEM drift gap
- `[Ansys FileID]= Hole_70_50_70_VDRIFT_2500_3L`
- `[Parts Argon] = 0.7`
- `[Parts CO2] = 0.3`
- `[Parts N2]=0`
- `[Parts O2]=0`
- `[Pressure in Torr]=760`
- `[Temperature in K]=293.15`
- `[Parts H2O]=0`
- `[Muon energy (eV)]=10000000000`   (muons will not be simulated anyway)

Thanks to the naming convention the outfile will be stored in
`$REPODATAPATH/DataRaw/SingleElectron/3L/Hole_70_50_70_VDRIFT_2500_3L/<yyyy.mm.dd>/test.root`<br>
Details on the contents of this file in the section [What's inside a simulation output file](#what-s-inside-a-simulation-output-file)
### Analyze simulation
In<br>
`$REPO_HOME/Analysis`<br>
are located the file to analyze the TTree produced by `gemAllBatch`.

The analysis main file is AnagemAllBatch.cpp. It is kept as clean as possible, leaving all the function and variable declaration in AnaDef.h.<br>
To analyze the simulation you just executed, issue:<br>
`./AnagemAllBatch $REPO_HOME/DataRaw/SingleElectron/3L/Hole_70_50_70_VDRIFT_2500_3L/ <option>`<br>
* `<option> = <yyy.mm.dd>` will analyze only the files in this subfolder
*  `<option> = .` will analyze the file in all subfolders of `$REPO_HOME/DataRaw/SingleElectron/3L/Hole_70_50_70_VDRIFT_2500_3L/`

The output file is  stored in the folder `$REPODATAPATH/DataAna` and inherits the name from the ANSYS file naming convention. <br>
Details on the contents of this file in the section [What's inside an analysis output file](#what-s-inside-an-analysis-output-file)

## CONDOR batch system setup
If you want take advantage of the computational power of a CONDOR cluster you are in the right section.
### Initial setup
To get started, you have to download and install:
* [Garfield++](https://garfieldpp.web.cern.ch/garfieldpp/documentation/) ([commit](https://gitlab.cern.ch/garfield/garfieldpp/-/tree/db8d7814e628cbba0aaec82a044b043042581273/) 15 April 2020<sup>[__2__](#myfootnote2)</sup>, follow the [installation guide](http://garfieldpp.web.cern.ch/garfieldpp/getting-started/)).

At the time of writing, ROOT CERN 6.20 is already installed in the Physikzentrum CONDOR cluster.<br>
In case you are dealing with ROOT version incopatibility try:
* `source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.20.00/x86_64-centos7-gcc48-opt/bin/thisroot.sh`
or
* contact you IT expert

#### EOS folder
Please follow this guide to mount the EOS filesystem in your Physikzentrum machine: [EOS in Physikzentrum cluster](https://wiki-3ab.physik.rwth-aachen.de/twiki/do/view/Inst3IT/UsingEOS)

#### Environment Variables
Please export the following environment variables. I recommend to add them to your `$HOME/.bashrc`
1. **GARFIELD_HOME** pointing to your Garfield build directory;
2. **HEED_DATABASE**=$GARFIELD_HOME/Heed/heed++/database;
3. **REPO_HOME**  pointing to the root folder of this project;
4. **REPODATAPATH** pointing to your EOS space (CERNBOX)

The point 4 is delicate. You want that all nodes on the cluster can access `$REPODATAPATH`. At the same time your CERNBOX is private so by default the NODES cluster cannot access/see it.<br>
The solution is the kerberos keytab ([some info](https://twiki.cern.ch/twiki/bin/view/ITProc/AFSAndServiceAccount))
1. On your local machine in the cluster (e.g. lx3a44) issue: 
   1. `ktutil`
   2. `add_entry -password -p <your_user>@CERN.CH -k 1 -e arcfour-hmac-md5`
   3. `wkt <your_user>.keytab`
2. `mv <your_user>.keytab $REPO_HOME/Garfield`
3. edit `$REPO_HOME/RunGemAllBatch.py`:
   1. replace all occurences of fivone with  <your_user>

The keytab you created will be used from all the nodes to authenticate themselves as you and have access to your EOS folder.<br>
You can add this snippet of code to your `$HOME/.bashrc` to get an CERN kerberos ticket each time you start a new session:

```
kinit -kt $REPO_HOME/Garfield/<your_user>.keytab  <your_user>
eosfusebind -g
```
Don't share your keytab file.
#### Compiler
In order to run a simulation you have to compile the `*.cpp` code into an executable. I am using gcc compiler version 7.5.0. You can check your compiler version issuing `gcc --version`. Compatibility with older version is not guaranteed.<br>
On stock the Physikzentrum cluster uses gcc 4.9. No worries, just add this line to your `$HOME/.bashrc` use a compiler version compatible with this repo<br>
`source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8.3.0.multilib/x86_64-centos7/setup.sh`

You can compile the whole project by running:
* `cd $REPO_HOME`
* `make`
#### Field Map
In order to run a simulation, the executable needs to know the Electric Field map and the geometry of the GEM. It will search it in the folder `$REPODATAPATH/Ansys/Finalansysfiles/`. A set of ANSYS files<sup>[__1__](#myfootnote1)</sup> is stored in the Ansys folder of this repo. Execute:<br>
`cp $REPO_HOME/Ansys $REPODATAPATH/`<br>
to store them properly.


### Run batch of simulation as jobs

Normally, to achieve good statistics you need to run many simulations. For this the batch system is perfect. I have modified a script so that is automatically creates and submits batch jobs to the system.
It is called `RunGemAllBatch.py` and located in the Garfield folder.

You need to declaree the `NumberOfJobs` and `interval` and `job_flavour`. These determine how many iterations are run and how they are split. Typically 100 simulations run comfortably within the 2 hour queue (So `Interval`=100). To get good statistics run at least 1000 simulations (`NumberOfJobs`=10, `Interval`=100).

For your paths you need to declare an `afs_dir` to the directory that `gemAllBatch.cpp` is in. Set `eos_dir` to the directory where you want to create several subdirectories for all different simulations.
Finally, set `GARFIELD_HOME_Location` to the folder you installed Garfield in.

For this script to work properly, create a folder `BatchScripts`, `Executables` and `TempFiles` in the folder that is pointed to by `afs_dir`. `BatchScripts` will have all the scripts as well as the condor submit files written by the python script in case you want to run some again manually. `Executables` stores all the executables created each time you run the script, and `TempFiles` is just were the root files are stored while they are being filled and before they are moved to eos.

Further down, there are lists which you can fill to iterate through different setups automatically, this should be self-explanatory.
Inside the for loops you need to provide the `NAME` generation as well as the `SUBDIRECTORY` in which the resulting files should be saved in.

### Analyze batch simulations

Follow the same steps that are detailed in [Analyze simulation](#analyze-simulation)

---

## What's inside a simulation output file

Let's first explain the branches contained in the  `TTree`:

|Branch |Leaves|Description|
|-------|------|------------|
|`electronBranch`|x1,y1,z1,time1,energy1<br>x2,y2,z2,dx2,dy2,dz2,time2,energy2<br>status<br> radius|init. param. of the created electron <br> param. at the end of the electron drift line with momentum <br> reason why the track was stopped<br> Distance(Final(x,y),ParentCluster(x,y))|
|`ionBranch`|same as electronBranch without momentum |same as electronBranch without momentum|
|`infoBranch`|cnt<br>numberElec, numberIon, numberElecAll<br>x0,y0,z0<br>dx0,dy0,dz0<br>time0,e0 |electron counter<br>total number of electrons, ions, electrons + starting elec<br> init. posit. of the electron <br> init. momentum <br> init. time, energy|
|`muonBranch`|      beta <br>   x0Track, y0Track, z0Track, t0Track<br>dx0Track, dy0Track,dz0Track<br>numberClusters, numberElecTrack|only if simulated Muon = **y**|


## What's inside an analysis output file

|HistoName|Description|
|---------|-----------|
| `xProduction`<br>`yProduction`<br>`zProduction`| The distribution of the **production** coordinate of all the simulated electrons|
| `xCollection`<br>`yCollection`<br>`zCollection`| The distribution of the **endPoint** coordinate of all the simulated electrons|
| `gain`| The distribution of the **gain-values** resulting from each simulation|
| `totalElectrons`| The distribution of the number of produced electrons in each simulation|
| `GEM<>TopFractionLost`<br>`GEM<>BotFractionLost` | The distribution of the fraction of electrons ended on each layer|


<a name="myfootnote1">[1]</a> : The ANSYS files go in group of 4:

* __ELIST.lis__: the list of elements with pointers to the material property table and to the node list;
* __NLIST.lis__: the list of nodes, with their position in space;
* __MPLIST.lis__: material property table;
* __PRNSOL.lis__: estimated potentials at each of the nodes.


<a name="myfootnote2">[2]</a> : I have made some small changes to the Garfield code in order to make it work properly with COMSOL. In case of trouble send me an email.
